package com.lamposcan.di.module

import android.app.Application
import android.content.Context

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.lamposcan.data.AppDataManager
import com.lamposcan.data.DataManager
import com.lamposcan.data.prefs.AppPreferencesHelper
import com.lamposcan.data.prefs.PreferenceHelper
import com.lamposcan.data.remote.ApiHelper
import com.lamposcan.data.remote.AppApiHelper
import com.lamposcan.di.PreferenceInfo
import com.lamposcan.utils.AppConstants
import com.lamposcan.utils.ResourceProvider
import com.lamposcan.utils.rx.AppSchedulerProvider
import com.lamposcan.utils.rx.SchedulerProvider

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper {
        return appApiHelper
    }

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideDataManager(appDataManager: AppDataManager): DataManager {
        return appDataManager
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
    }

    @Provides
    @PreferenceInfo
    fun providePreferenceName(): String {
        return AppConstants.PREF_NAME
    }

    @Provides
    @Singleton
    fun providePreferencesHelper(appPreferencesHelper: AppPreferencesHelper): PreferenceHelper {
        return appPreferencesHelper
    }

    @Provides
    fun provideSchedulerProvider(): SchedulerProvider {
        return AppSchedulerProvider()
    }

    @Provides
    @Singleton
    fun providerResourceProvider(context: Context): ResourceProvider {
        return ResourceProvider(context)
    }
}
