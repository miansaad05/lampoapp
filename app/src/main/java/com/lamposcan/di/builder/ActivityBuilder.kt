package com.lamposcan.di.builder

import com.lamposcan.ui.main.home.MainActivity
import com.lamposcan.ui.main.home.MainActivityModule
import com.lamposcan.ui.main.home.addpost.AddPostActivity
import com.lamposcan.ui.main.home.addpost.AddPostActivityModule
import com.lamposcan.ui.main.home.conversation.ConversationProvider
import com.lamposcan.ui.main.home.discover.DiscoverActivity
import com.lamposcan.ui.main.home.discover.DiscoverActivityModule
import com.lamposcan.ui.main.home.navigation.friendslist.FollowFollowingsActivity
import com.lamposcan.ui.main.home.navigation.friendslist.FollowFollowingsModule
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers.FollowersProvider
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings.FollowingsProvider
import com.lamposcan.ui.main.home.navigation.friendslist.unfollowdialog.UnFollowProvider
import com.lamposcan.ui.main.home.navigation.notifications.NotificationsActivity
import com.lamposcan.ui.main.home.navigation.notifications.NotificationsModule
import com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests.FriendRequestsProvider
import com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab.NotificationsProvider
import com.lamposcan.ui.main.home.navigation.scanhistory.ScanHistoryActivity
import com.lamposcan.ui.main.home.navigation.scanhistory.ScanHistoryModule
import com.lamposcan.ui.main.home.navigation.setting.SettingsActivity
import com.lamposcan.ui.main.home.navigation.setting.SettingsActivityModule
import com.lamposcan.ui.main.home.navigation.terms.TermsAndConditionsActivity
import com.lamposcan.ui.main.home.navigation.terms.TermsAndConditionsActivityModule
import com.lamposcan.ui.main.home.newsfeed.NewsFeedProvider
import com.lamposcan.ui.main.home.newsfeed.comments.CommentsActivity
import com.lamposcan.ui.main.home.newsfeed.comments.CommentsActivityModule
import com.lamposcan.ui.main.home.newsfeed.fullscreenview.FullScreenActivity
import com.lamposcan.ui.main.home.newsfeed.fullscreenview.FullScreenActivityModule
import com.lamposcan.ui.main.home.newsfeed.likes.LikesActivity
import com.lamposcan.ui.main.home.newsfeed.likes.LikesActivityModule
import com.lamposcan.ui.main.home.newsfeed.reportpost.ReportPostProvider
import com.lamposcan.ui.main.home.singlepostview.SinglePostActivity
import com.lamposcan.ui.main.home.singlepostview.SinglePostActivityModule
import com.lamposcan.ui.main.profile.editprofile.EditProfileActivity
import com.lamposcan.ui.main.profile.editprofile.EditProfileActivityModule
import com.lamposcan.ui.main.profile.viewprofile.ProfileActivity
import com.lamposcan.ui.main.profile.viewprofile.ProfileActivityModule
import com.lamposcan.ui.main.profile.viewprofile.profilefragment.ProfileProvider
import com.lamposcan.ui.main.qrcode.posts.PostQrProvider
import com.lamposcan.ui.main.qrcode.profile.ProfileQrProvider
import com.lamposcan.ui.main.qrcode.scanqr.ScanCameraActivity
import com.lamposcan.ui.main.qrcode.scanqr.ScanCameraActivityModule
import com.lamposcan.ui.registeration.forgotpassword.ForgotPasswordActivity
import com.lamposcan.ui.registeration.forgotpassword.ForgotPasswordActivityModule
import com.lamposcan.ui.registeration.login.LoginActivity
import com.lamposcan.ui.registeration.login.LoginActivityModule
import com.lamposcan.ui.registeration.signup.RegisterActivity
import com.lamposcan.ui.registeration.signup.RegisterActivityModule
import com.lamposcan.ui.registeration.success.SuccessActivity
import com.lamposcan.ui.registeration.success.SuccessActivityModule
import com.lamposcan.ui.splash.SplashActivity
import com.lamposcan.ui.splash.SplashActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [SplashActivityModule::class])
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [LoginActivityModule::class])
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [ForgotPasswordActivityModule::class])
    abstract fun bindForgotPassword(): ForgotPasswordActivity

    @ContributesAndroidInjector(modules = [RegisterActivityModule::class])
    abstract fun bindRegisterPassword(): RegisterActivity

    @ContributesAndroidInjector(modules = [SuccessActivityModule::class])
    abstract fun bindSuccessActivity(): SuccessActivity

    @ContributesAndroidInjector(
        modules = [MainActivityModule::class, NewsFeedProvider::class,
            ConversationProvider::class,
            ProfileProvider::class,
            ReportPostProvider::class,
            ProfileQrProvider::class,
            PostQrProvider::class]
    )
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [AddPostActivityModule::class])
    abstract fun bindAddPostActivity(): AddPostActivity


    @ContributesAndroidInjector(modules = [TermsAndConditionsActivityModule::class])
    abstract fun bindTermsAndConditionsActivity(): TermsAndConditionsActivity

    @ContributesAndroidInjector(modules = [EditProfileActivityModule::class])
    abstract fun bindEditProfileActivity(): EditProfileActivity

    @ContributesAndroidInjector(modules = [LikesActivityModule::class])
    abstract fun bindAddLikesActivity(): LikesActivity

    @ContributesAndroidInjector(
        modules = [CommentsActivityModule::class,
            ReportPostProvider::class,
            PostQrProvider::class]
    )
    abstract fun bindAddCommentsActivity(): CommentsActivity

    @ContributesAndroidInjector(
        modules = [ProfileActivityModule::class,
            ProfileProvider::class,
            ProfileQrProvider::class,
            PostQrProvider::class]
    )
    abstract fun bindProfileActivity(): ProfileActivity

    @ContributesAndroidInjector(
        modules = [FollowFollowingsModule::class,
            FollowersProvider::class,
            FollowingsProvider::class,
            UnFollowProvider::class]
    )
    abstract fun bindAddFollowFollowingsActivity(): FollowFollowingsActivity

    @ContributesAndroidInjector(
        modules = [NotificationsModule::class, FriendRequestsProvider::class,
            NotificationsProvider::class]
    )
    abstract fun bindNotificationsActivity(): NotificationsActivity


    @ContributesAndroidInjector(modules = [ScanHistoryModule::class])
    abstract fun bindAddScanHistory(): ScanHistoryActivity

    @ContributesAndroidInjector(modules = [DiscoverActivityModule::class])
    abstract fun bindAddDiscoverActivity(): DiscoverActivity

    @ContributesAndroidInjector(modules = [ScanCameraActivityModule::class])
    abstract fun bindAddScanCameraActivity(): ScanCameraActivity

    @ContributesAndroidInjector(modules = [SinglePostActivityModule::class])
    abstract fun bindAddSinglePostActivity(): SinglePostActivity

    @ContributesAndroidInjector(modules = [SettingsActivityModule::class])
    abstract fun bindAddSettingsActivity(): SettingsActivity

    @ContributesAndroidInjector(modules = [FullScreenActivityModule::class])
    abstract fun bindAddFullScreenActivity(): FullScreenActivity
}
