package com.lamposcan

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * A provider factory that persists ViewModels [ViewModel].
 * Used if the view model has a parameterized constructor.
 */
class ViewModelProviderFactory<V>(val viewModel: V) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom((viewModel as T)::class.java)) {
            return viewModel
        }
        throw IllegalArgumentException("Unknown class name")
    }
}
