package com.lamposcan.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;

import com.lamposcan.R;
import com.lamposcan.databinding.InputWidgetBinding;
import com.lamposcan.utils.AppLogger;

@BindingMethods({
        @BindingMethod(type = InputWidget.class, attribute = "onInputFieldClick",
                method = "inputFieldClicked")
})
public class InputWidget extends FrameLayout {
    final int INPUT_TYPE_VISIBLE_PASSWORD = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;
    final int INPUT_TYPE_HIDDEN_PASSWORD = EditorInfo.TYPE_TEXT_VARIATION_NORMAL;
    private InputWidgetBinding inputWidgetBinding;
    private String text = "", hint = "";
    private Context context;
    private boolean enableInput = true;
    private boolean showPasswordVisibilityIcon = false;
    private int inputTypeIcon = 0;
    private int passwordIcon = 0;
    private int inputTypes = 0;
    private OnInputFieldClick listener;
    private boolean isPasswordShowing;

    public InputWidget(@NonNull Context context) {
        super(context);
        initializeView(context, null, 0);
    }

    public InputWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initializeView(context, attrs, 0);
    }

    public InputWidget(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeView(context, attrs, defStyleAttr);
    }

    private void initializeView(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.InputWidget,
                defStyleAttr, 0);

        this.context = context;
        try {
            int size = array.length();
            for (int count = 0; count < size; count++) {
                int attr = array.getIndex(count);
                switch (attr) {
                    case R.styleable.InputWidget_android_text:
                        text = array.getString(attr);
                        break;
                    case R.styleable.InputWidget_android_hint:
                        hint = array.getString(attr);
                        break;
                    case R.styleable.InputWidget_enableInput:
                        enableInput = array.getBoolean(attr, true);
                        break;
                    case R.styleable.InputWidget_passwordVisibilityIcon:
                        showPasswordVisibilityIcon = array.getBoolean(attr, true);
                        break;
                    case R.styleable.InputWidget_inputTypeIcon:
                        inputTypeIcon = array.getResourceId(attr, R.drawable.icon_email);
                        break;
                    case R.styleable.InputWidget_passwordIcon:
                        passwordIcon = array.getResourceId(attr, R.drawable.icon_locked);
                        break;
                    case R.styleable.InputWidget_android_inputType:
                        inputTypes = array.getInt(attr, EditorInfo.TYPE_TEXT_VARIATION_NORMAL);
                        break;
                    default:
                        AppLogger.INSTANCE.d("asad_input_widget",
                                "Unknown attribute for " + getClass().toString() + ": " + attr);
                }
            }


            inputWidgetBinding = InputWidgetBinding.inflate(LayoutInflater.from(context), this, true);

            if (!enableInput) {
                inputWidgetBinding.etInput.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (listener != null) {
                            listener.inputFieldClicked();
                        }
                    }
                });
            }
            inputWidgetBinding.ivPassword.setVisibility(showPasswordVisibilityIcon ? View.VISIBLE : View.GONE);
            if (showPasswordVisibilityIcon) {
                inputWidgetBinding.ivPassword.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showPassword(!isPasswordShowing);
                    }
                });
            }
        } finally {
            array.recycle();
        }
    }

    private boolean isPasswordVisible(EditText editText) {
        return editText.getInputType() == INPUT_TYPE_VISIBLE_PASSWORD;
    }

    public boolean isPasswordInput() {
        int inputType = inputWidgetBinding.etInput.getInputType();
        return inputType == (EditorInfo.TYPE_CLASS_TEXT | EditorInfo.TYPE_NUMBER_VARIATION_PASSWORD)
                || inputType == (EditorInfo.TYPE_CLASS_TEXT | EditorInfo.TYPE_TEXT_VARIATION_PASSWORD)
                || inputType == (EditorInfo.TYPE_CLASS_TEXT | EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
                || inputType == (EditorInfo.TYPE_CLASS_TEXT | EditorInfo.TYPE_TEXT_VARIATION_WEB_PASSWORD);

    }

    public void showPassword(boolean visible) {
        if (isPasswordInput()) {
            isPasswordShowing = visible;
            int start = inputWidgetBinding.etInput.getSelectionStart();
            int end = inputWidgetBinding.etInput.getSelectionEnd();
            inputWidgetBinding.etInput.setTransformationMethod(visible ? null : new PasswordTransformationMethod());
            // retain cursor selection
            inputWidgetBinding.etInput.setSelection(start, end);
            inputWidgetBinding.ivPassword.setImageResource(visible ? R.drawable.ic_visibility_off : R.drawable.ic_visibility);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        setInputFieldText(text);
        setInputFieldHint(hint);
        setInputType(inputTypes);
        setInputTypeIcon(inputTypeIcon);
        setPasswordIcon(passwordIcon);
    }

    private void setPasswordIcon(int passwordIcon) {
        inputWidgetBinding.ivPassword.setImageResource(passwordIcon);
    }

    private void setInputType(int inputTypes) {
        inputWidgetBinding.etInput.setInputType(inputTypes);
    }

    private void setInputTypeIcon(int inputTypeIcon) {
        inputWidgetBinding.ivInputType.setImageResource(inputTypeIcon);
    }

    public void setInputFieldText(String text) {
        if (inputWidgetBinding != null) {
            inputWidgetBinding.etInput.setText(text);
        }
    }

    public void setInputFieldHint(String hint) {
        if (inputWidgetBinding != null) {
            inputWidgetBinding.etInput.setHint(hint);
        }
    }

    public void setOnInputFieldClick(OnInputFieldClick listener) {
        this.listener = listener;
    }

    public interface OnInputFieldClick {
        void inputFieldClicked();
    }

}
