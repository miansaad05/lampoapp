package com.lamposcan.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;

import com.lamposcan.R;
import com.lamposcan.databinding.ButtonWidgetBinding;
import com.lamposcan.utils.AppLogger;

@BindingMethods({
        @BindingMethod(type = ButtonWidget.class, attribute = "onButtonClick",
                method = "onButtonClicked")
})
public class ButtonWidget extends FrameLayout {
    private ButtonWidgetBinding buttonWidgetBinding;

    private String text = "";
    private int bg = 0;
    private int textColor = 0;
    private Context context;
    private OnButtonClick listener;

    public ButtonWidget(@NonNull Context context) {
        super(context);
        initializeView(context, null, 0);
    }

    public ButtonWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initializeView(context, attrs, 0);
    }

    public ButtonWidget(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeView(context, attrs, defStyleAttr);
    }

    private void initializeView(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ButtonWidget,
                defStyleAttr, 0);

        this.context = context;
        try {
            int size = array.length();
            for (int count = 0; count < size; count++) {
                int attr = array.getIndex(count);
                switch (attr) {
                    case R.styleable.ButtonWidget_android_text:
                        text = array.getString(attr);
                        break;
                    case R.styleable.ButtonWidget_android_textColor:
                        textColor = array.getResourceId(attr,
                                ContextCompat.getColor(context, R.color.colorWhite));
                        break;
                    case R.styleable.ButtonWidget_background:
                        bg = array.getResourceId(attr, R.drawable.bg_btn);
                        break;
                    default:
                        AppLogger.INSTANCE.d("asad_input_widget",
                                "Unknown attribute for " + getClass().toString() + ": " + attr);
                }
            }


            buttonWidgetBinding = ButtonWidgetBinding.inflate(LayoutInflater.from(context), this, true);

            buttonWidgetBinding.button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onButtonClicked();
                    }
                }
            });
        } finally {
            array.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setInputFieldText(text);
        setTextColor(textColor);
        setButtonBackground(bg);
    }

    private void setButtonBackground(int bg) {
        buttonWidgetBinding.parent.setBackground(ContextCompat.getDrawable(context, bg));
    }

    private void setTextColor(int textColor) {
        buttonWidgetBinding.button.setTextColor(ContextCompat.getColor(context, textColor));
    }


    public void setInputFieldText(String text) {
        if (buttonWidgetBinding != null) {
            buttonWidgetBinding.button.setText(text);
        }
    }

    public void setOnButtonClick(OnButtonClick listener) {
        this.listener = listener;
    }

    public interface OnButtonClick {
        void onButtonClicked();
    }

}
