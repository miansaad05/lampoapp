package com.lamposcan.ui.splash

import com.lamposcan.ui.base.BaseNavigator

interface SplashNavigator : BaseNavigator{

    fun decideNextActivity()
}
