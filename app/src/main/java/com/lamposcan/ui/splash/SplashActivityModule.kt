package com.lamposcan.ui.splash

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class SplashActivityModule : BaseActivityModule<SplashViewModel>() {
    @Provides
    fun provideSplashViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): SplashViewModel {
        return SplashViewModel(dataManager, schedulerProvider)
    }
}
