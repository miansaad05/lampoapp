package com.lamposcan.ui.splash

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class SplashViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<SplashNavigator>(dataManager, schedulerProvider) {

    fun goToNextActivity(): Disposable {
        return Completable.timer(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .subscribe { navigator?.decideNextActivity() }
    }
}
