package com.lamposcan.ui.splash

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ActivitySplashBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.MainActivity
import com.lamposcan.ui.registeration.login.LoginActivity
import com.lamposcan.ui.shake.transparent.TransparentActivity
import com.lamposcan.utils.CommonUtils
import com.tramsun.libs.prefcompat.Pref
import javax.inject.Inject

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(), SplashNavigator {

    private lateinit var binding: ActivitySplashBinding
    @Inject
    lateinit var viewModel: SplashViewModel


    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_splash
    }

    override fun viewModel(): SplashViewModel {
        return viewModel
    }

    override fun onStart() {
        super.onStart()
        if (intent.hasExtra("isFromShake")) {
            startActivity(Intent(this, TransparentActivity::class.java))
            finishAffinity()
        }
    }

    override fun onResume() {
        super.onResume()
        if (intent.hasExtra("isFromShake")) {
            startActivity(Intent(this, TransparentActivity::class.java))
            finishAffinity()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!intent.hasExtra("isFromShake")) {

            viewDataBinding?.let { binding = it }
            viewModel.navigator = this


            viewModel.compositeDisposable.add(viewModel.goToNextActivity())
        }
    }

    override fun decideNextActivity() {
        val gsonModel = Pref.getString(CommonUtils.USER_MODEL)

        if (!TextUtils.isEmpty(gsonModel)) {
            startActivity(MainActivity.newIntent(context))
        } else {
            startActivity(LoginActivity.newIntent(context))
        }
        finishAffinity()
    }
}
