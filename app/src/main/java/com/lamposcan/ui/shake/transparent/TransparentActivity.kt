package com.lamposcan.ui.shake.transparent

import android.app.Activity
import android.graphics.Bitmap
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.lamposcan.R
import com.lamposcan.utils.AppLogger
import com.tarek360.instacapture.Instacapture
import com.tarek360.instacapture.listener.SimpleScreenCapturingListener

class TransparentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transparent)

        Toast.makeText(this, "TransparentActivity Started!", Toast.LENGTH_LONG).show()

        Instacapture.capture(this, object : SimpleScreenCapturingListener() {
            override fun onCaptureComplete(bitmap: Bitmap) {
                AppLogger.d("my_receiver", "onCaptureComplete")
                //Your code here..
            }
        })
    }
}
