package com.lamposcan.ui.shake;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.lamposcan.R;
import com.lamposcan.utils.AppLogger;
import com.squareup.seismic.ShakeDetector;

public class ServiceShake extends Service implements com.squareup.seismic.ShakeDetector.Listener {

    public int onStartCommand(Intent intent, int flags, int startId) {
        AppLogger.INSTANCE.d("my_service", "onStartCommand()");

        showNotification();
        return START_STICKY;
    }

    private void showNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String NOTIFICATION_CHANNEL_ID = "com.lamposcan";
            String channelName = "My Background Service";
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName,
                    NotificationManager.IMPORTANCE_NONE);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.drawable.icon_lampo)
                    .setContentTitle("LampoScan Shaking...")
                    .setPriority(NotificationManager.IMPORTANCE_MAX)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .build();
            startForeground(2, notification);
        } else {

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "defaultChannel");

            builder.setSmallIcon(R.drawable.icon_lampo)
                    .setOngoing(true)
                    .setContentTitle("LampoScan Shaking...")
                    .setCategory(Notification.CATEGORY_SERVICE);

            Notification notification = builder.build();
            startForeground(123, notification);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppLogger.INSTANCE.d("my_service", "onCreate()");
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector sd = new ShakeDetector(this);
        sd.start(sensorManager);
//register your sensor manager listener here
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void hearShake() {



        AppLogger.INSTANCE.d("my_service_shake", ">>>>>> my service_receiver <<<<<");
        try {
            Intent broadcastIntent = new
                    Intent("serviceRestarted");
            sendBroadcast(broadcastIntent);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
        }
        Toast.makeText(this, "ShackFromService", Toast.LENGTH_SHORT).show();
    }

}