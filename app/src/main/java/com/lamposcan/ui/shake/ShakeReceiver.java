package com.lamposcan.ui.shake;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.lamposcan.utils.AppLogger;
import com.tarek360.instacapture.Instacapture;
import com.tarek360.instacapture.listener.SimpleScreenCapturingListener;

public class ShakeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        AppLogger.INSTANCE.d("my_receiver", ">>>>>> my receiver <<<<<");
        if (null != intent && intent.getAction() != null && intent.getAction().equals("serviceRestarted")) {



            Intent appStartIntent = context.getPackageManager().getLaunchIntentForPackage("com.lamposcan");
            appStartIntent.putExtra("isFromShake", true);
            context.startActivity(appStartIntent);
        }
    }

}