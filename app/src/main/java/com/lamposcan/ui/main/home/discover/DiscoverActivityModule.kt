package com.lamposcan.ui.main.home.discover

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.discover.adapter.DiscoverPeopleAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class DiscoverActivityModule : BaseActivityModule<DiscoverViewModel>() {
    @Provides
    fun provideDiscoverViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): DiscoverViewModel {
        return DiscoverViewModel(dataManager, schedulerProvider)
    }

    @Provides
    fun provideDiscoverPeopleAdapter(context: DiscoverActivity): DiscoverPeopleAdapter {
        return DiscoverPeopleAdapter(context)
    }
}
