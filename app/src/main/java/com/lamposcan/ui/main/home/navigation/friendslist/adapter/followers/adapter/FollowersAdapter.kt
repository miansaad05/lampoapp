package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.api.FollowersModel
import com.lamposcan.databinding.ItemFollowersBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class FollowersAdapter(val context: Context?) : RecyclerView.Adapter<BaseViewHolder>(),
    AdapterUpdateListener {
    var list: MutableList<FollowersModel> = ArrayList()
    private var listener: OnClickListener? = null


    fun setListener(listener: OnClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemFollowersBinding.inflate(
            LayoutInflater.from(context), parent, false
        )
        return FollowersViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    interface OnClickListener {
        fun onFollowClick()
    }

    override fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun addItems(obj: Any) {
        list.add(0, obj as FollowersModel)
        notifyItemInserted(0)
    }

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {
        list.addAll(items as Collection<FollowersModel>)
        notifyDataSetChanged()
    }

    open inner class FollowersViewHolder(val binding: ItemFollowersBinding) :
        BaseViewHolder(binding.root), FollowersItemViewModel.OnClickListener {
        private lateinit var itemViewModel: FollowersItemViewModel
        override fun onBind(position: Int) {
            itemViewModel = FollowersItemViewModel(list[position], this)
            binding.viewModel = itemViewModel
            binding.executePendingBindings()
        }

        override fun onFollowClick() {
            listener?.onFollowClick()
        }
    }
}