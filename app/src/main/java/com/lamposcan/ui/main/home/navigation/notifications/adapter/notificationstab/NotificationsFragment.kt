package com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.NotificationModel
import com.lamposcan.databinding.NotificationsFragmentBinding
import com.lamposcan.ui.base.baseFragment.BaseFragment
import com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab.adapter.NotificationsAdapter
import com.lamposcan.utils.MessageAlert
import com.lamposcan.utils.NetworkUtils
import javax.inject.Inject

class NotificationsFragment :
    BaseFragment<NotificationsFragmentBinding, NotificationsViewModel>(),
    NotificationsNavigator {

    companion object {
        fun newInstance() = NotificationsFragment()
    }

    @Inject
    lateinit var viewModel: NotificationsViewModel
    @Inject
    lateinit var adapter: NotificationsAdapter

    private lateinit var binding: NotificationsFragmentBinding

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.notifications_fragment
    }

    override fun viewModel(): NotificationsViewModel {
        return viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (view != null)
            view.tag = this
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        baseActivity?.let {
            mContext = it
        }
        userModel = getUserModelFromPref()

        binding.rvNotifications.itemAnimator = null
        binding.rvNotifications.adapter = adapter


        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.fetchNotifiations(userModel.apiToken)
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun showMessage(message: String) {
        viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
    }

    override fun onRefresh() {
        if (NetworkUtils.isNetworkConnected(context)) {
            adapter.clearItems()
            viewModel.isRefreshing.set(false)
            viewModel.fetchNotifiations(userModel.apiToken)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun setNotificationList(list: List<NotificationModel>) {
        adapter.addItems(list, false)
        hideLoader()
        viewModel.isRefreshing.set(false)
    }

    override fun showLoader() {
        baseActivity?.showLoading()
    }

    override fun hideLoader() {
        baseActivity?.hideLoading()
    }
}
