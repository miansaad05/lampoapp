package com.lamposcan.ui.main.home.newsfeed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ConversationFragmentBinding
import com.lamposcan.ui.base.baseFragment.BaseFragment
import com.lamposcan.ui.main.home.conversation.ConversationNavigator
import com.lamposcan.ui.main.home.conversation.ConversationViewModel
import com.lamposcan.utils.MessageAlert
import javax.inject.Inject

class ConversationFragment : BaseFragment<ConversationFragmentBinding, ConversationViewModel>(),
    ConversationNavigator {

    companion object {
        fun newInstance() = ConversationFragment()
    }

    @Inject
    lateinit var viewModel: ConversationViewModel

    private lateinit var binding: ConversationFragmentBinding

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.conversation_fragment
    }

    override fun viewModel(): ConversationViewModel {
        return viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (view != null)
            view.tag = this
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.let { binding = it }
    }

    override fun showMessage(message: String) {
        viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
    }
}
