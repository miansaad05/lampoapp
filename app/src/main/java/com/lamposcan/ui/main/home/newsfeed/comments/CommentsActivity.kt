package com.lamposcan.ui.main.home.newsfeed.comments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.ImageView
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.CommentModel
import com.lamposcan.data.model.api.Image
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.data.remote.ApiEndPoints
import com.lamposcan.databinding.CommentsActivityBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.newsfeed.adapter.MultiPostPagerAdapter
import com.lamposcan.ui.main.home.newsfeed.comments.adapter.CommentsAdapter
import com.lamposcan.ui.main.home.newsfeed.fullscreenview.FullScreenActivity
import com.lamposcan.ui.main.home.newsfeed.reportpost.ReportPostDialog
import com.lamposcan.ui.main.qrcode.posts.PostQrDialog
import com.lamposcan.utils.NetworkUtils
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class CommentsActivity : BaseActivity<CommentsActivityBinding, CommentsViewModel>(),
    CommentsNavigator, CommentsAdapter.OnItemClickListener, HasSupportFragmentInjector,
    ReportPostDialog.Callback, MultiPostPagerAdapter.ClickListener {

    private lateinit var binding: CommentsActivityBinding
    @Inject
    lateinit var viewModel: CommentsViewModel

    @Inject
    protected lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    @Inject
    lateinit var commentsAdapter: CommentsAdapter

    private lateinit var postModel: PostModel

    companion object {
        fun newIntent(context: Context, postModel: String) =
            run {
                val intent = Intent(context, CommentsActivity::class.java)
                intent.putExtra("postModel", postModel)
            }
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun openFullScreen(moviesList: MutableList<Image>?) {
        startActivity(context.let {
            FullScreenActivity.newIntent(
                it,
                Gson().toJson(postModel)
            )
        })
    }

    override fun layoutId(): Int {
        return R.layout.comments_activity
    }

    override fun viewModel(): CommentsViewModel {
        viewModel = ViewModelProviders.of(this, factory).get(CommentsViewModel::class.java)
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userModel = getUserModelFromPref()

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this


        if (TextUtils.isEmpty(userModel.profile?.profileImage)) {
            viewModel.userProfileImage.set("")
        } else
            viewModel.userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + userModel.profile?.profileImage)

        commentsAdapter.setListener(this)

        intent.hasExtra("postModel").run {
            postModel = Gson().fromJson(intent.getStringExtra("postModel"), PostModel::class.java)

            setPostInfo(postModel)

            if (TextUtils.isEmpty(postModel.user.profile?.profileImage)) {
                viewModel.postUserImage.set("")
            } else
                viewModel.postUserImage.set(ApiEndPoints.IMAGE_BASE_URL + postModel.user.profile?.profileImage)

            if (NetworkUtils.isNetworkConnected(context)) {
                showLoading()
                viewModel.fetchPostComments(userModel.apiToken, postModel.id)
            } else {
                showMessage(getString(R.string.no_internet))
            }
        }

        binding.rvComments.adapter = commentsAdapter
    }

    fun setPostInfo(postModel: PostModel) {
        this.postModel = postModel

        hideLoading()
        viewModel.userName.set(postModel.user.fname + " " + postModel.user.lname)
        viewModel.commentsCounter.set(ApiEndPoints.IMAGE_BASE_URL + postModel.user.profile?.profileImage)

        viewModel.postTitle.set(postModel.title)
        viewModel.postDescription.set(postModel.description)
        viewModel.postTime.set(postModel.createdAt)

        when {
            postModel.images != null && postModel.images.size > 1 -> viewModel.isMultiPost.set(true)
            else -> viewModel.isMultiPost.set(false)
        }

        if (postModel.images != null && postModel.images.size > 0) {
            viewModel.isImages.set(true)
            val adapter = MultiPostPagerAdapter(context, postModel.images)
            adapter.setListener(this)
            binding.viewPager.adapter = adapter
            binding.tabsIndicator.attachTo(binding.viewPager)
        } else {
            viewModel.isImages.set(false)
        }

        when {
            postModel.totalLikes == 0 ->
                viewModel.likeCounter.set("0")
            else -> viewModel.likeCounter.set(postModel.totalLikes.toString())
        }
        when {
            postModel.totalShares == 0 ->
                viewModel.sharePostCounter.set("0")
            else -> viewModel.sharePostCounter.set(postModel.totalShares.toString())
        }

        when {
            postModel.totalComments == 0 ->
                viewModel.commentsCounter.set("0")
            else -> viewModel.commentsCounter.set(postModel.totalComments.toString())
        }

        if (!TextUtils.isEmpty(postModel.user.profile?.city) && !TextUtils.isEmpty(postModel.user.profile?.country))
            viewModel.userLocation.set(postModel.user.profile?.city + ", " + postModel.user.profile?.country)
        else if (!TextUtils.isEmpty(postModel.user.profile?.city)) {
            viewModel.userLocation.set(postModel.user.profile?.city)
        } else {
            viewModel.userLocation.set(postModel.user.profile?.country)
        }

        if (postModel.isLikedByAuthUser == 0) {
            viewModel.likedIcon.set(R.drawable.ic_like)
        } else {
            viewModel.likedIcon.set(R.drawable.ic_like_green)
        }

        viewModel.isDataLoad.set(true)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    override fun getFields() {
        if (NetworkUtils.isNetworkConnected(context)) {
            showLoading()
            viewModel.addComment(
                userModel.apiToken,
                postModel.id,
                binding.etUserComment.text.toString().trim()
            )
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun addComments(comment: CommentModel) {
        binding.etUserComment.setText("")
        viewModel.isRefreshing.set(false)
        hideLoading()
        commentsAdapter.addItems(comment)
    }

    override fun hideLoader() {
        hideLoading()
    }

    override fun setComments(commentList: List<CommentModel>) {
        viewModel.isRefreshing.set(false)
        hideLoading()
        commentList.let { commentsAdapter.addItems(it, false) }
    }

    override fun onRefresh() {
        if (NetworkUtils.isNetworkConnected(context)) {
            commentsAdapter.clearItems()
            viewModel.fetchPostComments(userModel.apiToken, postModel.id)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    private var adapterPosition: Int = 0
    override fun onCommentLikeClicked(model: CommentModel) {
        adapterPosition = commentsAdapter.list.indexOf(model)

        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
//            if (userModel.id == model.commentUser.id) {
            viewModel.onLikeClick(userModel.apiToken, model.id)
//            } else {
//                viewModel.onUnLikeClick(userModel.apiToken, model.id)
//            }
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun onLikeResponse(model: CommentModel) {
        hideLoader()
        commentsAdapter.refreshLikeCount(model, adapterPosition)
    }

    override fun showLoader() {
        showLoading()
    }

    override fun onQrCodeClick() {
        val postQrCode =
            PostQrDialog.newInstance(Gson().toJson(postModel).toString())
//        profileQrCodeDialog.setListener(this)
        postQrCode.show(supportFragmentManager)
    }

    override fun onOptionsMenuClick() {
        if (TextUtils.equals(userModel.id.toString(), postModel.user.id.toString())) {
            showMyPostMenu(postModel, binding.ivOptions)
        } else {
            showOtherPostMenu(postModel, binding.ivOptions)
        }
    }

    private fun showMyPostMenu(postModel: PostModel, ivOptions: ImageView) {
        val wrapper = ContextThemeWrapper(context, R.style.popupMenuStyle)
        val popup = PopupMenu(wrapper, ivOptions)
        //Inflating the Popup using xml file
        popup.menuInflater.inflate(R.menu.my_post_menu, popup.menu)

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_delete_post -> {
                    if (NetworkUtils.isNetworkConnected(context)) {
                        showLoader()
                        viewModel.onDeleteMyPost(userModel.apiToken, postModel.id)
                    } else {
                        showMessage(getString(R.string.no_internet))
                    }
                    true
                }
                else -> true
            }
        }

        popup.show()
    }

    private fun showOtherPostMenu(postModel: PostModel, ivOptions: ImageView) {
        val wrapper = ContextThemeWrapper(context, R.style.popupMenuStyle)
        val popup = PopupMenu(wrapper, ivOptions)
        //Inflating the Popup using xml file
        popup.menuInflater.inflate(R.menu.other_post_menu, popup.menu)

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_report_post -> {
                    val reportPostDialog =
                        ReportPostDialog.newInstance(Gson().toJson(postModel).toString())
                    reportPostDialog.setListener(this)
                    reportPostDialog.show(supportFragmentManager)
                    true
                }
                else -> true
            }
        }

        popup.show()
    }

    override fun onReportAndDeleteSuccessFully() {
        hideLoader()
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onSubmitClick(tagName: String, description: String, postModel: String) {

        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.onReport(userModel.apiToken, this.postModel.id, tagName, description)
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }
}