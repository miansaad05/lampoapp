package com.lamposcan.ui.main.home.newsfeed.adapter

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.lamposcan.R
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.data.remote.ApiEndPoints

class PostsItemViewModel(post: PostModel, listener: Listeners) : BaseItemViewModel(post, listener) {
    val isMultiPost: ObservableBoolean = ObservableBoolean()
    val postImageUrl: ObservableField<String> = ObservableField()
    val postDescription: ObservableField<String> = ObservableField()
    val postTitle: ObservableField<String> = ObservableField()
    val placeHolder: ObservableInt = ObservableInt(R.drawable.ic_error)
    val errorHolder: ObservableInt = ObservableInt(R.drawable.ic_error)

    init {
        userName.set(postModel.user.fname + " " + postModel.user.lname)
        userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + postModel.user.profile?.profileImage)

        postTitle.set(postModel.title)
        postDescription.set(postModel.description)
        postTime.set(postModel.createdAt)

        if (postModel.isLikedByAuthUser == 0) {
            likedIcon.set(R.drawable.ic_like)
        } else {
            likedIcon.set(R.drawable.ic_like_green)
        }
        when {
            postModel.images != null && postModel.images.size > 1 -> isMultiPost.set(true)
            else -> isMultiPost.set(false)
        }

        if (postModel.images != null && postModel.images.size > 0) {
            postImageUrl.set(ApiEndPoints.IMAGE_BASE_URL + postModel.images.get(0).postImage)
        }

        when {
            postModel.totalLikes == 0 ->
                likeCounter.set("0")
            else -> likeCounter.set(postModel.totalLikes.toString())
        }
        when {
            postModel.totalShares == 0 ->
                sharePostCounter.set("0")
            else -> sharePostCounter.set(postModel.totalShares.toString())
        }

        when {
            postModel.totalComments == 0 ->
                commentsCounter.set("0")
            else -> commentsCounter.set(postModel.totalComments.toString())
        }

        if (!TextUtils.isEmpty(postModel.user.profile?.city) && !TextUtils.isEmpty(postModel.user.profile?.country))
            userLocation.set(postModel.user.profile?.city + ", " + postModel.user.profile?.country)
        else if (!TextUtils.isEmpty(postModel.user.profile?.city)) {
            userLocation.set(postModel.user.profile?.city)
        } else {
            userLocation.set(postModel.user.profile?.country)
        }
    }
}

