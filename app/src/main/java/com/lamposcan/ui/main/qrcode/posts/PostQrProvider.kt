package com.lamposcan.ui.main.qrcode.posts

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PostQrProvider {
    @ContributesAndroidInjector(modules = [PostQrModule::class])
    abstract fun providePostQrFactory(): PostQrDialog
}