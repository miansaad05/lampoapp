package com.lamposcan.ui.main.home.navigation.notifications

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.navigation.notifications.adapter.NotificationsPagerAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class NotificationsModule : BaseActivityModule<NotificationsViewModel>() {
    @Provides
    fun provideScanHistoryViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): NotificationsViewModel {
        return NotificationsViewModel(dataManager, schedulerProvider)
    }

    @Provides
    fun provideFollowFollowingPagerAdapter(context: NotificationsActivity): NotificationsPagerAdapter {
        return NotificationsPagerAdapter(context, context.supportFragmentManager)
    }
}
