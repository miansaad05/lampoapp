package com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class FriendRequestsViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<FriendRequestsNavigator>(dataManager, mSchedulerProvider) {
    val isRefreshing = ObservableBoolean(false)

    fun onRefresh() {
        isRefreshing.set(true)
        navigator?.onRefresh()
    }

    fun getUserRequests(apiToken: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().getUserRequests("Bearer $apiToken")
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val listType = object : TypeToken<ArrayList<UserModel>>() {}.type
                                val usersList = Gson().fromJson<List<UserModel>>(
                                    Gson().toJson(it.data), listType
                                )
                                navigator?.setUserRequests(usersList)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )

    }


    fun acceptRequest(apiToken: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().acceptRequest("Bearer $apiToken", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let {
                            navigator?.deleteListItem(it.message)
                        }
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )

    }

    fun rejectRequest(apiToken: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().rejectRequest("Bearer $apiToken", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let {
                            navigator?.deleteListItem(it.message)
                        }
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )

    }
}