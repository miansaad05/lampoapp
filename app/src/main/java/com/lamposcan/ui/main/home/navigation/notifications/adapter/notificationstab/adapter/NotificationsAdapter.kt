package com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.api.NotificationModel
import com.lamposcan.databinding.ItemNotificationsBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class NotificationsAdapter(val context: Context?) : RecyclerView.Adapter<BaseViewHolder>(),
    AdapterUpdateListener {
    var list: MutableList<NotificationModel> = ArrayList()
    private var listener: OnClickListener? = null


    fun setListener(listener: OnClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemNotificationsBinding.inflate(
            LayoutInflater.from(context), parent, false
        )
        return NotificationViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    interface OnClickListener

    override fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun addItems(obj: Any) {
        list.add(0, obj as NotificationModel)
        notifyItemInserted(0)
    }

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {
        list.addAll(items as Collection<NotificationModel>)
        notifyDataSetChanged()
    }

    open inner class NotificationViewHolder(val binding: ItemNotificationsBinding) :
        BaseViewHolder(binding.root) {
        private lateinit var itemViewModel: NotificationsItemViewModel
        override fun onBind(position: Int) {
            itemViewModel = NotificationsItemViewModel(list[position])
            binding.viewModel = itemViewModel
            binding.executePendingBindings()
        }

    }
}