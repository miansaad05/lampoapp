package com.lamposcan.ui.main.qrcode.profile
import com.lamposcan.ui.base.BaseNavigator

interface ProfileQrNavigator : BaseNavigator {
    fun onSubmitClick()
}
