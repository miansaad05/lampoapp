package com.lamposcan.ui.main.home.navigation.friendslist.unfollowdialog

import com.lamposcan.ui.base.BaseNavigator

interface UnFollowNavigator : BaseNavigator {
    fun onSubmitClick()
    fun onCancelClick()
}
