package com.lamposcan.ui.main.home.addpost

import android.text.TextUtils
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.AppLogger
import com.lamposcan.utils.interfaces.ToolbarMenuHelper
import com.lamposcan.utils.rx.SchedulerProvider
import java.io.File

class AddPostViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<AddPostNavigator>(dataManager, schedulerProvider) {
    var title: String = ""
    var description: String = ""
    var isPublic: Int = 0
    val files = ArrayList<File>()

    var mToolbarMenuHelper: ToolbarMenuHelper = object : ToolbarMenuHelper {
        override fun onBackClick() {
            navigator?.onBack()
        }

        override fun onMenuItem() {
            navigator?.onMenuClicked()
        }

    }

    fun onSubmitPost() {
        navigator?.getFields()
    }

    fun submitFields(apiToken: String) {
        when {
            files.size == 0 && TextUtils.isEmpty(title) && TextUtils.isEmpty(description) -> {
                navigator?.showMessage("Please enter post information or attach any media")
                return
            }
            else -> {
                navigator?.showLoader()
                hitCreatePost(apiToken)
            }
        }
    }

    private fun hitCreatePost(apiToken: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().createPost(
                title, description, "Bearer $apiToken", isPublic, files
            )
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        navigator?.showMessage(it.message)
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {
                                /*val jsonObject =
                                    object : TypeToken<ProfileImageUpdateModel>() {}.type
                                val profileImageUpdateModel =
                                    Gson().fromJson<ProfileImageUpdateModel>(
                                        Gson().toJson(it.data), jsonObject
                                    )*/

                                AppLogger.d(
                                    "asad_create_post",
                                    "post = " + Gson().toJson(it)
                                )
                                navigator?.onResponse()
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError
                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")

                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }
}
