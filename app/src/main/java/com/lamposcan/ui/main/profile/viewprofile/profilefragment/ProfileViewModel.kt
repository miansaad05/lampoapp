package com.lamposcan.ui.main.profile.viewprofile.profilefragment

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider
import java.util.*

class ProfileViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<ProfileNavigator>(dataManager, mSchedulerProvider) {

    val postImageUrl: ObservableField<String> = ObservableField("")
    val btnText: ObservableField<String> = ObservableField("")
    val userName: ObservableField<String> = ObservableField("")
    val userLocation: ObservableField<String> = ObservableField("")
    val isMyProfile = ObservableBoolean()
    val isFollowing = ObservableBoolean()
    val isFollowRequestSent = ObservableBoolean()
    val isPendingRequest = ObservableBoolean()
    val isRefreshing = ObservableBoolean(false)
    val isDataShown = ObservableBoolean()
    val isPublicProfile = ObservableBoolean()

    fun onRefresh() {
        isRefreshing.set(true)
        navigator?.onRefresh()
    }

    fun getUserProfile(apiToken: String?, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().getUserProfile("Bearer $apiToken", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {
                                val jsonObject = object : TypeToken<UserModel>() {}.type
                                val userModel = Gson().fromJson<UserModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.setUserInfo(userModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun getUserPosts(apiToken: String?, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().getUserPost("Bearer $apiToken", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val listType = object : TypeToken<ArrayList<PostModel>>() {}.type
                                val posts = Gson().fromJson<List<PostModel>>(
                                    Gson().toJson(it.data), listType
                                )
                                navigator?.setPosts(posts)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onDeleteMyPost(token: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().deletePost("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        navigator?.onReportAndDeleteSuccessFully()
                        navigator?.showMessage(it.message)
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onFollowClick() {
        val id = navigator?.getVisitedUserId()
        val apiToken = navigator?.getUserToken()
        val checkIsNetworkConnected: Boolean? = navigator?.isNetworkConnected()

        checkIsNetworkConnected?.let {
            if (it) {
                id?.let {
                    compositeDisposable.add(
                        dataManager.getApiHelper().addFriend("Bearer $apiToken", it)
                            .subscribeOn(getmSchedulerProvider().io()).observeOn(
                                getmSchedulerProvider().ui()
                            )
                            .subscribe({
                                if (it.code == 200) {
                                    it?.let {
                                        navigator?.hideLoader()
                                        navigator?.showMessage(it.message)
                                        navigator?.refreshPage()

                                    }
                                } else {
                                    navigator?.hideLoader()
                                    navigator?.onHandleError(it.message)
                                }
                            }, {
                                navigator?.hideLoader()
                                if (it != null) {
                                    val error = it as ANError

                                    if (!TextUtils.isEmpty(error.errorBody))
                                        navigator?.onHandleError(error.errorBody)
                                    else
                                        navigator?.onHandleError("No Internet Connection Available")


                                } else {
                                    navigator?.onHandleError("No Internet Connection Available")
                                }
                            })
                    )
                }
            }
        }
    }

    fun onSentCancelRequest() {
        val id = navigator?.getVisitedUserId()
        val apiToken = navigator?.getUserToken()
        val checkIsNetworkConnected: Boolean? = navigator?.isNetworkConnected()

        checkIsNetworkConnected?.let {
            if (it) {
                id?.let {
                    compositeDisposable.add(
                        dataManager.getApiHelper().rejectRequest("Bearer $apiToken", it)
                            .subscribeOn(getmSchedulerProvider().io()).observeOn(
                                getmSchedulerProvider().ui()
                            )
                            .subscribe({
                                if (it.code == 200) {
                                    it?.let {
                                        navigator?.hideLoader()
                                        navigator?.showMessage(it.message)
                                    }
                                } else {
                                    navigator?.hideLoader()
                                    navigator?.onHandleError(it.message)
                                }
                            }, {
                                navigator?.hideLoader()
                                if (it != null) {
                                    val error = it as ANError

                                    if (!TextUtils.isEmpty(error.errorBody))
                                        navigator?.onHandleError(error.errorBody)
                                    else
                                        navigator?.onHandleError("No Internet Connection Available")


                                } else {
                                    navigator?.onHandleError("No Internet Connection Available")
                                }
                            })
                    )
                }
            }
        }
    }

    fun onUnFollowClick() {
        val id = navigator?.getVisitedUserId()
        val apiToken = navigator?.getUserToken()
        val checkIsNetworkConnected: Boolean? = navigator?.isNetworkConnected()

        checkIsNetworkConnected?.let {
            if (it) {
                id?.let {
                    compositeDisposable.add(
                        dataManager.getApiHelper().unFollowUser("Bearer $apiToken", it)
                            .subscribeOn(getmSchedulerProvider().io()).observeOn(
                                getmSchedulerProvider().ui()
                            )
                            .subscribe({
                                if (it.code == 200) {
                                    it?.let {
                                        navigator?.hideLoader()
                                        navigator?.showMessage(it.message)
                                        navigator?.refreshPage()
                                    }
                                } else {
                                    navigator?.hideLoader()
                                    navigator?.onHandleError(it.message)
                                }
                            }, {
                                navigator?.hideLoader()
                                if (it != null) {
                                    val error = it as ANError

                                    if (!TextUtils.isEmpty(error.errorBody))
                                        navigator?.onHandleError(error.errorBody)
                                    else
                                        navigator?.onHandleError("No Internet Connection Available")


                                } else {
                                    navigator?.onHandleError("No Internet Connection Available")
                                }
                            })
                    )
                }
            }
        }
    }

    fun onAcceptRequestClick() {
        val id = navigator?.getVisitedUserId()
        val apiToken = navigator?.getUserToken()
        val checkIsNetworkConnected: Boolean? = navigator?.isNetworkConnected()

        checkIsNetworkConnected?.let {
            if (it) {
                id?.let {
                    compositeDisposable.add(
                        dataManager.getApiHelper().acceptRequest("Bearer $apiToken", it)
                            .subscribeOn(getmSchedulerProvider().io()).observeOn(
                                getmSchedulerProvider().ui()
                            )
                            .subscribe({
                                if (it.code == 200) {
                                    it?.let {
                                        navigator?.hideLoader()
                                        navigator?.showMessage(it.message)
                                        navigator?.refreshPage()
                                    }
                                } else {
                                    navigator?.hideLoader()
                                    navigator?.onHandleError(it.message)
                                }
                            }, {
                                navigator?.hideLoader()
                                if (it != null) {
                                    val error = it as ANError

                                    if (!TextUtils.isEmpty(error.errorBody))
                                        navigator?.onHandleError(error.errorBody)
                                    else
                                        navigator?.onHandleError("No Internet Connection Available")


                                } else {
                                    navigator?.onHandleError("No Internet Connection Available")
                                }
                            })
                    )
                }
            }
        }

    }

    fun onRejectRequestClick() {

        val id = navigator?.getVisitedUserId()
        val apiToken = navigator?.getUserToken()
        val checkIsNetworkConnected: Boolean? = navigator?.isNetworkConnected()

        checkIsNetworkConnected?.let {
            id?.let {
                compositeDisposable.add(
                    dataManager.getApiHelper().rejectRequest("Bearer $apiToken", it)
                        .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                        .subscribe({
                            if (it.code == 200) {
                                it?.let {
                                    navigator?.hideLoader()
                                    navigator?.showMessage(it.message)
                                    navigator?.refreshPage()
                                }
                            } else {
                                navigator?.hideLoader()
                                navigator?.onHandleError(it.message)
                            }
                        }, {
                            navigator?.hideLoader()
                            if (it != null) {
                                val error = it as ANError

                                if (!TextUtils.isEmpty(error.errorBody))
                                    navigator?.onHandleError(error.errorBody)
                                else
                                    navigator?.onHandleError("No Internet Connection Available")


                            } else {
                                navigator?.onHandleError("No Internet Connection Available")
                            }
                        })
                )
            }
        }
    }

    fun onQrCodeClick() {
        navigator?.onQrCodeClicked()
    }


    fun getScannedUserInfo(apiToken: String?, qrCodeType: String, qrCode: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().getScanResult("Bearer $apiToken", qrCodeType, qrCode)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {
                                val jsonObject = object : TypeToken<UserModel>() {}.type
                                val userModel = Gson().fromJson<UserModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.setUserInfo(userModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }
}

