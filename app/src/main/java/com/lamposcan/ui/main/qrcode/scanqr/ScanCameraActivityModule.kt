package com.lamposcan.ui.main.qrcode.scanqr

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class ScanCameraActivityModule : BaseActivityModule<ScanCameraViewModel>() {
    @Provides
    fun provideSplashViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): ScanCameraViewModel {
        return ScanCameraViewModel(dataManager, schedulerProvider)
    }
}
