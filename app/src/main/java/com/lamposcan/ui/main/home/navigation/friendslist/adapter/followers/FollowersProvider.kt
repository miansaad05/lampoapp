package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FollowersProvider {
    @ContributesAndroidInjector(modules = [FollowersModule::class])
    abstract fun provideFollowersFactory() : FollowersFragment
}