package com.lamposcan.ui.main.home.addpost.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.others.PostImagesModel
import com.lamposcan.databinding.ItemAddPostImageBinding
import com.lamposcan.databinding.ItemPostImagesBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class PostImagesAdapter(private val selectedImageList: MutableList<PostImagesModel>) :
    RecyclerView.Adapter<BaseViewHolder>(), AdapterUpdateListener {
    private var mListener: ItemClickListener? = null

    companion object {

        const val ADD_IMAGE_HOLDER = 1
        const val IMAGE_HOLDER = 2
    }

    fun getList(): MutableList<PostImagesModel> {
        return selectedImageList
    }

    fun getListSize() = selectedImageList.size

    override fun getItemViewType(position: Int): Int {
        if (selectedImageList[position].isUrlEmpty) {
            return ADD_IMAGE_HOLDER
        } else {
            return IMAGE_HOLDER
        }
    }

    fun setListener(mListener: ItemClickListener) {
        this.mListener = mListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        when (viewType) {
            ADD_IMAGE_HOLDER -> {
                val binding = ItemAddPostImageBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
                return AddImageViewHolder(binding)
            }
            IMAGE_HOLDER -> {
                val binding = ItemPostImagesBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
                return PostImageViewHolder(binding)
            }
            else -> {
                val binding = ItemAddPostImageBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
                return AddImageViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(baseViewHolder: BaseViewHolder, position: Int) {
        when (baseViewHolder.itemViewType) {
            ADD_IMAGE_HOLDER -> {
                (baseViewHolder as AddImageViewHolder).onBind(position)
            }
            IMAGE_HOLDER -> {
                (baseViewHolder as PostImageViewHolder).onBind(position)
            }
        }
    }

    override fun getItemCount(): Int {
//        return if (selectedImageList.size > 0) {
        if (selectedImageList.size == 0) return 1
        else return selectedImageList.size
//        } else if (selectedImageList.size == 0) {
//            ADD_IMAGE_HOLDER
//        } else ADD_IMAGE_HOLDER
    }

    override fun clearItems() {
        selectedImageList.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        selectedImageList.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun addItems(`object`: Any) {}

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {

        val size = selectedImageList.size
        selectedImageList.addAll(0, items as Collection<PostImagesModel>)

        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun addImage()
    }

    inner class AddImageViewHolder(private val binding: ItemAddPostImageBinding) :
        BaseViewHolder(binding.root) {
        override fun onBind(position: Int) {
            val itemViewModel = AddImageItemViewModel()
            binding.viewModel = itemViewModel

            binding.parent.setOnClickListener {
                mListener?.addImage()
            }
        }
    }

    inner class PostImageViewHolder(private val itemBinding: ItemPostImagesBinding) :
        BaseViewHolder(itemBinding.root) {

        override fun onBind(position: Int) {
            val itemViewModel = PostImagesItemViewModel(selectedImageList[position])
            itemBinding.viewModel = itemViewModel
            itemBinding.executePendingBindings()
        }
    }
}
