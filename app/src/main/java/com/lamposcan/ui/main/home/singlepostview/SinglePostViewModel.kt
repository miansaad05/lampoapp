package com.lamposcan.ui.main.home.singlepostview

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.R
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.interfaces.ToolbarMenuHelper
import com.lamposcan.utils.rx.SchedulerProvider

class SinglePostViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<SinglePostNavigator>(dataManager, schedulerProvider) {

    val isDataLoad: ObservableBoolean = ObservableBoolean()
    val isMultiPost: ObservableBoolean = ObservableBoolean()
    val postImageUrl: ObservableField<String> = ObservableField("")
    val postDescription: ObservableField<String> = ObservableField("")
    val postTitle: ObservableField<String> = ObservableField("")
    val placeHolder: ObservableInt = ObservableInt(R.drawable.ic_error)
    val errorHolder: ObservableInt = ObservableInt(R.drawable.ic_error)
    val postTime: ObservableField<String> = ObservableField("")
    val likedIcon: ObservableInt = ObservableInt()

    val userName: ObservableField<String> = ObservableField("")
    val userProfileImage: ObservableField<String> = ObservableField("")
    val userLocation: ObservableField<String> = ObservableField("")
    val likesThumb: ObservableField<String> = ObservableField("")
    val likeCounter: ObservableField<String> = ObservableField("")
    val commentsCounter: ObservableField<String> = ObservableField("")
    val sharePostCounter: ObservableField<String> = ObservableField("")

    var mToolbarMenuHelper: ToolbarMenuHelper = object : ToolbarMenuHelper {
        override fun onBackClick() {
            navigator?.onBack()
        }

        override fun onMenuItem() {
        }

    }

    fun getPostInfo(apiToken: String?, qrCodeType: String, qrCode: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().getScanResult("Bearer $apiToken", qrCodeType, qrCode)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {
                                val jsonObject = object : TypeToken<PostModel>() {}.type
                                val postModel = Gson().fromJson<PostModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.setPostInfo(postModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onLikeClick() {
        navigator?.onLikeClick()
    }

    fun openLikeClick() {
        navigator?.openLikeClick()
    }

    fun onCommentClick() {
        navigator?.onCommentClick()
    }

    fun hitLikeApi(token: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().likePosts("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val jsonObject = object : TypeToken<PostModel>() {}.type

                                val postModel = Gson().fromJson<PostModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.onLikeResponse(postModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun hitUnLikeApi(token: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().unLikePost("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val jsonObject = object : TypeToken<PostModel>() {}.type

                                val postModel = Gson().fromJson<PostModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.onLikeResponse(postModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }
}
