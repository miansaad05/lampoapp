package com.lamposcan.ui.main.home.addpost

import com.lamposcan.data.DataManager
import com.lamposcan.data.model.others.PostImagesModel
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.addpost.adapter.PostImagesAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class AddPostActivityModule : BaseActivityModule<AddPostViewModel>() {
    @Provides
    fun provideSplashViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): AddPostViewModel {
        return AddPostViewModel(dataManager, schedulerProvider)
    }

    @Provides
    fun provideAddImagesPostAdapter(): PostImagesAdapter {
        return PostImagesAdapter(ArrayList<PostImagesModel>())
    }
}
