package com.lamposcan.ui.main.home.navigation.terms

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class TermsAndConditionsActivityModule : BaseActivityModule<TermsAndConditionsViewModel>() {
    @Provides
    fun provideTermsAndConditionsViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): TermsAndConditionsViewModel {
        return TermsAndConditionsViewModel(dataManager, schedulerProvider)
    }
}
