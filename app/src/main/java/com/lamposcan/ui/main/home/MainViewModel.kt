package com.lamposcan.ui.main.home

import androidx.databinding.ObservableField
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.interfaces.MoreOptionsToolbarMenuHelper
import com.lamposcan.utils.rx.SchedulerProvider

class MainViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<MainNavigator>(dataManager, schedulerProvider) {

    val userName: ObservableField<String> = ObservableField("")
    val imageUrl: ObservableField<String> = ObservableField("")

    var mToolbarMenuHelper: MoreOptionsToolbarMenuHelper = object : MoreOptionsToolbarMenuHelper {

        override fun onOpenDrawer() {
            navigator?.openDrawerMenu()
        }

        override fun onBackClick() {
            navigator?.onBack()
        }

        override fun onMenuItem() {
            navigator?.openDiscoverActivity()
        }

        override fun onMenuItem1() {

        }

        override fun onMenuItem2() {
            navigator?.openNotificationsActivity()
        }
    }

    fun onLogoutClick() {
        navigator?.onLogoutClicked()
    }

    fun onEditProfileClick() {
        navigator?.onEditProfileClicked()
    }
}
