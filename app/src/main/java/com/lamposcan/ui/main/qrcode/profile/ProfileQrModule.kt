package com.lamposcan.ui.main.qrcode.profile

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class ProfileQrModule : BaseActivityModule<ProfileQrViewModel>() {
    @Provides
    fun provideProfileQrViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): ProfileQrViewModel {
        return ProfileQrViewModel(dataManager, mSchedulerProvider)
    }
}