package com.lamposcan.ui.main.home.newsfeed.fullscreenview

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.newsfeed.fullscreenview.FullScreenViewModel
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class FullScreenActivityModule : BaseActivityModule<FullScreenViewModel>() {
    @Provides
    fun provideFullScreenViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): FullScreenViewModel {
        return FullScreenViewModel(dataManager, schedulerProvider)
    }
}
