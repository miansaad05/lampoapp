package com.lamposcan.ui.main.home

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etebarian.meowbottomnavigation.MeowBottomNavigation
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.others.NavigationDrawerModel
import com.lamposcan.data.remote.ApiEndPoints
import com.lamposcan.databinding.ActivityMainBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.adapter.NavigationDrawerAdapter
import com.lamposcan.ui.main.home.addpost.AddPostActivity
import com.lamposcan.ui.main.home.discover.DiscoverActivity
import com.lamposcan.ui.main.home.navigation.friendslist.FollowFollowingsActivity
import com.lamposcan.ui.main.home.navigation.notifications.NotificationsActivity
import com.lamposcan.ui.main.home.navigation.scanhistory.ScanHistoryActivity
import com.lamposcan.ui.main.home.navigation.setting.SettingsActivity
import com.lamposcan.ui.main.home.navigation.terms.TermsAndConditionsActivity
import com.lamposcan.ui.main.home.newsfeed.ConversationFragment
import com.lamposcan.ui.main.home.newsfeed.NewsFeedFragment
import com.lamposcan.ui.main.profile.editprofile.EditProfileActivity
import com.lamposcan.ui.main.profile.viewprofile.profilefragment.ProfileFragment
import com.lamposcan.ui.main.qrcode.scanqr.ScanCameraActivity
import com.lamposcan.ui.registeration.login.LoginActivity
import com.lamposcan.utils.MessageAlert
import com.lamposcan.utils.RequestCodes
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import net.quikkly.android.Quikkly
import net.quikkly.core.QuikklyCore
import javax.inject.Inject


class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), MainNavigator,
    HasSupportFragmentInjector, NavigationDrawerAdapter.OnClickListener {

    @Inject
    protected lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var binding: ActivityMainBinding
    @Inject
    lateinit var viewModel: MainViewModel
    @Inject
    lateinit var navAdapter: NavigationDrawerAdapter

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    lateinit var newsFeedFragment: NewsFeedFragment
    lateinit var conversationFragment: ConversationFragment
    lateinit var profileFragment: ProfileFragment

    companion object {
        fun newIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    object NAV_CONSTANTS {
        const val HOME = 1
        const val HOME_ADD_POST = 2
        const val HOME_SCAN = 3
        const val HOME_CONVERSATION = 4
        const val HOME_PROFILE = 5

        val NEWS_FEED_TAG = NewsFeedFragment::class.simpleName
        val CONVERSATION_TAG = ConversationFragment::class.simpleName
        val PROFILE_TAG = ProfileFragment::class.simpleName


        const val NAV_LAMPOSCAN = 10
        const val NAV_FRIENDS_LIST = 11
        const val NAV_SCAN_HISTORY = 12
        const val NAV_SETTINGS = 13
        const val NAV_TERMS = 14
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_main
    }

    override fun viewModel(): MainViewModel {
        viewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userModel = getUserModelFromPref()

        QuikklyCore.checkLinking()

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this
        navAdapter.setListener(this)

        if (!Quikkly.isConfigured()) {
            Quikkly.configureInstance(this@MainActivity, 2, 0)
            // Quikkly.configureInstance(this, "blueprint_custom.json", 2, 0);
        }

        initViews()
        initFragments()
        setUpBottomNav()

        NAV_CONSTANTS.NEWS_FEED_TAG?.let { addFragment(newsFeedFragment, it) }

        setUpBottomNavListeners()
        setUpDrawer()
        setUpListInNavigation()
    }

    private fun initViews() {
        viewModel.userName.set("${userModel.fname}  ${userModel.lname}")
        if (TextUtils.isEmpty(userModel.profile?.profileImage)) {
            viewModel.imageUrl.set("")
        } else
            viewModel.imageUrl.set(ApiEndPoints.IMAGE_BASE_URL + userModel.profile?.profileImage)
    }

    override fun onLogoutClicked() {
        onSessionExpire()
    }

    override fun onEditProfileClicked() {
        binding.drawerLayout.closeDrawers()
        startActivity(EditProfileActivity.newIntent(context))
    }

    override fun showMessage(message: String) {
        MessageAlert.showErrorMessage(binding.root, message)
    }

    override fun onItemClick(model: NavigationDrawerModel) {
        when (model.id) {
            NAV_CONSTANTS.NAV_LAMPOSCAN -> {
                startActivityForResult(
                    ScanCameraActivity.newInstance(
                        this@MainActivity, 1.0,
                        true, false
                    ), RequestCodes.SCAN_CAMERA
                )
            }
            NAV_CONSTANTS.NAV_FRIENDS_LIST -> {
                startActivity(FollowFollowingsActivity.newIntent(context))
            }
            NAV_CONSTANTS.NAV_SCAN_HISTORY -> {
                startActivity(ScanHistoryActivity.newIntent(context))
            }
            NAV_CONSTANTS.NAV_SETTINGS -> {
                startActivity(SettingsActivity.newIntent(context))
            }
            NAV_CONSTANTS.NAV_TERMS -> {
                startActivity(TermsAndConditionsActivity.newIntent(context))
            }
        }
    }

    private fun addFragment(fragment: Fragment, tag: String) {
        val supportFragmentManager = supportFragmentManager
        supportFragmentManager.beginTransaction().replace(binding.frameContainer.id, fragment, tag)
            .commit()
    }

    private fun initFragments() {
        newsFeedFragment = NewsFeedFragment.newInstance()
        conversationFragment = ConversationFragment.newInstance()
        profileFragment = ProfileFragment.newInstance(userModel)
    }

    override fun openNotificationsActivity() {
        startActivity(NotificationsActivity.newIntent(context))
    }

    override fun openDiscoverActivity() {
        startActivity(DiscoverActivity.newIntent(context))
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    private fun setUpBottomNav() {
        binding.bottomNav.add(
            MeowBottomNavigation.Model(
                NAV_CONSTANTS.HOME,
                R.drawable.home
            )
        )
        binding.bottomNav.add(
            MeowBottomNavigation.Model(
                NAV_CONSTANTS.HOME_ADD_POST,
                R.drawable.icon_add_post
            )
        )
        binding.bottomNav.add(
            MeowBottomNavigation.Model(
                NAV_CONSTANTS.HOME_SCAN,
                R.drawable.icon_scan
            )
        )
        binding.bottomNav.add(
            MeowBottomNavigation.Model(
                NAV_CONSTANTS.HOME_CONVERSATION,
                R.drawable.icon_conversation
            )
        )
        binding.bottomNav.add(
            MeowBottomNavigation.Model(
                NAV_CONSTANTS.HOME_PROFILE,
                R.drawable.icon_profile
            )
        )

        binding.bottomNav.show(NAV_CONSTANTS.HOME)

    }

    private fun setUpBottomNavListeners() {
        binding.bottomNav.setOnClickMenuListener {

            when (it.id) {
                NAV_CONSTANTS.HOME -> {
                    NAV_CONSTANTS.NEWS_FEED_TAG?.let { it1 -> addFragment(newsFeedFragment, it1) }
                }
                NAV_CONSTANTS.HOME_ADD_POST -> {
                    startActivityForResult(
                        AddPostActivity.newIntent(context),
                        RequestCodes.ADD_POST
                    )
                }
                NAV_CONSTANTS.HOME_SCAN -> {
                    startActivityForResult(
                        ScanCameraActivity.newInstance(
                            this@MainActivity, 1.0,
                            true, false
                        ), RequestCodes.SCAN_CAMERA
                    )
                }
                NAV_CONSTANTS.HOME_CONVERSATION -> {
                    NAV_CONSTANTS.CONVERSATION_TAG?.let { it1 ->
                        addFragment(
                            conversationFragment,
                            it1
                        )
                    }
                }
                NAV_CONSTANTS.HOME_PROFILE -> {
                    NAV_CONSTANTS.PROFILE_TAG?.let { it1 ->
                        addFragment(
                            profileFragment,
                            it1
                        )
                    }
                }
            }
        }
    }

    private fun setUpDrawer() {
        binding.drawerLayout.setScrimColor(Color.TRANSPARENT)
        val actionBarDrawerToggle = object : ActionBarDrawerToggle(
            this, binding.drawerLayout,
            R.string.open, R.string.close
        ) {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                val scaleFactor = 6f
                val slideX: Float = drawerView.width * slideOffset
                binding.content.translationX = slideX

                binding.content.scaleX = 1 - (slideOffset / scaleFactor)
                binding.content.scaleY = 1 - (slideOffset / scaleFactor)
            }
        }

        binding.drawerLayout.addDrawerListener(actionBarDrawerToggle)
    }

    override fun openDrawerMenu() {
        binding.drawerLayout.openDrawer(Gravity.LEFT)
    }

    private fun setUpListInNavigation() {

        binding.rvNav.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.rvNav.adapter = navAdapter


        val list = listOf(
            NavigationDrawerModel(
                NAV_CONSTANTS.NAV_LAMPOSCAN,
                R.drawable.icon_lamposcan,
                getString(R.string.name)
            ),
            NavigationDrawerModel(
                NAV_CONSTANTS.NAV_FRIENDS_LIST,
                R.drawable.icon_friends,
                getString(R.string.friends_list)
            ),
            NavigationDrawerModel(
                NAV_CONSTANTS.NAV_SCAN_HISTORY,
                R.drawable.icon_scan_history,
                getString(R.string.scan_history)
            ),
            NavigationDrawerModel(
                NAV_CONSTANTS.NAV_SETTINGS,
                R.drawable.icon_settings,
                getString(R.string.setting)
            ),
            NavigationDrawerModel(
                NAV_CONSTANTS.NAV_TERMS, R.drawable.icon_terms, getString(R.string.terms)
            )
        )

        navAdapter.addData(list.toMutableList())
    }

    override fun decideNextActivity() {
        startActivity(LoginActivity.newIntent(context))
        finishAffinity()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RequestCodes.ADD_POST || requestCode == RequestCodes.SCAN_CAMERA) {
                binding.bottomNav.show(NAV_CONSTANTS.HOME)
                NAV_CONSTANTS.NEWS_FEED_TAG?.let { it1 -> addFragment(newsFeedFragment, it1) }
            }
        }
    }
}
