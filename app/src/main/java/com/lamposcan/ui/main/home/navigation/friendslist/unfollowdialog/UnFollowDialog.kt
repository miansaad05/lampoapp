package com.lamposcan.ui.main.home.navigation.friendslist.unfollowdialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.FollowingsModel
import com.lamposcan.databinding.UnfollowBinding
import com.lamposcan.ui.base.BaseDialog
import com.lamposcan.ui.main.home.navigation.friendslist.FollowFollowingsActivity
import javax.inject.Inject

class UnFollowDialog : BaseDialog<UnfollowBinding, UnFollowViewModel>(), UnFollowNavigator {
    private lateinit var binding: UnfollowBinding
    @Inject
    lateinit var viewModel: UnFollowViewModel

    var model: FollowingsModel? = null

    companion object {
        fun newInstance(followingModel: FollowingsModel): UnFollowDialog {
            val fragment = UnFollowDialog()
            val bundle = Bundle()
            bundle.putString("followingModel", Gson().toJson(followingModel))
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.unfollow
    }

    override fun viewModel(): UnFollowViewModel? {
        return viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        arguments?.let {
            model = Gson().fromJson(it.getString("followingModel"), FollowingsModel::class.java)

            viewModel.unfollowHeader.set("Unfollow ${model?.fname} from your list?")
            viewModel.unfollowFooter.set("Are you sure you want to unfollow ${model?.lname} as your friend?")
        }


        return view
    }

    fun show(fragmentManager: FragmentManager) {
        super.show(fragmentManager, "TAG")
    }


    override fun onSubmitClick() {
        model?.let {
            if (activity is FollowFollowingsActivity) {
                (activity as FollowFollowingsActivity).adapter.followingsFragment.onConfirm(
                    Gson().toJson(
                        it
                    )
                )
            }
        }
        dialog?.dismiss()
    }

    override fun onCancelClick() {
        dialog?.dismiss()
    }
}