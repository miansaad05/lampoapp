package com.lamposcan.ui.main.home.newsfeed.reportpost.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.others.TagModel
import com.lamposcan.databinding.ItemTagsBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class TagsAdapter(val context: Context?) : RecyclerView.Adapter<BaseViewHolder>(),
    AdapterUpdateListener {
    private var list: MutableList<TagModel> = ArrayList()
    private var listener: TagsClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemTagsBinding.inflate(
            LayoutInflater.from(context), parent, false
        )
        return TagViewHolder(binding)
    }

    fun setListener(listener: TagsClickListener) {
        this.listener = listener
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun addItems(obj: Any) {
        list.add(0, obj as TagModel)
        notifyItemInserted(0)
    }

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {
        list.clear()
        list.addAll(items as Collection<TagModel>)
        notifyDataSetChanged()
    }

    interface TagsClickListener {
        fun onTagClick(adapterPosition: Int)
    }

    fun getList(): MutableList<TagModel> {
        return list
    }

    fun getSelectedIndex(): Int {
        for (i in 1 until list.size) {
            if (list[i].isSelected) {
                return i
            }
        }
        return 0
    }

    open inner class TagViewHolder(val binding: ItemTagsBinding) :
        BaseViewHolder(binding.root), TagItemViewModel.TagsClickListener {
        private lateinit var itemViewModel: TagItemViewModel

        override fun onBind(position: Int) {
            itemViewModel = TagItemViewModel(list[position], this)
            binding.viewModel = itemViewModel
            binding.executePendingBindings()
        }

        override fun onTagClick() {
            val position = adapterPosition
            unSelect()
            changeToSelected(position)
        }

        private fun unSelect() {
            for (i in 0 until list.size) {
                list[i].isSelected = false
            }
        }

        private fun changeToSelected(adapterPosition: Int) {
            list[adapterPosition].isSelected = true
            notifyDataSetChanged()
        }
    }

}