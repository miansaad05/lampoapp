package com.lamposcan.ui.main.home.newsfeed.fullscreenview

import com.lamposcan.ui.base.BaseNavigator

interface FullScreenNavigator : BaseNavigator {
    fun showLoader()

    fun hideLoader()
}
