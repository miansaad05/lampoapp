package com.lamposcan.ui.main.home.newsfeed.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.lamposcan.data.model.api.Image;
import com.lamposcan.databinding.ItemMultipostBinding;
import com.lamposcan.utils.interfaces.AdapterUpdateListener;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public class MultiPostPagerAdapter extends PagerAdapter implements AdapterUpdateListener, MultiPostItemViewModel.ClickListener {
    Context mContext;
    private List<Image> moviesList;

    public MultiPostPagerAdapter(Context mContext, List<Image> moviesList) {
        this.mContext = mContext;
        this.moviesList = moviesList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ItemMultipostBinding mBinding;

        MultiPostItemViewModel itemViewModel = new MultiPostItemViewModel(moviesList
                .get(position).getPostImage(), this);
        mBinding = ItemMultipostBinding.inflate(LayoutInflater.from(mContext), container, false);
        mBinding.setViewModel(itemViewModel);

        container.addView(mBinding.getRoot());
        return mBinding.getRoot();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public void openFullScreen() {
        listener.openFullScreen(moviesList);
    }

    private ClickListener listener;

    public void setListener(ClickListener listener) {
        this.listener = listener;
    }

    public interface ClickListener {
        void openFullScreen(List<Image> moviesList);
    }

    @Override
    public int getCount() {
        if (moviesList != null && moviesList.size() > 0) {
            return moviesList.size();
        } else
            return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void clearItems() {

    }

    @Override
    public void removeItem(int position) {

    }

    @Override
    public void addItems(Object object) {

    }

    @Override
    public void addItems(@NotNull Collection<?> items, boolean isLoadMore) {
        moviesList.clear();
        moviesList.addAll((Collection<? extends Image>) items);
        notifyDataSetChanged();
    }
}
