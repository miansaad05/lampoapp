package com.lamposcan.ui.main.home.navigation.setting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ActivitySettingsBinding
import com.lamposcan.ui.base.BaseActivity
import javax.inject.Inject

class SettingsActivity :
    BaseActivity<ActivitySettingsBinding, SettingsViewModel>(),
    SettingsNavigator {

    private lateinit var binding: ActivitySettingsBinding
    @Inject
    lateinit var viewModel: SettingsViewModel

    companion object {
        fun newIntent(context: Context) = Intent(context, SettingsActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_settings
    }

    override fun viewModel(): SettingsViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this
    }
}
