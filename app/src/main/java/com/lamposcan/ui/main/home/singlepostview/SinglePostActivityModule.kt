package com.lamposcan.ui.main.home.singlepostview

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.splash.SplashViewModel
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class SinglePostActivityModule : BaseActivityModule<SinglePostViewModel>() {
    @Provides
    fun provideSinglePostViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): SinglePostViewModel {
        return SinglePostViewModel(dataManager, schedulerProvider)
    }
}
