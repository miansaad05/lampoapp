package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers

import com.lamposcan.data.model.api.FollowersDataModel
import com.lamposcan.ui.base.BaseNavigator

interface FollowersNavigator : BaseNavigator {
    fun showLoader()

    fun hideLoader()

    fun onRefresh()
    fun setFollowersList(followersDataModel: FollowersDataModel)
}
