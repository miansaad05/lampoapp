package com.lamposcan.ui.main.profile.viewprofile

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class ProfileActivityModule : BaseActivityModule<ProfileViewModel>() {
    @Provides
    fun provideSplashViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): ProfileViewModel {
        return ProfileViewModel(dataManager, schedulerProvider)
    }
}
