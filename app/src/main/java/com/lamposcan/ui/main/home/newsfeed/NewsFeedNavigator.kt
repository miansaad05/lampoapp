package com.lamposcan.ui.main.home.newsfeed

import com.lamposcan.data.model.api.PostModel
import com.lamposcan.ui.base.BaseNavigator

interface NewsFeedNavigator : BaseNavigator {
    fun showLoader()

    fun hideLoader()

    fun onRefresh()

    fun onLikeResponse(postModel: PostModel)

    fun onResponse()
    fun setPosts(posts: List<PostModel>?)
    fun onReportAndDeleteSuccessFully()
}
