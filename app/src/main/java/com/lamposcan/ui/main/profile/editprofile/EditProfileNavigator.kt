package com.lamposcan.ui.main.profile.editprofile

import com.lamposcan.data.model.api.ProfileImageUpdateModel
import com.lamposcan.ui.base.BaseNavigator

interface EditProfileNavigator : BaseNavigator {
    fun getFields()

    fun showLoader()

    fun onResponse()

    fun hideLoader()

    fun onResponse(profileImageUpdateModel: ProfileImageUpdateModel)

    fun updateProfilePicClicked()

}
