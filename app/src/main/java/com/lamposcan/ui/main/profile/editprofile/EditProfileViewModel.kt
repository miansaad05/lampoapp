package com.lamposcan.ui.main.profile.editprofile

import android.net.Uri
import android.text.TextUtils
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.ProfileImageUpdateModel
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.CommonUtils
import com.lamposcan.utils.rx.SchedulerProvider
import com.lamposcan.utils.validation.Validator
import com.tramsun.libs.prefcompat.Pref
import java.io.File

class EditProfileViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<EditProfileNavigator>(dataManager, schedulerProvider) {

    val imageUrl: ObservableField<String>
    lateinit var imageUri: Uri
    val fullName: ObservableField<String>
    val firstName: ObservableField<String>
    val lastName: ObservableField<String>
    val email: ObservableField<String>
    val country: ObservableField<String>
    val city: ObservableField<String>
    val dob: ObservableField<String>
    val gender: ObservableInt

    init {
        userModel = ObservableField<UserModel>()
        imageUrl = ObservableField("")
        fullName = ObservableField("")
        firstName = ObservableField("")
        lastName = ObservableField("")
        email = ObservableField("")
        country = ObservableField("")
        city = ObservableField("")
        dob = ObservableField("")
        gender = ObservableInt()
    }

    fun onBack() {
        navigator?.onBack()
    }

    fun onSaveClick() {
        navigator?.getFields()
    }

    fun submitFields() {
        when {
            !Validator.validateName(firstName.get().toString()) -> {
                navigator?.showMessage("Please enter first name")
                return
            }
            !Validator.validateName(lastName.get().toString()) -> {
                navigator?.showMessage("Please enter last name")
                return
            }
            !Validator.validateDob(dob.get().toString()) -> {
                navigator?.showMessage("Please select Date of Birth")
                return
            }
            else -> {
                navigator?.showLoader()
                this.hitUpdateUserApi()
            }
        }
    }

    private fun hitUpdateUserApi() {
        compositeDisposable.add(
            dataManager.getApiHelper().updateUserProfile(
                firstName.get().toString().trim(),
                lastName.get().toString().trim(),
                email.get().toString().trim(),
                country.get().toString().trim(),
                city.get().toString().trim(),
                dob.get().toString().trim(),
                gender.get(), "Bearer ${userModel.get()?.apiToken}"
            )
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        navigator?.showMessage(it.message)
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {
                                val jsonObject = object : TypeToken<UserModel>() {}.type
                                val userModel = Gson().fromJson<UserModel>(
                                    Gson().toJson(it.data), jsonObject
                                )

                                Pref.putString(CommonUtils.USER_MODEL, Gson().toJson(userModel))
                                navigator?.onResponse()
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun updateProfilePicClick() {
        navigator?.updateProfilePicClicked()
    }

    fun updateProfilePicture(imageUrl: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().updateUserProfilePicture(
                File(imageUrl), "Bearer ${userModel.get()?.apiToken}"
            )
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        navigator?.showMessage(it.message)
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {
                                val jsonObject =
                                    object : TypeToken<ProfileImageUpdateModel>() {}.type
                                val profileImageUpdateModel =
                                    Gson().fromJson<ProfileImageUpdateModel>(
                                        Gson().toJson(it.data), jsonObject
                                    )
                                navigator?.onResponse(profileImageUpdateModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }
}
