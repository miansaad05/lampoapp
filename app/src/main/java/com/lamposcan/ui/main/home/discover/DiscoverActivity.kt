package com.lamposcan.ui.main.home.discover

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.databinding.ActivityDiscoverBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.discover.adapter.DiscoverPeopleAdapter
import com.lamposcan.ui.main.profile.viewprofile.ProfileActivity
import com.lamposcan.utils.NetworkUtils
import javax.inject.Inject


class DiscoverActivity : BaseActivity<ActivityDiscoverBinding, DiscoverViewModel>(),
    DiscoverNavigator, DiscoverPeopleAdapter.OnClickListener {

    private lateinit var binding: ActivityDiscoverBinding
    @Inject
    lateinit var viewModel: DiscoverViewModel

    @Inject
    lateinit var peopleAdapter: DiscoverPeopleAdapter

    companion object {
        fun newIntent(context: Context) = Intent(context, DiscoverActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_discover
    }

    override fun viewModel(): DiscoverViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userModel = getUserModelFromPref()

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this


        binding.rvPeople.adapter = peopleAdapter
        peopleAdapter.setListener(this)

        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.fetchPeople(userModel.apiToken)
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }


    override fun onRefresh() {
        if (NetworkUtils.isNetworkConnected(context)) {
            peopleAdapter.clearItems()
            viewModel.fetchPeople(userModel.apiToken)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun setUsersList(usersList: List<UserModel>) {
        viewModel.isRefreshing.set(false)
        hideLoading()
        peopleAdapter.addItems(usersList, false)
    }

    override fun onOpenProfileClick(userModel: UserModel) {
        startActivity(context.let { ProfileActivity.newIntent(it, Gson().toJson(userModel)) })
    }

    override fun hideLoader() {
        hideLoading()
    }

    override fun showLoader() {
        showLoading()
    }
}
