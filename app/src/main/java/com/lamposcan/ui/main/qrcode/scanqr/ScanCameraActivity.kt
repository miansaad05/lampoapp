package com.lamposcan.ui.main.qrcode.scanqr

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.text.TextUtils
import android.widget.Toast
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ActivityScanCameraBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.singlepostview.SinglePostActivity
import com.lamposcan.ui.main.profile.viewprofile.ProfileActivity
import com.lamposcan.utils.AppLogger
import com.lamposcan.utils.RequestCodes
import net.quikkly.android.Quikkly
import net.quikkly.android.ScanResultListener
import net.quikkly.android.ui.CameraPreview
import net.quikkly.core.ScanResult
import java.io.IOException
import javax.inject.Inject

class ScanCameraActivity : BaseActivity<ActivityScanCameraBinding, ScanCameraViewModel>(),
    ScanCameraNavigator, ScanResultListener {

    private lateinit var binding: ActivityScanCameraBinding
    @Inject
    lateinit var viewModel: ScanCameraViewModel

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_scan_camera
    }

    override fun viewModel(): ScanCameraViewModel {
        return viewModel
    }

    internal var handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        // Check that Quikkly is set up before other usage.
        Quikkly.getInstance()

        val scanFragment = ScanFragment()

        // Configure ScanFragment
        scanFragment.setShowResultOverlay(intent.getBooleanExtra(EXTRA_SHOW_OVERLAY, false))
        scanFragment.setShowStatusCode(intent.getBooleanExtra(EXTRA_SHOW_STATUS_CODE, false))
        scanFragment.setZoom(intent.getDoubleExtra(EXTRA_ZOOM, CameraPreview.DEFAULT_ZOOM))
        scanFragment.setScanResultListener(this)

        // Insert ScanFragment into this Activity's layout.
        val fragmentManager = fragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.scan_camera_activity_root, scanFragment)
        fragmentTransaction.commit()

    }

    /**
     * Do something with ScanResult.
     *
     *
     * Warning: will be called from background threads. Use handlers to post back to UI thread.
     */

    override fun onScanResult(scanResult: ScanResult?) {
        var stringResult = ""
        handler.post {
            // Do something in UI thread with ScanResult.
            if (scanResult != null && !scanResult.isEmpty) {
                for (tag in scanResult.tags) {
                    AppLogger.d(
                        "scanResult",
                        String.format("Scanned code %s:%s", tag.templateIdentifier, tag.data)
                    )
                    stringResult = tag.data.toString()
                    break
                }
                decideNextActivity(stringResult)
            }

        }
    }

    fun decideNextActivity(stringResult: String) {
        val embededString = stringResult.substring(0, stringResult.length - 1)
        val embededCode: String = stringResult.substring(stringResult.length - 1)

        if (TextUtils.equals(embededCode, "0")) {
            startActivityForResult(
                SinglePostActivity.newIntent(
                    context,
                    embededCode,
                    embededString
                ), RequestCodes.SCAN_CAMERA
            )
        } else {
            startActivityForResult(
                ProfileActivity.newIntent(context, embededCode, embededString),
                RequestCodes.SCAN_CAMERA
            )
        }
    }

    companion object {

        val EXTRA_SHOW_OVERLAY = "extra_show_overlay"
        val EXTRA_SHOW_STATUS_CODE = "extra_show_status_code"
        val EXTRA_ZOOM = "extra_zoom"

        /**
         * Helper to start ScanCameraActivity with settings in Intent.
         */
        fun newInstance(
            context: Context,
            zoom: Double,
            showResultOverlay: Boolean,
            showStatusCode: Boolean
        ): Intent {
            val intent = Intent(context, ScanCameraActivity::class.java)
            // Pass settings to onCreate() via Intent.
            intent.putExtra(EXTRA_SHOW_OVERLAY, showResultOverlay)
            intent.putExtra(EXTRA_SHOW_STATUS_CODE, showStatusCode)
            intent.putExtra(EXTRA_ZOOM, zoom)


            return intent
        }
    }

    override fun onBack() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RequestCodes.PHOTO_REQUEST) {
                if (data != null) {
                    val contentURI = data.data
                    try {
                        val photo =
                            MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                        if (photo != null) {
                            ScanImageTask().execute(photo)
                        } else {
                            Toast.makeText(this, "Error getting image.", Toast.LENGTH_SHORT).show()
                        }
                    } catch (e: IOException) {
                        Toast.makeText(this, "Error getting image.", Toast.LENGTH_SHORT).show()
                    }

                }
            } else {
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }

    private inner class ScanImageTask : AsyncTask<Bitmap, Void, ScanResult>() {
        override fun onPreExecute() {
        }

        override fun doInBackground(vararg bitmaps: Bitmap): ScanResult? {
            return if (bitmaps == null || bitmaps.size == 0) null else Quikkly.getInstance().scanSingleImage(
                bitmaps[0]
            )
        }

        override fun onPostExecute(scanResult: ScanResult?) {
            if (scanResult == null || scanResult.isEmpty) {
                Toast.makeText(context, "No QrCode found", Toast.LENGTH_SHORT).show()
            } else {
                var stringResult = ""
                // Do something in UI thread with ScanResult.
                if (!scanResult.isEmpty) {
                    for (tag in scanResult.tags) {
                        stringResult = tag.data.toString()
                        break
                    }
                    decideNextActivity(stringResult)
                }
            }
        }
    }

    fun selectFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, RequestCodes.PHOTO_REQUEST)
    }
}
