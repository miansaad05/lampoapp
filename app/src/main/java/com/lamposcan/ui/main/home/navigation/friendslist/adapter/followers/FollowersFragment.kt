package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.FollowersDataModel
import com.lamposcan.databinding.FollowersFragmentBinding
import com.lamposcan.ui.base.baseFragment.BaseFragment
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers.adapter.FollowersAdapter
import com.lamposcan.utils.MessageAlert
import com.lamposcan.utils.NetworkUtils
import javax.inject.Inject

class FollowersFragment : BaseFragment<FollowersFragmentBinding, FollowersViewModel>(),
    FollowersNavigator, FollowersAdapter.OnClickListener {

    companion object {
        fun newInstance() = FollowersFragment()
    }

    @Inject
    lateinit var viewModel: FollowersViewModel
    @Inject
    lateinit var adapter: FollowersAdapter

    private lateinit var binding: FollowersFragmentBinding

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.followers_fragment
    }

    override fun viewModel(): FollowersViewModel {
        return viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (view != null)
            view.tag = this
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        baseActivity?.let {
            mContext = it
        }
        userModel = getUserModelFromPref()

        adapter.setListener(this)
        binding.rvFollowers.itemAnimator = null
        binding.rvFollowers.adapter = adapter


        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.fetchFollowers(userModel.apiToken)
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun showMessage(message: String) {
        viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
    }

    override fun setFollowersList(followersDataModel: FollowersDataModel) {
        if (followersDataModel.followers != null)
            adapter.addItems(followersDataModel.followers, false)
        hideLoader()
        viewModel.isRefreshing.set(false)
    }

    override fun onRefresh() {
        if (NetworkUtils.isNetworkConnected(context)) {
            adapter.clearItems()
            viewModel.fetchFollowers(userModel.apiToken)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun showLoader() {
        baseActivity?.showLoading()
    }

    override fun hideLoader() {
        baseActivity?.hideLoading()
    }

    override fun onFollowClick() {

    }
}
