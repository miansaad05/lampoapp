package com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab.adapter.NotificationsAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class NotificationsModule : BaseActivityModule<NotificationsViewModel>() {
    @Provides
    fun provideFollowersViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): NotificationsViewModel {
        return NotificationsViewModel(dataManager, mSchedulerProvider)
    }

    @Provides
    fun provideNotificationsAdapter(fragment: NotificationsFragment): NotificationsAdapter {
        return NotificationsAdapter(fragment.context)
    }
}