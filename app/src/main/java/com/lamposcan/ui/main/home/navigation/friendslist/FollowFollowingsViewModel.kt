package com.lamposcan.ui.main.home.navigation.friendslist

import androidx.databinding.ObservableField
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.interfaces.ToolbarMenuHelper
import com.lamposcan.utils.rx.SchedulerProvider

class FollowFollowingsViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<FollowFollowingsNavigator>(dataManager, schedulerProvider) {
    val totalRequestCount = ObservableField<String>()

    var mToolbarMenuHelper: ToolbarMenuHelper = object : ToolbarMenuHelper {
        override fun onBackClick() {
            navigator?.onBack()
        }

        override fun onMenuItem() {
        }

    }

    fun onRequestClick() {

    }
}
