package com.lamposcan.ui.main.home.navigation.friendslist.unfollowdialog

import androidx.databinding.ObservableField
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class UnFollowViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<UnFollowNavigator>(dataManager, mSchedulerProvider) {

    val unfollowHeader = ObservableField<String>("")
    val unfollowFooter = ObservableField<String>("")

    fun onSubmit() {
        navigator?.onSubmitClick()
    }

    fun onCancel() {
        navigator?.onCancelClick()
    }
}