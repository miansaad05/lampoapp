package com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FriendRequestsProvider {
    @ContributesAndroidInjector(modules = [FriendRequestsModule::class])
    abstract fun provideFriendRequestsFactory(): FriendRequestsFragment
}