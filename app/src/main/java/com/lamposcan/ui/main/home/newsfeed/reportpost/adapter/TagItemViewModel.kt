package com.lamposcan.ui.main.home.newsfeed.reportpost.adapter

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.lamposcan.data.model.others.TagModel

class TagItemViewModel(tagModel: TagModel, mListener: TagsClickListener) {

    val tagName: ObservableField<String> = ObservableField()
    val isSelected = ObservableBoolean(false)
    val listener: TagsClickListener = mListener

    init {
        tagName.set(tagModel.tagName)
        isSelected.set(tagModel.isSelected)
    }

    interface TagsClickListener {
        fun onTagClick()

    }

    fun onTagClick() {
        listener.onTagClick()
    }
}

