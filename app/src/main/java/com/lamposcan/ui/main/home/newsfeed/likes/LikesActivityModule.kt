package com.lamposcan.ui.main.home.newsfeed.likes

import android.content.Context
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.newsfeed.likes.adapter.LikesAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class LikesActivityModule : BaseActivityModule<LikesViewModel>() {
    @Provides
    fun provideSplashViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): LikesViewModel {
        return LikesViewModel(dataManager, schedulerProvider)
    }

    @Provides
    fun provideLikesAdapter(context: Context): LikesAdapter {
        return LikesAdapter(context)
    }
}
