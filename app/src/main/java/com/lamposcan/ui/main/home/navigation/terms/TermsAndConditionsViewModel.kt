package com.lamposcan.ui.main.home.navigation.terms

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class TermsAndConditionsViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<TermsAndConditionsNavigator>(dataManager, schedulerProvider)
