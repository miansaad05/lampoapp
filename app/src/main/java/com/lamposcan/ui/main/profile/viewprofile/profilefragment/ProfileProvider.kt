package com.lamposcan.ui.main.profile.viewprofile.profilefragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProfileProvider {
    @ContributesAndroidInjector(modules = [ProfileModule::class])
    abstract fun provideProfileFactory(): ProfileFragment
}