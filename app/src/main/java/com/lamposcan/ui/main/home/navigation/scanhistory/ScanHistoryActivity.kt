package com.lamposcan.ui.main.home.navigation.scanhistory

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ActivityScanHistoryBinding
import com.lamposcan.ui.base.BaseActivity
import javax.inject.Inject

class ScanHistoryActivity :
    BaseActivity<ActivityScanHistoryBinding, ScanHistoryViewModel>(),
    ScanHistoryNavigator {

    private lateinit var binding: ActivityScanHistoryBinding
    @Inject
    lateinit var viewModel: ScanHistoryViewModel

    companion object {
        fun newIntent(context: Context) = Intent(context, ScanHistoryActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_scan_history
    }

    override fun viewModel(): ScanHistoryViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this
    }
}
