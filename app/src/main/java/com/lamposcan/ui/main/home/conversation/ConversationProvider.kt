package com.lamposcan.ui.main.home.conversation

import com.lamposcan.ui.main.home.newsfeed.ConversationFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ConversationProvider {
    @ContributesAndroidInjector(modules = [ConversationModule::class])
    abstract fun provideConversationFactory(): ConversationFragment
}