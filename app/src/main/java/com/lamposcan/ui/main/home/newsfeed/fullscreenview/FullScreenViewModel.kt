package com.lamposcan.ui.main.home.newsfeed.fullscreenview

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.ui.main.home.singlepostview.SinglePostNavigator
import com.lamposcan.utils.rx.SchedulerProvider

class FullScreenViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<FullScreenNavigator>(dataManager, schedulerProvider) {
}