package com.lamposcan.ui.main.home.newsfeed.likes.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.api.LikesModel
import com.lamposcan.databinding.ItemLikesBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class LikesAdapter(val context: Context?) : RecyclerView.Adapter<BaseViewHolder>(),
    AdapterUpdateListener {
    private var list: MutableList<LikesModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemLikesBinding.inflate(
            LayoutInflater.from(context), parent, false
        )
        return LikesViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun addItems(obj: Any) {
        list.add(0, obj as LikesModel)
        notifyItemInserted(0)
    }

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {
        list.addAll(items as Collection<LikesModel>)
        notifyDataSetChanged()
    }

    open inner class LikesViewHolder(val binding: ItemLikesBinding) :
        BaseViewHolder(binding.root) {
        private lateinit var itemViewModel: LikesItemViewModel
        override fun onBind(position: Int) {
            itemViewModel = LikesItemViewModel(list[position])
            binding.viewModel = itemViewModel
            binding.executePendingBindings()
        }
    }
}