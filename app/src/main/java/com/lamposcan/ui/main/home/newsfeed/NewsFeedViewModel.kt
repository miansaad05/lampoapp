package com.lamposcan.ui.main.home.newsfeed

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider
import java.util.*

class NewsFeedViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<NewsFeedNavigator>(dataManager, mSchedulerProvider) {
    val isRefreshing = ObservableBoolean(false)

    fun fetchPosts(token: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().fetchPosts("Bearer $token")
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val listType = object : TypeToken<ArrayList<PostModel>>() {}.type
                                val posts = Gson().fromJson<List<PostModel>>(
                                    Gson().toJson(it.data), listType
                                )
                                navigator?.setPosts(posts)
                                navigator?.onResponse()
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")

                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onRefresh() {
        isRefreshing.set(true)
        navigator?.onRefresh()
    }

    fun onLikeClick(token: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().likePosts("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val jsonObject = object : TypeToken<PostModel>() {}.type

                                val postModel = Gson().fromJson<PostModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.onLikeResponse(postModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onUnLikeClick(token: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().unLikePost("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val jsonObject = object : TypeToken<PostModel>() {}.type

                                val postModel = Gson().fromJson<PostModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.onLikeResponse(postModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onDeleteMyPost(token: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().deletePost("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        navigator?.onReportAndDeleteSuccessFully()
                        navigator?.showMessage(it.message)
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")

                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onReport(token: String, id: Int, tagName: String, description: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().reportPost(
                "Bearer $token",
                id.toString(), tagName, description
            )
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        navigator?.onReportAndDeleteSuccessFully()
                        navigator?.showMessage(it.message)
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")

                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

}
