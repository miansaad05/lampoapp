package com.lamposcan.ui.main.home.navigation.scanhistory
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class ScanHistoryModule : BaseActivityModule<ScanHistoryViewModel>() {
    @Provides
    fun provideScanHistoryViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): ScanHistoryViewModel {
        return ScanHistoryViewModel(dataManager, schedulerProvider)
    }
}
