package com.lamposcan.ui.main.home.newsfeed.likes

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.LikesModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.interfaces.ToolbarMenuHelper
import com.lamposcan.utils.rx.SchedulerProvider
import java.util.*

class LikesViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<LikesNavigator>(dataManager, schedulerProvider) {
    val likesCount = ObservableField<String>("")
    val isRefreshing = ObservableBoolean(false)

    var mToolbarMenuHelper: ToolbarMenuHelper = object : ToolbarMenuHelper {
        override fun onBackClick() {
            navigator?.onBack()
        }

        override fun onMenuItem() {
        }

    }

    fun fetchPostLikes(token: String?, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().getPostLikes("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val listType = object : TypeToken<ArrayList<LikesModel>>() {}.type
                                val likesList = Gson().fromJson<List<LikesModel>>(
                                    Gson().toJson(it.data), listType
                                )
                                navigator?.setLikes(likesList)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onRefresh() {
        isRefreshing.set(true)
        navigator?.onRefresh()
    }

}
