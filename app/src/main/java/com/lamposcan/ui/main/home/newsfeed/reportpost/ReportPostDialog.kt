package com.lamposcan.ui.main.home.newsfeed.reportpost

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.others.TagModel
import com.lamposcan.databinding.ReportPostBinding
import com.lamposcan.ui.base.BaseDialog
import com.lamposcan.ui.main.home.newsfeed.reportpost.adapter.TagsAdapter
import javax.inject.Inject

class ReportPostDialog : BaseDialog<ReportPostBinding, ReportPostViewModel>(), ReportPostNavigator {
    private lateinit var binding: ReportPostBinding
    @Inject
    lateinit var viewModel: ReportPostViewModel
    @Inject
    lateinit var adapter: TagsAdapter

    private var listener: Callback? = null

    companion object {
        fun newInstance(postModel: String): ReportPostDialog {
            val fragment = ReportPostDialog()
            val bundle = Bundle()
            bundle.putString("postModel", postModel)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.report_post
    }

    override fun viewModel(): ReportPostViewModel? {
        return viewModel
    }

    private var postModel = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this


        postModel = arguments?.getString("postModel").toString()


        val tagsList: MutableList<TagModel> = ArrayList()
        tagsList.add(TagModel("Misleading", false))
        tagsList.add(TagModel("Sexually Inappropriate", false))
        tagsList.add(TagModel("Offensive", false))
        tagsList.add(TagModel("Violence", false))
        tagsList.add(TagModel("Spam", false))
        tagsList.add(TagModel("Prohibited Content", false))
        tagsList.add(TagModel("False News", false))
        tagsList.add(TagModel("Others", false))


        binding.rvTags.adapter = adapter
        adapter.addItems(tagsList, false)

        return view
    }

    fun setListener(mListener: Callback) {
        listener = mListener
    }

    fun show(fragmentManager: FragmentManager) {
        super.show(fragmentManager, "TAG")
    }

    interface Callback {
        fun onSubmitClick(tagName: String, description: String, postModel: String)
    }

    override fun onSubmitClick() {
        listener?.onSubmitClick(
            adapter.getList()[adapter.getSelectedIndex()].tagName,
            binding.etUserComment.text.toString(), postModel
        )
        dialog?.dismiss()
    }

}