package com.lamposcan.ui.main.home.navigation.friendslist.unfollowdialog

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.newsfeed.reportpost.adapter.TagsAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class UnFollowModule : BaseActivityModule<UnFollowViewModel>() {
    @Provides
    fun provideUnFollowViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): UnFollowViewModel {
        return UnFollowViewModel(dataManager, mSchedulerProvider)
    }

}