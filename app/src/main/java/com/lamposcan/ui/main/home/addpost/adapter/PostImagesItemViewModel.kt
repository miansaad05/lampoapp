package com.lamposcan.ui.main.home.addpost.adapter

import android.text.TextUtils
import androidx.databinding.ObservableField
import com.lamposcan.data.model.others.PostImagesModel

class PostImagesItemViewModel(model: PostImagesModel) {
    val imageUrl: ObservableField<String>

    init {
        if (TextUtils.isEmpty(model.name)) {
            this.imageUrl = ObservableField("")
        } else
            this.imageUrl = ObservableField(model.name)
    }
}
