package com.lamposcan.ui.main.qrcode.profile

import androidx.databinding.ObservableField
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class ProfileQrViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<ProfileQrNavigator>(dataManager, mSchedulerProvider) {

//    val userProfileImage = ObservableField<String>()
    val username = ObservableField<String>("")

    fun onSubmitReport() {
        navigator?.onSubmitClick()
    }
}