package com.lamposcan.ui.main.home.addpost

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.annotation.NonNull
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.others.PostImagesModel
import com.lamposcan.databinding.ActivityAddPostBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.addpost.adapter.PostImagesAdapter
import com.lamposcan.utils.*
import java.io.ByteArrayOutputStream
import java.io.File
import javax.inject.Inject

class AddPostActivity : BaseActivity<ActivityAddPostBinding, AddPostViewModel>(), AddPostNavigator,
    PostImagesAdapter.ItemClickListener {

    private lateinit var binding: ActivityAddPostBinding
    @Inject
    lateinit var viewModel: AddPostViewModel
    @Inject
    lateinit var postImagesAdapter: PostImagesAdapter

    private var fileUri: Uri

    init {
        fileUri = Uri.parse("")
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, AddPostActivity::class.java)

    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_add_post
    }

    override fun viewModel(): AddPostViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userModel = getUserModelFromPref()
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this



        postImagesAdapter.setListener(this)

        binding.rvPostImages.layoutManager = GridLayoutManager(context, 3)

        binding.rvPostImages.addItemDecoration(
            ItemOffsetDecoration(
                context,
                R.dimen.add_post_margins,
                R.dimen.add_post_margins,
                R.dimen.add_post_margins,
                R.dimen.add_post_margins
            )
        )

        binding.rvPostImages.adapter = postImagesAdapter

        postImagesAdapter.addItems(
            listOf(PostImagesModel("", true)), false
        )
    }

    override fun onMenuClicked() {
        showOptionsMenu()
    }


    private fun showOptionsMenu() {
        val wrapper = ContextThemeWrapper(context, R.style.popupMenuStyle)
        val popup = PopupMenu(wrapper, binding.toolbar.ivMenuItem)
        //Inflating the Popup using xml file
        popup.menuInflater.inflate(R.menu.create_post_menu, popup.menu)

        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_private -> {
                    viewModel.isPublic = 0
                    MessageAlert.showInformationMessage(
                        binding.root,
                        getString(R.string.private_post)
                    )
                    true
                }
                R.id.menu_public -> {
                    viewModel.isPublic = 1
                    MessageAlert.showInformationMessage(
                        binding.root,
                        getString(R.string.public_posting)
                    )
                    true
                }
                else -> true
            }
        })

        popup.show()
    }

    override fun addImage() {
        if (postImagesAdapter.getListSize() < 6) {
            checkPermissions()
        } else {
            MessageAlert.showErrorMessage(binding.root, "You can`t select more then 5")
        }
    }

    private fun checkPermissions() {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.CAMERA
            ) === PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) === PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) === PackageManager.PERMISSION_GRANTED
        ) {
            open()
        } else {
            askCameraPermission(Manifest.permission.CAMERA, CommonUtils.CAMERA_PERMISSION_RESULT)
        }
    }

    private fun open() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems =
            arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallery()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    private fun choosePhotoFromGallery() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, CommonUtils.GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CommonUtils.CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == CommonUtils.CAMERA) {
                data?.let {
                    val imageBitmap = it.extras?.get("data") as Bitmap?
                    imageBitmap?.let {
                        fileUri = getImageUri(context, imageBitmap)
                        postImagesAdapter.addItems(
                            listOf(PostImagesModel(fileUri.toString(), false)), false
                        )
                    }

                    uploadPrescriptionToServer()
                }

            } else if (requestCode == CommonUtils.GALLERY) {
                data?.data?.let {
                    fileUri = it
                    getBitmapFromGallery(data)
                    uploadPrescriptionToServer()
                }
            }
        }
    }

    private fun uploadPrescriptionToServer() {
        AppLogger.d("hello", "hello")
//        val imagePath = getRealPathFromURI(fileUri, context)
//        mPostAdViewModel.sendImageToServer(imagePath)
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        if (requestCode == CommonUtils.CAMERA_PERMISSION_RESULT) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                askCameraPermission(permissions[0], CommonUtils.CAMERA_PERMISSION_RESULT)
            } else {
                askStoragePermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    CommonUtils.STORAGE_PERMISSION_RESULT
                )
            }
        } else if (requestCode == CommonUtils.STORAGE_PERMISSION_RESULT) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                askStoragePermission(permissions[0], CommonUtils.STORAGE_PERMISSION_RESULT)
            } else {
                open()
            }
        }
    }

    private fun getBitmapFromGallery(data: Intent) {
        val pickedImage = data.data
        // Let's read picked image path using content resolver
        val filePath = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = pickedImage?.let { contentResolver.query(it, filePath, null, null, null) }
        cursor?.moveToFirst()
        val imagePath = cursor?.getString(cursor.getColumnIndex(filePath[0]))

        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888

        val imageBitmap = BitmapFactory.decodeFile(imagePath, options)

        cursor?.close()

        fileUri = getImageUri(this, imageBitmap)

        postImagesAdapter.addItems(listOf(PostImagesModel(fileUri.toString(), false)), false)

    }

    private fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val path =
            MediaStore.Images.Media.insertImage(
                inContext.contentResolver,
                inImage,
                "Title",
                null
            )
        return Uri.parse(path)
    }

    private fun askCameraPermission(permission: String, flag: Int) {
        PermissionHandlerHelper.checkPermissionHelper(
            this,
            arrayOf(permission),
            object : PermissionHandlerHelper.CheckPermissionResponse {
                override fun permissionGranted() {
                    askStoragePermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        CommonUtils.STORAGE_PERMISSION_RESULT
                    )
                }

                override fun showNeededPermissionDialog() {
                    AlertDialog.Builder(context)
                        .setMessage("Camera permission require to take photo. Please grant this app storage Permission.")
                        .setPositiveButton(
                            "Settings"
                        ) { dialog, which -> Utility.openPermissionsInSettings(context as Activity) }
                        .create().show()
                }

                @TargetApi(Build.VERSION_CODES.M)
                override fun requestPermission() {
                    requestPermissions(arrayOf(permission), flag)
                }
            })
    }

    fun askStoragePermission(permission: String, flag: Int) {
        PermissionHandlerHelper.checkPermissionHelper(
            this,
            PermissionHandlerHelper.STORAGE_PERMISSION,
            object : PermissionHandlerHelper.CheckPermissionResponse {
                override fun permissionGranted() {

                }

                override fun showNeededPermissionDialog() {
                    AlertDialog.Builder(context)
                        .setMessage("Permissions require to select Photo. Please grant this app storage Permission.")
                        .setPositiveButton(
                            "Settings"
                        ) { dialog, which -> Utility.openPermissionsInSettings(context as Activity) }
                        .create().show()
                }

                @TargetApi(Build.VERSION_CODES.M)
                override fun requestPermission() {
                    requestPermissions(arrayOf(permission), flag)
                }
            })
    }

    override fun getFields() {
        viewModel.title = binding.etTitle.text.toString().trim()
        viewModel.description = binding.etDescription.text.toString().trim()

        val selectedFile: MutableList<PostImagesModel> = postImagesAdapter.getList()
//        val files = HashMap<String, MutableList<File>>()

        val filesObj = ArrayList<File>()

        for (i in selectedFile) {
            val imagePath = getRealPathFromURI(
                Uri.parse(i.name), this
            )
            imagePath?.let {
                filesObj.add(
                    File(it)
                )
            }
        }


        viewModel.files.addAll(filesObj)
        viewModel.submitFields(userModel.apiToken)
    }

    fun getRealPathFromURI(contentURI: Uri, context: Activity): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = context.managedQuery(contentURI, projection, null, null, null) ?: return null
        val column_index = cursor
            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        return if (cursor.moveToFirst()) {
            // cursor.close();
            cursor.getString(column_index)
        } else null
        // cursor.close();
    }

    override fun showLoader() {
        showLoading()
    }

    override fun onResponse() {
        hideLoading()
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onBack() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun hideLoader() {
        hideLoading()
    }
}
