package com.lamposcan.ui.main.home.newsfeed.adapter

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.lamposcan.R
import com.lamposcan.data.remote.ApiEndPoints

class MultiPostItemViewModel(postImageUrl: String, val listener: ClickListener) {
    val postImageUrl: ObservableField<String> = ObservableField("")
    val placeHolder: ObservableInt = ObservableInt(R.drawable.ic_error)
    val errorHolder: ObservableInt = ObservableInt(R.drawable.ic_error)

    init {
        this.postImageUrl.set(ApiEndPoints.IMAGE_BASE_URL + postImageUrl)
    }

    interface ClickListener {
        fun openFullScreen()
    }

    fun openFullScreen() {
        listener.openFullScreen()
    }
}

