package com.lamposcan.ui.main.home.navigation.notifications

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.interfaces.ToolbarMenuHelper
import com.lamposcan.utils.rx.SchedulerProvider

class NotificationsViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<NotificationsNavigator>(dataManager, schedulerProvider) {

    var mToolbarMenuHelper: ToolbarMenuHelper = object : ToolbarMenuHelper {
        override fun onBackClick() {
            navigator?.onBack()
        }

        override fun onMenuItem() {
        }

    }
}
