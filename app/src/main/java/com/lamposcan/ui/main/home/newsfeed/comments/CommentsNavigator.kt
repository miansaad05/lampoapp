package com.lamposcan.ui.main.home.newsfeed.comments

import com.lamposcan.data.model.api.CommentModel
import com.lamposcan.ui.base.BaseNavigator

interface CommentsNavigator : BaseNavigator {
    fun showLoader()
    fun onRefresh()
    fun hideLoader()
    fun setComments(commentList: List<CommentModel>)
    fun addComments(comment: CommentModel)
    fun getFields()
    fun onQrCodeClick()
    fun onOptionsMenuClick()
    fun onReportAndDeleteSuccessFully()
    fun onLikeResponse(model: CommentModel)
}
