package com.lamposcan.ui.main.home.discover

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.interfaces.ToolbarMenuHelper
import com.lamposcan.utils.rx.SchedulerProvider
import java.util.*

class DiscoverViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<DiscoverNavigator>(dataManager, schedulerProvider) {

    val isRefreshing = ObservableBoolean(false)
    var mToolbarMenuHelper: ToolbarMenuHelper = object : ToolbarMenuHelper {
        override fun onBackClick() {
            navigator?.onBack()
        }

        override fun onMenuItem() {
        }

    }

    fun fetchPeople(apiToken: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().getUsersList("Bearer $apiToken")
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val listType = object : TypeToken<ArrayList<UserModel>>() {}.type
                                val usersList = Gson().fromJson<List<UserModel>>(
                                    Gson().toJson(it.data), listType
                                )
                                navigator?.setUsersList(usersList)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }


    fun onRefresh() {
        isRefreshing.set(true)
        navigator?.onRefresh()
    }
}
