package com.lamposcan.ui.main.home.navigation.friendslist.adapter

import android.content.Context
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers.FollowersFragment
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings.FollowingsFragment


class FollowFollowingPagerAdapter(val context: Context, val manager: FragmentManager) :
    FragmentStatePagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val TAB_TITLES = arrayOf("Followings", "Followers")

    val followingsFragment: FollowingsFragment
    val followersFragment: FollowersFragment

    init {
        followingsFragment = FollowingsFragment.newInstance()
        followersFragment = FollowersFragment.newInstance()
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return followingsFragment
            }
            else -> {
                return followersFragment
            }
        }
    }

    override fun getCount(): Int {
        return TAB_TITLES.size
    }

    @Nullable
    override fun getPageTitle(position: Int): CharSequence? {
        return TAB_TITLES[position]
    }


}