package com.lamposcan.ui.main.profile.viewprofile.profilefragment

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.newsfeed.adapter.PostsAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class ProfileModule : BaseActivityModule<ProfileViewModel>() {
    @Provides
    fun provideProfileViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): ProfileViewModel {
        return ProfileViewModel(dataManager, mSchedulerProvider)
    }


    @Provides
    fun providePostsAdapter(fragment: ProfileFragment): PostsAdapter {
        return PostsAdapter(fragment.context)
    }
}