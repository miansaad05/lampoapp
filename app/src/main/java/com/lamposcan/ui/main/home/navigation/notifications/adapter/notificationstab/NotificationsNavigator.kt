package com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab

import com.lamposcan.data.model.api.NotificationModel
import com.lamposcan.ui.base.BaseNavigator

interface NotificationsNavigator : BaseNavigator {
    fun showLoader()

    fun hideLoader()

    fun onRefresh()

    fun setNotificationList(
        list: List<NotificationModel
                >
    )
}
