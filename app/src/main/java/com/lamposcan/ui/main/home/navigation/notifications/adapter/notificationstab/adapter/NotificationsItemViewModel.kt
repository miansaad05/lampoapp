package com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab.adapter

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.lamposcan.data.model.api.NotificationModel
import com.lamposcan.data.remote.ApiEndPoints

class NotificationsItemViewModel(notificationModel: NotificationModel) {
    val userProfileImage: ObservableField<String> = ObservableField()
    val likeUserText: ObservableField<String> = ObservableField()
    val time: ObservableField<String> = ObservableField()
    val isRead: ObservableBoolean = ObservableBoolean()

    init {
        userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL /*+ notificationModel.profileImage*/)
        likeUserText.set(notificationModel.message)
        time.set(notificationModel.createdAt)

        isRead.set(notificationModel.unread == 0)

    }

}