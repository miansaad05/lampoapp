package com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests.adapter

import androidx.databinding.ObservableField
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.data.remote.ApiEndPoints

class FriendRequestsItemViewModel(userModel: UserModel, mListener: OnClickListener) {
    val userProfileImage: ObservableField<String> = ObservableField()
    val userName: ObservableField<String> = ObservableField()
    val listener: OnClickListener

    init {
        listener = mListener
        userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + userModel.profile?.profileImage)
        userName.set(userModel.fname + "  " + userModel.lname + " send you a friend request")
    }

    interface OnClickListener {
        fun onCancel()
        fun onConfirm()
    }

    fun onCancel() {
        listener.onCancel()
    }

    fun onConfirm() {
        listener.onConfirm()
    }
}