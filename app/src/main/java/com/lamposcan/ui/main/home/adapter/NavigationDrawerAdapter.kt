package com.lamposcan.ui.main.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.others.NavigationDrawerModel
import com.lamposcan.databinding.ItemNavigationDrawerBinding
import com.lamposcan.ui.base.BaseViewHolder

class NavigationDrawerAdapter(val context: Context) : RecyclerView.Adapter<BaseViewHolder>() {
    private var list: MutableList<NavigationDrawerModel> = ArrayList()
    private var listener: OnClickListener? = null

    fun addData(list: MutableList<NavigationDrawerModel>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(listener: OnClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemNavigationDrawerBinding.inflate(
            LayoutInflater.from(context), parent, false
        )
        return NavItemViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    interface OnClickListener {
        fun onItemClick(model: NavigationDrawerModel)
    }

    inner class NavItemViewHolder(binding: ItemNavigationDrawerBinding) :
        BaseViewHolder(binding.root), NavigationDrawerItemViewModel.OnItemClick {
        private val itemBinding = binding

        init {
            binding.executePendingBindings()
        }

        override fun onBind(position: Int) {
            val navItemViewModel = NavigationDrawerItemViewModel(
                list[position], this
            )

            itemBinding.viewModel = navItemViewModel
        }

        override fun onItemClick(model: NavigationDrawerModel) {
            listener?.onItemClick(model)
        }

    }
}