package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.FollowingsDataModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class FollowingsViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<FollowingsNavigator>(dataManager, mSchedulerProvider) {
    val isRefreshing = ObservableBoolean(false)

    fun onRefresh() {
        isRefreshing.set(true)
        navigator?.onRefresh()
    }

    fun fetchFollowings(apiToken: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().getUserFollowings("Bearer $apiToken")
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {


                                val jsonObject = object : TypeToken<FollowingsDataModel>() {}.type
                                val followersModel = Gson().fromJson<FollowingsDataModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.setFollowingsList(followersModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )

    }

    fun unFollowUser(apiToken: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().unFollowUser("Bearer $apiToken", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {
                                navigator?.showMessage(it.message)
                                navigator?.onUnFollowSuccess()
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )

    }
}