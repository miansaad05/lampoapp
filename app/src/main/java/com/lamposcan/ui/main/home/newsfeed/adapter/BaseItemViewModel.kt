package com.lamposcan.ui.main.home.newsfeed.adapter

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.lamposcan.data.model.api.PostModel

open class BaseItemViewModel(val postModel: PostModel, val listener: Listeners) {
    val userName: ObservableField<String> = ObservableField()
    val userProfileImage: ObservableField<String> = ObservableField("")
    val userLocation: ObservableField<String> = ObservableField()
    val likesThumb: ObservableField<String> = ObservableField("")
    val likeCounter: ObservableField<String> = ObservableField()
    val commentsCounter: ObservableField<String> = ObservableField()
    val sharePostCounter: ObservableField<String> = ObservableField()
    val postTime: ObservableField<String> = ObservableField()
    val likedIcon: ObservableInt = ObservableInt()

    open interface Listeners {
        fun openLike()
        fun onLike()
        fun onComment()
        fun onShare()
        fun openProfile()
        fun onOptionsMenuClick()
        fun onQrCodeClick()
        fun openFullScreen()
    }

    fun onProfileClick() {
        listener.openProfile()
    }

    fun onLikeClick() {
        listener.onLike()
    }

    fun openFullScreen() {
        listener.openFullScreen()
    }

    fun openLikeClick() {
        listener.openLike()
    }

    fun onOptionsMenuClick() {
        listener.onOptionsMenuClick()
    }

    fun onCommentClick() {
        listener.onComment()
    }

    fun onShareClick() {
        listener.onShare()
    }

    fun onQrCodeClick() {
        listener.onQrCodeClick()
    }
}