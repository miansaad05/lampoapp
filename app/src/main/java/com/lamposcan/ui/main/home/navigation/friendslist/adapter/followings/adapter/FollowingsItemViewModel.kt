package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings.adapter

import androidx.databinding.ObservableField
import com.lamposcan.data.model.api.FollowingsModel
import com.lamposcan.data.remote.ApiEndPoints

class FollowingsItemViewModel(followingsModel: FollowingsModel, mListener: OnClickListener) {
    val userProfileImage: ObservableField<String> = ObservableField()
    val userName: ObservableField<String> = ObservableField()
    val userLocation: ObservableField<String> = ObservableField()
    val followersCount: ObservableField<String> = ObservableField()
    val followingsCount: ObservableField<String> = ObservableField()
    val listener: OnClickListener

    init {
        listener = mListener

        userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + followingsModel.profileImage)
        userName.set(followingsModel.fname + "  " + followingsModel.lname)
        if (followingsModel.country == null) {
            userLocation.set("")
        } else {
            userLocation.set("Location : " + followingsModel.country)
        }
        followersCount.set(followingsModel.followers.toString())
        followingsCount.set(followingsModel.followings.toString())
    }

    fun onUnFollowClick() {
        listener.onUnFollowClick()
    }

    interface OnClickListener {
        fun onUnFollowClick()
    }

}