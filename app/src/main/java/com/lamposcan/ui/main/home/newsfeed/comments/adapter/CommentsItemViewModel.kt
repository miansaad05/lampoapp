package com.lamposcan.ui.main.home.newsfeed.comments.adapter

import android.text.TextUtils
import androidx.databinding.ObservableField
import com.lamposcan.data.model.api.CommentModel
import com.lamposcan.data.remote.ApiEndPoints

class CommentsItemViewModel(model: CommentModel, val listener: OnItemClickListener) {

    val userName: ObservableField<String> = ObservableField()
    val userProfileImage: ObservableField<String> = ObservableField("")
    val comment: ObservableField<String> = ObservableField("")
    val totalCommentLikes: ObservableField<String> = ObservableField("")
    val createdTime: ObservableField<String> = ObservableField("")

    interface OnItemClickListener {
        fun onCommentLikeClick()
    }

    init {
        userName.set(model.commentUser.fname + " " + model.commentUser.lname)
        userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + model.commentUser.profilePicture)

        comment.set(model.comment)


        if (TextUtils.equals(model.totalLikes, "0.0")) {
            totalCommentLikes.set("0")

        } else
            totalCommentLikes.set(model.totalLikes)

        createdTime.set(model.createdAt)

    }

    fun onCommentLikeClick() {
        listener.onCommentLikeClick()
    }
}

