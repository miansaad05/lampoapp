package com.lamposcan.ui.main.home.adapter

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.lamposcan.data.model.others.NavigationDrawerModel

class NavigationDrawerItemViewModel(
    private val itemViewModel: NavigationDrawerModel,
    mListener: OnItemClick
) {

    val icon: ObservableInt = ObservableInt(itemViewModel.icon)
    val title: ObservableField<String> = ObservableField(itemViewModel.title)
    val listener: OnItemClick = mListener


    fun onItemClick() {
        listener.onItemClick(itemViewModel)
    }

    interface OnItemClick {
        fun onItemClick(model: NavigationDrawerModel)
    }
}