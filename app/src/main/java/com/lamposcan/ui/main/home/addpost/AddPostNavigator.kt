package com.lamposcan.ui.main.home.addpost

import com.lamposcan.ui.base.BaseNavigator

interface AddPostNavigator : BaseNavigator {
    fun onMenuClicked()
    fun getFields()
    fun showLoader()

    fun onResponse()
    fun hideLoader()
}
