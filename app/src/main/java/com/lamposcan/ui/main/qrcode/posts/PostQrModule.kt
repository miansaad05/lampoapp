package com.lamposcan.ui.main.qrcode.posts

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class PostQrModule : BaseActivityModule<PostQrViewModel>() {
    @Provides
    fun providePostQrViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): PostQrViewModel {
        return PostQrViewModel(dataManager, mSchedulerProvider)
    }
}