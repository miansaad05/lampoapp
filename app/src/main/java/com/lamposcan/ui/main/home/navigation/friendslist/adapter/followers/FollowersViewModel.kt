package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.FollowersDataModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class FollowersViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<FollowersNavigator>(dataManager, mSchedulerProvider) {
    val isRefreshing = ObservableBoolean(false)

    fun onRefresh() {
        isRefreshing.set(true)
        navigator?.onRefresh()
    }

    fun fetchFollowers(apiToken: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().getUserFollowers("Bearer $apiToken")
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {


                                val jsonObject = object : TypeToken<FollowersDataModel>() {}.type
                                val followersModel = Gson().fromJson<FollowersDataModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.setFollowersList(followersModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )

    }
}