package com.lamposcan.ui.main.home.newsfeed.comments.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.api.CommentModel
import com.lamposcan.databinding.ItemCommentsBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class CommentsAdapter(val context: Context?) : RecyclerView.Adapter<BaseViewHolder>(),
    AdapterUpdateListener {
    var list: MutableList<CommentModel> = ArrayList()
    private var mListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemCommentsBinding.inflate(
            LayoutInflater.from(context), parent, false
        )
        return LikesViewHolder(binding)
    }

    fun setListener(listener: OnItemClickListener) {
        mListener = listener
    }

    interface OnItemClickListener {
        fun onCommentLikeClicked(model: CommentModel)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun addItems(obj: Any) {
        list.add(0, obj as CommentModel)
        notifyItemInserted(0)
    }

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {
        list.addAll(items as Collection<CommentModel>)
        notifyDataSetChanged()
    }

    fun refreshLikeCount(model: CommentModel, adapterPosition: Int) {
//        list[adapterPosition].isLikedByAuthUser = model.isLikedByAuthUser
        list[adapterPosition].totalLikes = model.totalLikes
        notifyItemChanged(adapterPosition)
    }

    open inner class LikesViewHolder(val binding: ItemCommentsBinding) :
        BaseViewHolder(binding.root), CommentsItemViewModel.OnItemClickListener {
        private lateinit var itemViewModel: CommentsItemViewModel

        override fun onBind(position: Int) {
            itemViewModel = CommentsItemViewModel(list[position], this)
            binding.viewModel = itemViewModel
            binding.executePendingBindings()
        }

        override fun onCommentLikeClick() {
            mListener?.onCommentLikeClicked(list[adapterPosition])
        }
    }
}