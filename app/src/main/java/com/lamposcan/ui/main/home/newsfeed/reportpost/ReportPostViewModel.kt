package com.lamposcan.ui.main.home.newsfeed.reportpost

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class ReportPostViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<ReportPostNavigator>(dataManager, mSchedulerProvider) {

    fun onSubmitReport() {
        navigator?.onSubmitClick()
    }
}