package com.lamposcan.ui.main.home.newsfeed.reportpost

import com.lamposcan.ui.base.BaseNavigator

interface ReportPostNavigator : BaseNavigator {
    fun onSubmitClick()
}
