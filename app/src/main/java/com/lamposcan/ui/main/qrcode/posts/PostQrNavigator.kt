package com.lamposcan.ui.main.qrcode.posts
import com.lamposcan.ui.base.BaseNavigator

interface PostQrNavigator : BaseNavigator {
    fun onSubmitClick()
}
