package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers.adapter.FollowersAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class FollowersModule : BaseActivityModule<FollowersViewModel>() {
    @Provides
    fun provideFollowersViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): FollowersViewModel {
        return FollowersViewModel(dataManager, mSchedulerProvider)
    }

    @Provides
    fun provideFollowersAdapter(fragment: FollowersFragment): FollowersAdapter {
        return FollowersAdapter(fragment.context)
    }
}