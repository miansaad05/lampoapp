package com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.NotificationModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider
import java.util.*

class NotificationsViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<NotificationsNavigator>(dataManager, mSchedulerProvider) {
    val isRefreshing = ObservableBoolean(false)

    fun onRefresh() {
        isRefreshing.set(true)
        navigator?.onRefresh()
    }

    fun fetchNotifiations(apiToken: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().getNotifications("Bearer $apiToken")
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val listType =
                                    object : TypeToken<ArrayList<NotificationModel>>() {}.type
                                val notificationList = Gson().fromJson<List<NotificationModel>>(
                                    Gson().toJson(it.data), listType
                                )
                                navigator?.setNotificationList(notificationList)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )

    }
}