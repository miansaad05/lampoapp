package com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.databinding.FriendRequestFragmentBinding
import com.lamposcan.ui.base.baseFragment.BaseFragment
import com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests.adapter.FriendRequestsAdapter
import com.lamposcan.utils.MessageAlert
import com.lamposcan.utils.NetworkUtils
import javax.inject.Inject

class FriendRequestsFragment :
    BaseFragment<FriendRequestFragmentBinding, FriendRequestsViewModel>(),
    FriendRequestsNavigator, FriendRequestsAdapter.OnClickListener {

    companion object {
        fun newInstance() = FriendRequestsFragment()
    }

    @Inject
    lateinit var viewModel: FriendRequestsViewModel
    @Inject
    lateinit var adapter: FriendRequestsAdapter

    private lateinit var binding: FriendRequestFragmentBinding

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.friend_request_fragment
    }

    override fun viewModel(): FriendRequestsViewModel {
        return viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (view != null)
            view.tag = this
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        baseActivity?.let {
            mContext = it
        }

        adapter.setListener(this)
        userModel = getUserModelFromPref()

        binding.rvFriendRequest.itemAnimator = null
        binding.rvFriendRequest.adapter = adapter


        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.getUserRequests(userModel.apiToken)
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun showMessage(message: String) {
        viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
    }

    override fun setUserRequests(usersList: List<UserModel>) {
        adapter.addItems(usersList, false)
        hideLoader()
        viewModel.isRefreshing.set(false)
    }

    override fun onRefresh() {
        if (NetworkUtils.isNetworkConnected(context)) {
            adapter.clearItems()
            viewModel.getUserRequests(userModel.apiToken)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun showLoader() {
        baseActivity?.showLoading()
    }

    override fun hideLoader() {
        baseActivity?.hideLoading()
    }

    override fun onCancel(model: UserModel, position: Int) {
        this.adapterPosition = position
        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.rejectRequest(userModel.apiToken, model.id)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }


/*        this.adapterPosition = position
        adapter.list.removeAt(adapterPosition)
        adapter.notifyItemRemoved(adapterPosition)*/
    }

    var adapterPosition = 0
    override fun onConfirm(model: UserModel, position: Int) {
        this.adapterPosition = position
        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.acceptRequest(userModel.apiToken, model.id)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun deleteListItem(message: String) {
        hideLoader()
        showMessage(message)
        adapter.list.removeAt(adapterPosition)
        adapter.notifyItemRemoved(adapterPosition)
    }
}
