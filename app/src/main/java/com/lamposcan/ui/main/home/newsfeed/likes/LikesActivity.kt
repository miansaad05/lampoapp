package com.lamposcan.ui.main.home.newsfeed.likes

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.LikesModel
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.databinding.LikesActivityBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.newsfeed.likes.adapter.LikesAdapter
import com.lamposcan.utils.NetworkUtils
import javax.inject.Inject


class LikesActivity : BaseActivity<LikesActivityBinding, LikesViewModel>(), LikesNavigator {

    private lateinit var binding: LikesActivityBinding
    @Inject
    lateinit var viewModel: LikesViewModel

    @Inject
    lateinit var likesAdapter: LikesAdapter

    private lateinit var postModel: PostModel

    companion object {
        fun newIntent(context: Context) = Intent(context, LikesActivity::class.java)
        fun newIntent(context: Context, postModel: String) =
            run {
                val intent = Intent(context, LikesActivity::class.java)
                intent.putExtra("postModel", postModel)
            }
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.likes_activity
    }

    override fun viewModel(): LikesViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userModel = getUserModelFromPref()

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this


        intent.hasExtra("postModel").run {
            postModel = Gson().fromJson(intent.getStringExtra("postModel"), PostModel::class.java)

            viewModel.likesCount.set("${postModel.totalLikes} Like")
            if (NetworkUtils.isNetworkConnected(context)) {
                showLoading()
                viewModel.fetchPostLikes(userModel.apiToken, postModel.id)
            } else {
                showMessage(getString(R.string.no_internet))
            }
        }

        binding.rvLikes.adapter = likesAdapter
    }

    override fun setLikes(likesList: List<LikesModel>) {
        viewModel.isRefreshing.set(false)
        hideLoading()
        likesList.let { likesAdapter.addItems(it, false) }
    }

    override fun hideLoader() {
        hideLoading()
    }

    override fun onRefresh() {
        if (NetworkUtils.isNetworkConnected(context)) {
            likesAdapter.clearItems()
            viewModel.fetchPostLikes(userModel.apiToken, postModel.id)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun showLoader() {
        showLoading()
    }
}