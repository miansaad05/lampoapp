package com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests

import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseNavigator

interface FriendRequestsNavigator : BaseNavigator {
    fun showLoader()

    fun hideLoader()

    fun onRefresh()
    fun setUserRequests(usersList: List<UserModel>)
    fun deleteListItem(message: String)
}
