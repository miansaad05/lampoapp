package com.lamposcan.ui.main.home.discover.adapter

import android.text.TextUtils
import androidx.databinding.ObservableField
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.data.remote.ApiEndPoints

class DiscoverPeopleViewModel(userModel: UserModel, val listener: OnClickListener) {

    val userName: ObservableField<String> = ObservableField()
    val userProfileImage: ObservableField<String> = ObservableField("")

    init {
        userName.set(userModel.fname + " " + userModel.lname)
        if (TextUtils.isEmpty(userModel.profile?.profileImage)) {
            userProfileImage.set("")
        } else
            userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + userModel.profile?.profileImage)
    }

    fun onOpenProfileClick() {
        listener.onOpenProfileClick()
    }

    interface OnClickListener {
        fun onOpenProfileClick()
    }
}

