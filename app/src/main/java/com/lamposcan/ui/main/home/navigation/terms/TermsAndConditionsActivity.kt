package com.lamposcan.ui.main.home.navigation.terms

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ActivityTermsAndConditionsBinding
import com.lamposcan.ui.base.BaseActivity
import javax.inject.Inject

class TermsAndConditionsActivity :
    BaseActivity<ActivityTermsAndConditionsBinding, TermsAndConditionsViewModel>(),
    TermsAndConditionsNavigator {

    private lateinit var binding: ActivityTermsAndConditionsBinding
    @Inject
    lateinit var viewModel: TermsAndConditionsViewModel

    companion object {
        fun newIntent(context: Context) = Intent(context, TermsAndConditionsActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_terms_and_conditions
    }

    override fun viewModel(): TermsAndConditionsViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this
    }
}
