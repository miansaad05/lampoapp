package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.FollowingsDataModel
import com.lamposcan.data.model.api.FollowingsModel
import com.lamposcan.databinding.FollowingsFragmentBinding
import com.lamposcan.ui.base.baseFragment.BaseFragment
import com.lamposcan.ui.main.home.navigation.friendslist.FollowFollowingsActivity
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings.adapter.FollowingsAdapter
import com.lamposcan.ui.main.home.navigation.friendslist.unfollowdialog.UnFollowDialog
import com.lamposcan.utils.MessageAlert
import com.lamposcan.utils.NetworkUtils
import javax.inject.Inject

class FollowingsFragment : BaseFragment<FollowingsFragmentBinding, FollowingsViewModel>(),
    FollowingsNavigator, FollowingsAdapter.OnClickListener {

    companion object {
        fun newInstance() = FollowingsFragment()
    }

    @Inject
    lateinit var adapter: FollowingsAdapter
    @Inject
    lateinit var viewModel: FollowingsViewModel

    private lateinit var binding: FollowingsFragmentBinding

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.followings_fragment
    }

    override fun viewModel(): FollowingsViewModel {
        return viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (view != null)
            view.tag = this
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        baseActivity?.let {
            mContext = it
        }
        userModel = getUserModelFromPref()

        adapter.setListener(this)
        binding.rvFollowings.itemAnimator = null
        binding.rvFollowings.adapter = adapter

        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.fetchFollowings(userModel.apiToken)
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun setFollowingsList(followingsModel: FollowingsDataModel) {

        activity?.let {
            if (it is FollowFollowingsActivity)
                it.setRequestsCount(followingsModel.totalNoOfRequests)
        }

        if (followingsModel.followers != null)
            adapter.addItems(followingsModel.followers, false)
        hideLoader()
        viewModel.isRefreshing.set(false)
    }

    override fun showMessage(message: String) {
        viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
    }

    override fun onRefresh() {
        if (NetworkUtils.isNetworkConnected(context)) {
            adapter.clearItems()
            viewModel.fetchFollowings(userModel.apiToken)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun showLoader() {
        baseActivity?.showLoading()
    }

    override fun hideLoader() {
        baseActivity?.hideLoading()
    }

    var adapterPosition: Int = 0
    override fun onUnFollowClick(
        followingsModel: FollowingsModel,
        adapterPosition: Int
    ) {
        this.adapterPosition = adapterPosition
        UnFollowDialog.newInstance(followingsModel).show(childFragmentManager)
    }

    fun onConfirm(stringModel: String) {
        val model: FollowingsModel = Gson().fromJson(stringModel, FollowingsModel::class.java)
        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.unFollowUser(userModel.apiToken, model.id)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun onUnFollowSuccess() {
        hideLoader()
        adapter.list.removeAt(this.adapterPosition)
        adapter.notifyItemRemoved(this.adapterPosition)
    }
}
