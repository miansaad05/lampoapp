package com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab

import com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers.FollowersModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class NotificationsProvider {
    @ContributesAndroidInjector(modules = [NotificationsModule::class])
    abstract fun provideNotificationsFragmentFactory(): NotificationsFragment
}