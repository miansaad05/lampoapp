package com.lamposcan.ui.main.home.newsfeed.reportpost

import com.lamposcan.ui.main.home.newsfeed.reportpost.ReportPostDialog
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ReportPostProvider {
    @ContributesAndroidInjector(modules = [ReportPostModule::class])
    abstract fun provideReportPostFactory() : ReportPostDialog
}