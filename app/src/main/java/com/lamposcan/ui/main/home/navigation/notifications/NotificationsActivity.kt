package com.lamposcan.ui.main.home.navigation.notifications

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ActivityNotificationsBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.navigation.notifications.adapter.NotificationsPagerAdapter
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_follow_followings.*
import javax.inject.Inject

class NotificationsActivity :
    BaseActivity<ActivityNotificationsBinding, NotificationsViewModel>(),
    HasSupportFragmentInjector,
    NotificationsNavigator {

    @Inject
    protected lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var binding: ActivityNotificationsBinding
    @Inject
    lateinit var viewModel: NotificationsViewModel
    @Inject
    lateinit var adapter: NotificationsPagerAdapter

    companion object {
        fun newIntent(context: Context) = Intent(context, NotificationsActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_notifications
    }

    override fun viewModel(): NotificationsViewModel {
        viewModel = ViewModelProviders.of(this, factory).get(NotificationsViewModel::class.java)
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        view_pager.adapter = adapter
        tabs.setupWithViewPager(view_pager)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }
}
