package com.lamposcan.ui.main.home.newsfeed.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.api.Image
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.databinding.ItemImagesTitlePostBinding
import com.lamposcan.databinding.ItemTitlePostBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class PostsAdapter(val context: Context?) : RecyclerView.Adapter<BaseViewHolder>(),
    AdapterUpdateListener {
    var list: MutableList<PostModel> = ArrayList()
    private var listener: OnClickListener? = null

    companion object {
        const val SIMPLE_POST = 1
        const val IMAGES_POST = 2
    }

    fun addData(list: MutableList<PostModel>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(listener: OnClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == SIMPLE_POST) {
            val binding = ItemTitlePostBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
            SimplePostViewHolder(binding)
        } else {
            val binding = ItemImagesTitlePostBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
            ImagesPostViewHolder(binding)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position].hasImages == 1)
            IMAGES_POST
        else
            SIMPLE_POST
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    interface OnClickListener {
        fun onItemClick(model: PostModel)
        fun onLikeClick(model: PostModel, adapterPosition: Int)
        fun openLikeClick(model: PostModel)
        fun onShareClick(model: PostModel)
        fun onCommentClick(model: PostModel)
        fun onOpenProfileClick(postModel: PostModel)
        fun onQrCodeClick(postModel: PostModel)
        fun onOptionsMenuClick(
            postModel: PostModel,
            ivOptions: ImageView
        )

        fun openFullScreen(postModel: PostModel)
    }

    override fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun addItems(obj: Any) {
        list.add(0, obj as PostModel)
        notifyItemInserted(0)
    }

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {
        val size = list.size
        if (size == 0) {
            list.addAll(items as Collection<PostModel>)
            notifyDataSetChanged()
        } else {
            list.addAll(size, items as Collection<PostModel>)
            notifyItemRangeInserted(size + 1, list.size)
        }
    }

    fun refreshLikeCount(
        postModel: PostModel,
        adapterPosition: Int
    ) {
        list[adapterPosition].isLikedByAuthUser = postModel.isLikedByAuthUser
        list[adapterPosition].totalLikes = postModel.totalLikes
        notifyItemChanged(adapterPosition)
    }

    open inner class SimplePostViewHolder(val binding: ItemTitlePostBinding) :
        BaseViewHolder(binding.root), BaseItemViewModel.Listeners {
        private lateinit var itemViewModel: SimplePostItemViewModel
        override fun onBind(position: Int) {
            itemViewModel = SimplePostItemViewModel(list[position], this)
            binding.viewModel = itemViewModel
            binding.executePendingBindings()
        }

        override fun openLike() {
            listener?.openLikeClick(list[adapterPosition])
        }

        override fun onLike() {
            listener?.onLikeClick(list[adapterPosition], adapterPosition)
        }

        override fun onComment() {
            listener?.onCommentClick(list[adapterPosition])
        }

        override fun onShare() {
            listener?.onShareClick(list[adapterPosition])
        }

        override fun openProfile() {
            listener?.onOpenProfileClick(list[adapterPosition])
        }

        override fun onOptionsMenuClick() {
            listener?.onOptionsMenuClick(list[adapterPosition], binding.itemHeader.ivOptions)
        }

        override fun onQrCodeClick() {
            listener?.onQrCodeClick(list[adapterPosition])
        }

        override fun openFullScreen() {
            listener?.openFullScreen(list[adapterPosition])
        }
    }

    open inner class ImagesPostViewHolder(val binding: ItemImagesTitlePostBinding) :
        BaseViewHolder(binding.root), BaseItemViewModel.Listeners,
        MultiPostPagerAdapter.ClickListener {
        private lateinit var itemViewModel: PostsItemViewModel
        override fun onBind(position: Int) {
            itemViewModel = PostsItemViewModel(list[position], this)
            binding.viewModel = itemViewModel

            if (list[position].images != null && list[position].images.size > 0) {
                val adapter = MultiPostPagerAdapter(context, list[position].images)
                adapter.setListener(this)
                binding.viewPager.adapter = adapter
                binding.tabsIndicator.attachTo(binding.viewPager)
            }

            binding.executePendingBindings()
        }

        override fun openFullScreen(moviesList: MutableList<Image>?) {
            listener?.openFullScreen(list[adapterPosition])
        }

        override fun openLike() {
            listener?.openLikeClick(list[adapterPosition])
        }

        override fun openFullScreen() {
            listener?.openFullScreen(list[adapterPosition])
        }

        override fun onLike() {
            listener?.onLikeClick(list[adapterPosition], adapterPosition)
        }

        override fun onComment() {
            listener?.onCommentClick(list[adapterPosition])
        }

        override fun onShare() {
            listener?.onShareClick(list[adapterPosition])
        }

        override fun openProfile() {
            listener?.onOpenProfileClick(list[adapterPosition])
        }

        override fun onOptionsMenuClick() {
            listener?.onOptionsMenuClick(list[adapterPosition], binding.itemHeader.ivOptions)
        }

        override fun onQrCodeClick() {
            listener?.onQrCodeClick(list[adapterPosition])
        }
    }
}