package com.lamposcan.ui.main.home.navigation.notifications.adapter

import android.content.Context
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests.FriendRequestsFragment
import com.lamposcan.ui.main.home.navigation.notifications.adapter.notificationstab.NotificationsFragment


class NotificationsPagerAdapter(val context: Context, val manager: FragmentManager) :
    FragmentStatePagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val TAB_TITLES = arrayOf("Notifications", "Friend Requests")

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return NotificationsFragment.newInstance()
            else -> return FriendRequestsFragment.newInstance()
        }
    }

    override fun getCount(): Int {
        return TAB_TITLES.size
    }

    @Nullable
    override fun getPageTitle(position: Int): CharSequence? {
        return TAB_TITLES[position]
    }
}