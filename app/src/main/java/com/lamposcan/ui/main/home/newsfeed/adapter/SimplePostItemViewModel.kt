package com.lamposcan.ui.main.home.newsfeed.adapter

import android.text.TextUtils
import androidx.databinding.ObservableField
import com.lamposcan.R
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.data.remote.ApiEndPoints

class SimplePostItemViewModel(postModel: PostModel, listener: Listeners) :
    BaseItemViewModel(postModel, listener) {
    val postTitle: ObservableField<String> = ObservableField()
    val postDescription: ObservableField<String> = ObservableField()

    init {
        postTitle.set(postModel.title)
        postDescription.set(postModel.description)
        postTime.set(postModel.createdAt)

        if (postModel.isLikedByAuthUser == 0) {
            likedIcon.set(R.drawable.ic_like)
        } else {
            likedIcon.set(R.drawable.ic_like_green)
        }

        userName.set(postModel.user.fname + " " + postModel.user.lname)

        if (!TextUtils.isEmpty(postModel.user.profile?.city) && !TextUtils.isEmpty(postModel.user.profile?.country))
            userLocation.set(postModel.user.profile?.city + ", " + postModel.user.profile?.country)
        else if (!TextUtils.isEmpty(postModel.user.profile?.city)) {
            userLocation.set(postModel.user.profile?.city)
        } else {
            userLocation.set(postModel.user.profile?.country)
        }
        userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + postModel.user.profile?.profileImage)

        when {
            postModel.totalLikes == 0 ->
                likeCounter.set("0")
            else -> likeCounter.set(postModel.totalLikes.toString())
        }
        when {
            postModel.totalShares == 0 ->
                sharePostCounter.set("0")
            else -> sharePostCounter.set(postModel.totalShares.toString())
        }

        when {
            postModel.totalComments == 0 ->
                commentsCounter.set("0")
            else -> commentsCounter.set(postModel.totalComments.toString())
        }

    }


}