package com.lamposcan.ui.main.home.newsfeed.reportpost

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.newsfeed.reportpost.adapter.TagsAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class ReportPostModule : BaseActivityModule<ReportPostViewModel>() {
    @Provides
    fun provideReportPostViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): ReportPostViewModel {
        return ReportPostViewModel(dataManager, mSchedulerProvider)
    }

    @Provides
    fun provideTagsAdapter(
        dialog: ReportPostDialog
    ): TagsAdapter {
        return TagsAdapter(dialog.context)
    }

}