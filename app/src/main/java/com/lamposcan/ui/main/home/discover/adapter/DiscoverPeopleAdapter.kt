package com.lamposcan.ui.main.home.discover.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.databinding.ItemDiscoverPeopleBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class DiscoverPeopleAdapter(val context: Context?) : RecyclerView.Adapter<BaseViewHolder>(),
    AdapterUpdateListener {
    private var list: MutableList<UserModel> = ArrayList()
    private var listener: OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemDiscoverPeopleBinding.inflate(
            LayoutInflater.from(context), parent, false
        )
        return DiscoverPeopleViewHolder(binding)
    }

    fun setListener(listener: OnClickListener) {
        this.listener = listener
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    interface OnClickListener {
        fun onOpenProfileClick(userModel: UserModel)
    }

    override fun addItems(obj: Any) {
        list.add(0, obj as UserModel)
        notifyItemInserted(0)
    }

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {
        list.addAll(items as Collection<UserModel>)
        notifyDataSetChanged()
    }

    open inner class DiscoverPeopleViewHolder(val binding: ItemDiscoverPeopleBinding) :
        BaseViewHolder(binding.root), DiscoverPeopleViewModel.OnClickListener {
        private lateinit var itemViewModel: DiscoverPeopleViewModel
        override fun onBind(position: Int) {
            itemViewModel = DiscoverPeopleViewModel(list[position], this)
            binding.viewModel = itemViewModel
            binding.executePendingBindings()
        }

        override fun onOpenProfileClick() {
            listener?.onOpenProfileClick(list[adapterPosition])
        }
    }
}