package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings

import com.lamposcan.data.model.api.FollowingsDataModel
import com.lamposcan.ui.base.BaseNavigator

interface FollowingsNavigator : BaseNavigator {
    fun showLoader()

    fun hideLoader()

    fun onRefresh()
    fun setFollowingsList(followingsModel: FollowingsDataModel)
    fun onUnFollowSuccess()
}
