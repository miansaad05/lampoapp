package com.lamposcan.ui.main.home.newsfeed

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class NewsFeedProvider {
    @ContributesAndroidInjector(modules = [NewsFeedModule::class])
    abstract fun provideNewsFeedFactory() : NewsFeedFragment
}