package com.lamposcan.ui.main.qrcode.posts

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.lamposcan.R
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class PostQrViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<PostQrNavigator>(dataManager, mSchedulerProvider) {

    val postImage = ObservableField<String>("")
    val placeHolder: ObservableInt = ObservableInt(R.drawable.ic_error)
    val errorHolder: ObservableInt = ObservableInt(R.drawable.ic_error)

    val postTitle = ObservableField<String>("")
    val postDescription = ObservableField<String>("")
    val username = ObservableField<String>("")

    fun onSubmitReport() {
        navigator?.onSubmitClick()
    }
}