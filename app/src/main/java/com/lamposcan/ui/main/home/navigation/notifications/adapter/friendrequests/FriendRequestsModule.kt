package com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests.adapter.FriendRequestsAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class FriendRequestsModule : BaseActivityModule<FriendRequestsViewModel>() {
    @Provides
    fun provideFollowersViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): FriendRequestsViewModel {
        return FriendRequestsViewModel (dataManager, mSchedulerProvider)
    }

    @Provides
    fun provideFriendRequestsAdapter(fragment: FriendRequestsFragment): FriendRequestsAdapter {
        return FriendRequestsAdapter(fragment.context)
    }
}