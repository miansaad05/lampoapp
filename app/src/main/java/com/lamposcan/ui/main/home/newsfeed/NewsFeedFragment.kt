package com.lamposcan.ui.main.home.newsfeed

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.databinding.NewsFeedFragmentBinding
import com.lamposcan.ui.base.baseFragment.BaseFragment
import com.lamposcan.ui.main.home.newsfeed.adapter.PostsAdapter
import com.lamposcan.ui.main.home.newsfeed.comments.CommentsActivity
import com.lamposcan.ui.main.home.newsfeed.fullscreenview.FullScreenActivity
import com.lamposcan.ui.main.home.newsfeed.likes.LikesActivity
import com.lamposcan.ui.main.home.newsfeed.reportpost.ReportPostDialog
import com.lamposcan.ui.main.profile.viewprofile.ProfileActivity
import com.lamposcan.ui.main.qrcode.posts.PostQrDialog
import com.lamposcan.utils.MessageAlert
import com.lamposcan.utils.NetworkUtils
import com.lamposcan.utils.RequestCodes
import javax.inject.Inject


class NewsFeedFragment : BaseFragment<NewsFeedFragmentBinding, NewsFeedViewModel>(),
    NewsFeedNavigator, PostsAdapter.OnClickListener, ReportPostDialog.Callback {

    companion object {
        fun newInstance() = NewsFeedFragment()
    }

    @Inject
    lateinit var viewModel: NewsFeedViewModel
    @Inject
    lateinit var postsAdapter: PostsAdapter

    private lateinit var binding: NewsFeedFragmentBinding
    private lateinit var reportPostDialog: ReportPostDialog

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.news_feed_fragment
    }

    override fun viewModel(): NewsFeedViewModel {
        return viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (view != null)
            view.tag = this
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        baseActivity?.let {
            mContext = it
        }
        userModel = getUserModelFromPref()

        postsAdapter.setListener(this)

        binding.rvPosts.addItemDecoration(DividerItemDecoration(context, 0))

        binding.rvPosts.itemAnimator = null
        binding.rvPosts.adapter = postsAdapter

        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.fetchPosts(userModel.apiToken)
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun showMessage(message: String) {
        viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
    }

    override fun onRefresh() {
        if (NetworkUtils.isNetworkConnected(context)) {
            postsAdapter.clearItems()
            viewModel.fetchPosts(userModel.apiToken)
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun showLoader() {
        baseActivity?.showLoading()
    }

    override fun hideLoader() {
        baseActivity?.hideLoading()
    }

    override fun setPosts(posts: List<PostModel>?) {
        posts?.let { postsAdapter.addItems(it, false) }
    }

    override fun onResponse() {
        viewModel.isRefreshing.set(false)
        baseActivity?.hideLoading()
    }

    override fun onItemClick(model: PostModel) {

    }

    private var adapterPosition: Int = 0

    override fun onLikeClick(
        model: PostModel,
        adapterPosition: Int
    ) {
        this.adapterPosition = adapterPosition
        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            if (model.isLikedByAuthUser == 0) {
                viewModel.onLikeClick(userModel.apiToken, model.id)
            } else {
                viewModel.onUnLikeClick(userModel.apiToken, model.id)
            }
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun onLikeResponse(postModel: PostModel) {
        hideLoader()
        postsAdapter.refreshLikeCount(postModel, adapterPosition)
    }

    override fun openLikeClick(model: PostModel) {
        startActivity(context?.let { LikesActivity.newIntent(it, Gson().toJson(model)) })
    }

    override fun onShareClick(model: PostModel) {

    }

    override fun onOpenProfileClick(postModel: PostModel) {
        startActivity(context?.let { ProfileActivity.newIntent(it, Gson().toJson(postModel.user)) })
    }

    override fun onCommentClick(model: PostModel) {
        adapterPosition = postsAdapter.list.indexOf(model)
        startActivityForResult(context?.let {
            CommentsActivity.newIntent(
                it,
                Gson().toJson(model)
            )
        }, RequestCodes.DELETE_POST_FROM_COMMENT)
    }

    override fun openFullScreen(postModel: PostModel) {
        startActivity(context?.let {
            FullScreenActivity.newIntent(
                it,
                Gson().toJson(postModel)
            )
        })
    }

    override fun onOptionsMenuClick(
        postModel: PostModel,
        ivOptions: ImageView
    ) {
        adapterPosition = postsAdapter.list.indexOf(postModel)
        if (TextUtils.equals(userModel.id.toString(), postModel.user.id.toString())) {
            showMyPostMenu(postModel, ivOptions)
        } else {
            showOtherPostMenu(postModel, ivOptions)
        }
    }

    private fun showMyPostMenu(postModel: PostModel, ivOptions: ImageView) {
        val wrapper = ContextThemeWrapper(context, R.style.popupMenuStyle)
        val popup = PopupMenu(wrapper, ivOptions)
        //Inflating the Popup using xml file
        popup.menuInflater.inflate(R.menu.my_post_menu, popup.menu)

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_delete_post -> {
                    if (NetworkUtils.isNetworkConnected(context)) {
                        showLoader()
                        viewModel.onDeleteMyPost(userModel.apiToken, postModel.id)
                    } else {
                        showMessage(getString(R.string.no_internet))
                    }
                    true
                }
                else -> true
            }
        }

        popup.show()
    }

    override fun onReportAndDeleteSuccessFully() {
        hideLoader()
        postsAdapter.list.removeAt(adapterPosition)
        postsAdapter.notifyItemRemoved(adapterPosition)
    }

    private fun showOtherPostMenu(postModel: PostModel, ivOptions: ImageView) {
        val wrapper = ContextThemeWrapper(context, R.style.popupMenuStyle)
        val popup = PopupMenu(wrapper, ivOptions)
        //Inflating the Popup using xml file
        popup.menuInflater.inflate(R.menu.other_post_menu, popup.menu)

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_report_post -> {

                    reportPostDialog =
                        ReportPostDialog.newInstance(Gson().toJson(postModel).toString())
                    reportPostDialog.setListener(this)
                    reportPostDialog.show(childFragmentManager)
                    true
                }
                else -> true
            }
        }

        popup.show()
    }

    override fun onSubmitClick(tagName: String, description: String, postModel: String) {

        val model = Gson().fromJson<PostModel>(postModel, PostModel::class.java)

        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.onReport(userModel.apiToken, model.id, tagName, description)
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun onQrCodeClick(postModel: PostModel) {
        val postQrCode =
            PostQrDialog.newInstance(Gson().toJson(postModel).toString())
//        profileQrCodeDialog.setListener(this)
        postQrCode.show(childFragmentManager)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_CANCELED) {
            if (requestCode == RequestCodes.DELETE_POST_FROM_COMMENT) {
                onReportAndDeleteSuccessFully()
            }
        } else
            super.onActivityResult(requestCode, resultCode, data)
    }
}
