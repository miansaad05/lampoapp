package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followers.adapter

import androidx.databinding.ObservableField
import com.lamposcan.data.model.api.FollowersModel
import com.lamposcan.data.remote.ApiEndPoints

class FollowersItemViewModel(followersModel: FollowersModel, mListener: OnClickListener) {
    val userProfileImage: ObservableField<String> = ObservableField()
    val userName: ObservableField<String> = ObservableField()
    val userLocation: ObservableField<String> = ObservableField()
    val followersCount: ObservableField<String> = ObservableField()
    val followingsCount: ObservableField<String> = ObservableField()

    val listener: OnClickListener

    init {
        listener = mListener

        userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + followersModel.profileImage)
        userName.set(followersModel.fname + "  " + followersModel.lname)
        if (followersModel.country == null) {
            userLocation.set("")
        } else {
            userLocation.set("Location : " + followersModel.country)
        }
        followersCount.set(followersModel.followers.toString())
        followingsCount.set(followersModel.followings.toString())
    }

    fun onFollowClick() {
        listener.onFollowClick()
    }

    interface OnClickListener {
        fun onFollowClick()
    }
}