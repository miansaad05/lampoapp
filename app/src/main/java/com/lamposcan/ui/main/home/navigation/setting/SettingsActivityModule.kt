package com.lamposcan.ui.main.home.navigation.setting

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class SettingsActivityModule : BaseActivityModule<SettingsViewModel>() {
    @Provides
    fun provideSettingsViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): SettingsViewModel {
        return SettingsViewModel(dataManager, schedulerProvider)
    }
}
