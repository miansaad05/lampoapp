package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings.adapter.FollowingsAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class FollowingsModule : BaseActivityModule<FollowingsViewModel>() {
    @Provides
    fun provideFollowingsViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): FollowingsViewModel {
        return FollowingsViewModel(dataManager, mSchedulerProvider)
    }


    @Provides
    fun provideFollowersAdapter(fragment: FollowingsFragment): FollowingsAdapter {
        return FollowingsAdapter(fragment.context)
    }
}