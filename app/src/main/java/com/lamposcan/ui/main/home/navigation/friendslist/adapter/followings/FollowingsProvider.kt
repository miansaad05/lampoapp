package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FollowingsProvider {
    @ContributesAndroidInjector(modules = [FollowingsModule::class])
    abstract fun provideFollowingsFactory(): FollowingsFragment
}