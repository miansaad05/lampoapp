package com.lamposcan.ui.main.qrcode.scanqr;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.legacy.app.FragmentCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.lamposcan.R;

import net.quikkly.android.PipelineThreadListener;
import net.quikkly.android.Quikkly;
import net.quikkly.android.ScanResultListener;
import net.quikkly.android.ui.CameraPreview;
import net.quikkly.android.ui.ScanResultsOverlay;
import net.quikkly.android.utils.QuikklyUtils;
import net.quikkly.android.utils.UiUtils;
import net.quikkly.core.ScanResult;
import net.quikkly.core.Tag;

public class ScanFragment extends Fragment {

    private static final int CAMERA_PERMISSION_REQUEST = 11;

    public static final int CAMERA_PREVIEW_FIT_NONE = 0;
    public static final int CAMERA_PREVIEW_FIT_CROP = 1;
    public static final int CAMERA_PREVIEW_FIT_CENTER = 2;

    Quikkly quikkly;
    CameraPreview cameraPreview;
    Camera.Size previewSize;
    ScanResultsOverlay overlay;
    TextView textStatusCode;
    FloatingActionButton selectFromGallery;
    boolean showResultOverlay = false;
    boolean showStatusCode = false;
    boolean permissionGranted = false;
    int cameraPreviewFit = CAMERA_PREVIEW_FIT_NONE;
    double zoom;
    ScanResultListener listener;

    public void setScanResultListener(ScanResultListener listener) {
        this.listener = listener;
    }

    public void setShowResultOverlay(boolean show) {
        showResultOverlay = show;
        if (overlay != null) {
            overlay.setResult(null);
        }
    }

    public void setShowStatusCode(boolean show) {
        showStatusCode = show;
        if (textStatusCode != null) {
            textStatusCode.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    public void setZoom(double zoom) {
        this.zoom = zoom;
        if (cameraPreview != null) {
            cameraPreview.setZoom(zoom);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scan_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        overlay = view.findViewById(R.id.lampo_results_overlay);
        cameraPreview = new CameraPreview(view.getContext());

        selectFromGallery = view.findViewById(R.id.fab_select_from_gallery);
        cameraPreview.setZoom(zoom);
        ((FrameLayout) view.findViewById(R.id.lampo_camera_preview)).addView(cameraPreview);


        selectFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() instanceof ScanCameraActivity) {
                    ((ScanCameraActivity) getActivity()).selectFromGallery();
                }
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        checkAndRequestPermission();
    }

    @Override
    public void onResume() {
        super.onResume();
        quikkly = Quikkly.getInstance();
        if (permissionGranted)
            cameraPreview.requestPreview(getActivity(), cameraPreviewListener);
        else
            Log.w(Quikkly.TAG, "No camera permission, not starting preview");
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraPreview.stopPreview();
        quikkly.destroyScannerThreads();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_REQUEST) {
            if (permissions.length != 1 || !permissions[0].equals(Manifest.permission.CAMERA) || grantResults.length != 1) {
                throw new RuntimeException("Unexpected results from requesting camera permission.");
            }
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permissionGranted = true;
            } else {
                permissionGranted = false;
                showCameraPermissionDeniedDialog();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void checkAndRequestPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            permissionGranted = false;
            FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST);
        } else {
            permissionGranted = true;
        }
    }

    private CameraPreview.PreviewListener cameraPreviewListener = new CameraPreview.PreviewListener() {
        @Override
        public void onPreviewReady(boolean success, Camera.Size size) {
            quikkly.destroyScannerThreads();
            if (success && size != null) {
                previewSize = size;
                resizeCameraPreview();
                quikkly.prepareScannerThreads(size.width, size.height, pipelineListener);
            }
        }

        @Override
        public void onPreviewFrameCaptured(byte[] buffer) {
            quikkly.offerFrame(buffer);
        }

    };

    private void resizeCameraPreview() {
        if (cameraPreview != null && previewSize != null) {
            int vw = cameraPreview.getWidth();
            int vh = cameraPreview.getHeight();
            int pw = Math.min(previewSize.width, previewSize.height);
            int ph = Math.max(previewSize.width, previewSize.height);

            // How many times bigger is the preview image compared to view.
            double wr = 1.0 * pw / vw;
            double hr = 1.0 * ph / vh;

            if (cameraPreviewFit == CAMERA_PREVIEW_FIT_CENTER) {
                if (wr >= hr) {
                    // Width is larger, bars on top and bottom
                    double vish = 1.0 * vw * ph / pw;
                    int bart = (int) Math.floor((vh - vish) / 2);
                    int barb = (int) Math.ceil((vh - vish) / 2);
                    UiUtils.setLayoutMargins(cameraPreview, 0, bart, 0, barb);
                } else {
                    // Height is larger, bars on left and right
                    double visw = 1.0 * vh * pw / ph;
                    int barl = (int) Math.floor((vw - visw) / 2);
                    int barr = (int) Math.ceil((vw - visw) / 2);
                    UiUtils.setLayoutMargins(cameraPreview, barl, 0, barr, 0);
                }
            } else if (cameraPreviewFit == CAMERA_PREVIEW_FIT_CROP) {
                if (wr >= hr) {
                    // Width is larger, cut left and right
                    double totw = 1.0 * vh * pw / ph;
                    int cropl = (int) Math.floor((totw - vw) / 2);
                    int cropr = (int) Math.ceil((totw - vw) / 2);
                    UiUtils.setLayoutMargins(cameraPreview, -cropl, 0, -cropr, 0);
                } else {
                    // Height is larger, cut top and bottom
                    double toth = 1.0 * vw * ph / pw;
                    int cropt = (int) Math.floor((toth - vh) / 2);
                    int cropb = (int) Math.ceil((toth - vh) / 2);
                    UiUtils.setLayoutMargins(cameraPreview, 0, -cropt, 0, -cropb);
                }
            } else {
                UiUtils.setLayoutMargins(cameraPreview, 0, 0, 0, 0);
            }
        }
    }

    private PipelineThreadListener pipelineListener = new PipelineThreadListener() {
        @Override
        public void onScanResult(@Nullable ScanResult scanResult) {

            String stringResult = "";
            // Do something in UI thread with ScanResult.
            if (scanResult != null && !scanResult.isEmpty()) {
                for (Tag tag : scanResult.tags) {
                    stringResult = tag.getData().toString();
                    break;
                }
            }

            if (!TextUtils.isEmpty(stringResult) && getActivity() instanceof ScanCameraActivity) {
                ((ScanCameraActivity) getActivity()).decideNextActivity(stringResult);
            }
        }

        @Override
        public void onFreeBuffer(@NonNull byte[] buffer) {
            cameraPreview.addFreeBuffer(buffer);
        }
    };

    private void showCameraPermissionDeniedDialog() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Activity activity = ScanFragment.super.getActivity();
                if (activity != null) {
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        QuikklyUtils.launchAppSettings(activity);
                    } else {
                        activity.finish();
                    }
                }
            }
        };
        UiUtils.showConfirmDialog(ScanFragment.super.getActivity(), net.quikkly.android.R.string.quikkly_permission_required_dialog_title, net.quikkly.android.R.string.quikkly_permission_camera_denied_message, null, listener);
    }

}
