package com.lamposcan.ui.main.profile.viewprofile.profilefragment

import com.lamposcan.data.model.api.PostModel
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseNavigator

interface ProfileNavigator : BaseNavigator {
    fun showLoader()

    fun hideLoader()
    fun setUserInfo(userModel: UserModel)
    fun setPosts(posts: List<PostModel>)
    fun onReportAndDeleteSuccessFully()
    fun onRefresh()

    fun getVisitedUserId(): Int?
    fun getUserToken(): String
    fun isNetworkConnected(): Boolean?
    fun refreshPage()
    fun onQrCodeClicked()

}
