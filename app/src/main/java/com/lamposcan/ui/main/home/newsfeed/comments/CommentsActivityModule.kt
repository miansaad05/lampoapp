package com.lamposcan.ui.main.home.newsfeed.comments

import android.content.Context
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.newsfeed.comments.adapter.CommentsAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class CommentsActivityModule : BaseActivityModule<CommentsViewModel>() {
    @Provides
    fun provideSplashViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): CommentsViewModel {
        return CommentsViewModel(dataManager, schedulerProvider)
    }

    @Provides
    fun provideCommentsAdapter(context: Context): CommentsAdapter {
        return CommentsAdapter(context)
    }
}
