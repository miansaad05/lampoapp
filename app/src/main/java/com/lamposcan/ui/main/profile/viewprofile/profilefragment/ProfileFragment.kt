package com.lamposcan.ui.main.profile.viewprofile.profilefragment

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.data.remote.ApiEndPoints
import com.lamposcan.databinding.ProfileFragmentBinding
import com.lamposcan.ui.base.baseFragment.BaseFragment
import com.lamposcan.ui.main.home.newsfeed.adapter.PostsAdapter
import com.lamposcan.ui.main.home.newsfeed.comments.CommentsActivity
import com.lamposcan.ui.main.home.newsfeed.fullscreenview.FullScreenActivity
import com.lamposcan.ui.main.home.newsfeed.likes.LikesActivity
import com.lamposcan.ui.main.qrcode.posts.PostQrDialog
import com.lamposcan.ui.main.qrcode.profile.ProfileQrDialog
import com.lamposcan.utils.MessageAlert
import com.lamposcan.utils.NetworkUtils
import javax.inject.Inject

class ProfileFragment : BaseFragment<ProfileFragmentBinding, ProfileViewModel>(),
    ProfileNavigator, PostsAdapter.OnClickListener {

    companion object {
        fun newInstance(userModel: UserModel): ProfileFragment {
            val profileFragment = ProfileFragment()
            val bundle = Bundle()
            bundle.putString("visitedUserModel", Gson().toJson(userModel))
            profileFragment.arguments = bundle
            return profileFragment
        }

        fun newInstance(embededCode: String, embededString: String): Fragment {
            val profileFragment = ProfileFragment()
            val bundle = Bundle()
            bundle.putString("embededCode", embededCode)
            bundle.putString("embededString", embededString)
            profileFragment.arguments = bundle
            return profileFragment
        }

    }

    @Inject
    lateinit var viewModel: ProfileViewModel

    var visitedUserModel: UserModel? = null

    var qrCode: String = ""
    var qrCodeType: String = ""

    private lateinit var binding: ProfileFragmentBinding

    @Inject
    lateinit var postsAdapter: PostsAdapter

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.profile_fragment
    }

    override fun viewModel(): ProfileViewModel {
        return viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (view != null)
            view.tag = this
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        baseActivity?.let {
            mContext = it
        }

        userModel = getUserModelFromPref()
        arguments?.let {
            if (it.containsKey("visitedUserModel")) {
                visitedUserModel =
                    Gson().fromJson(it.getString("visitedUserModel"), UserModel::class.java)
            } else if (it.containsKey("embededCode") && it.containsKey("embededString")) {
                qrCode = it.getString("embededString").toString()
                qrCodeType = it.getString("embededCode").toString()
            }
        }

        postsAdapter.setListener(this)
        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            if (visitedUserModel == null) {
                binding.swipeRefresh.isEnabled = false
                viewModel.getScannedUserInfo(userModel.apiToken, qrCodeType, qrCode)

            } else {
                visitedUserModel?.id?.let { viewModel.getUserProfile(userModel.apiToken, it) }
            }
        } else {
            showMessage(getString(R.string.no_internet))
        }

        binding.rvPosts.itemAnimator = null
        binding.rvPosts.adapter = postsAdapter

    }

    override fun onQrCodeClicked() {
        val profileQrCodeDialog =
            ProfileQrDialog.newInstance(Gson().toJson(visitedUserModel).toString())
//        profileQrCodeDialog.setListener(this)
        profileQrCodeDialog.show(childFragmentManager)
    }

    override fun refreshPage() {
        if (NetworkUtils.isNetworkConnected(context)) {
            viewModel.isDataShown.set(false)
            postsAdapter.clearItems()
            showLoader()
            visitedUserModel?.id?.let { viewModel.getUserProfile(userModel.apiToken, it) }
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun setUserInfo(userModel: UserModel) {
        this.visitedUserModel = userModel
        if (userModel.id == this.userModel.id) {
            viewModel.btnText.set("")
            viewModel.isMyProfile.set(true)
            viewModel.isFollowing.set(false)
            viewModel.isFollowRequestSent.set(false)
        } else {
            if (userModel.isFollowRequestSent == 1) {
                viewModel.isPendingRequest.set(true)
                viewModel.isFollowRequestSent.set(false)
                viewModel.isFollowing.set(false)
                viewModel.isMyProfile.set(true)
            } else if (userModel.isPendingRequest == 1) {
                viewModel.isFollowRequestSent.set(true)
                viewModel.isPendingRequest.set(false)
                viewModel.isFollowing.set(false)
                viewModel.isMyProfile.set(true)
            } else if (userModel.isFollowing == 1 && userModel.isFollower == 1) {
                viewModel.isFollowRequestSent.set(false)
                viewModel.isPendingRequest.set(false)
                viewModel.isFollowing.set(true)
                viewModel.isMyProfile.set(true)
            } else if (userModel.isFollower == 1) {
                viewModel.isFollowRequestSent.set(false)
                viewModel.isPendingRequest.set(false)
                viewModel.isFollowing.set(false)
                viewModel.isMyProfile.set(false)
            } else if (userModel.isFollowing == 1) {
                viewModel.isFollowRequestSent.set(false)
                viewModel.isPendingRequest.set(false)
                viewModel.isFollowing.set(true)
                viewModel.isMyProfile.set(true)
            } else {
                viewModel.isFollowing.set(false)
                viewModel.isFollowRequestSent.set(false)
                viewModel.isPendingRequest.set(false)
                viewModel.isMyProfile.set(false)

            }

            viewModel.btnText.set("Follow")

        }

        viewModel.userName.set(userModel.fname + "  " + userModel.lname)

        if (!TextUtils.isEmpty(userModel.profile?.country)) {

            if (!TextUtils.isEmpty(userModel.profile.city) && !TextUtils.isEmpty(userModel.profile?.country))
                viewModel.userLocation.set(userModel.profile?.city + ", " + userModel.profile?.country)
            else if (!TextUtils.isEmpty(userModel.profile?.city)) {
                viewModel.userLocation.set(userModel.profile?.city)
            } else {
                viewModel.userLocation.set(userModel.profile?.country)
            }

            /*if (!TextUtils.isEmpty(userModel.profile?.city))
                viewModel.userLocation.set(userModel.profile?.country + ", " + userModel.profile.city)
            else {
                viewModel.userLocation.set(userModel.profile?.country)
            }*/
        }
        viewModel.postImageUrl.set(ApiEndPoints.IMAGE_BASE_URL + userModel.profile?.profileImage)

        if (NetworkUtils.isNetworkConnected(context)) {
            if (userModel.profilePrivacy == 1) {
                viewModel.isPublicProfile.set(true)
                viewModel.getUserPosts(this.userModel.apiToken, userModel.id)
            } else {
                viewModel.isDataShown.set(true)
                viewModel.isPublicProfile.set(false)
                hideLoader()
            }
        } else {
            hideLoader()
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun onRefresh() {
        if (NetworkUtils.isNetworkConnected(context)) {
            viewModel.isDataShown.set(false)
            visitedUserModel?.id?.let { viewModel.getUserProfile(userModel.apiToken, it) }
            postsAdapter.clearItems()
        } else {
            viewModel.isRefreshing.set(false)
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun setPosts(posts: List<PostModel>) {
        viewModel.isRefreshing.set(false)
        viewModel.isDataShown.set(true)
        hideLoader()
        posts.let { postsAdapter.addItems(it, false) }
    }

    override fun showMessage(message: String) {
        viewModel.isRefreshing.set(false)
        viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
    }

    override fun showLoader() {
        baseActivity?.showLoading()
    }

    override fun hideLoader() {
        baseActivity?.hideLoading()
    }

    override fun onItemClick(model: PostModel) {

    }

    private var adapterPosition: Int = 0

    override fun getVisitedUserId(): Int? {
        return visitedUserModel?.id
    }

    override fun getUserToken(): String {
        return userModel.apiToken
    }

    override fun onLikeClick(
        model: PostModel,
        adapterPosition: Int
    ) {
        this.adapterPosition = adapterPosition
        /*if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            viewModel.onLikeClick(userModel.apiToken, model.id)
        } else {
            showMessage(getString(R.string.no_internet))
        }*/
    }

    /*override fun onLikeResponse(postModel: PostModel) {
        hideLoader()
        postsAdapter.refreshLikeCount(postModel, adapterPosition)
    }*/
    override fun isNetworkConnected(): Boolean? {
        return if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            true
        } else {
            showMessage(getString(R.string.no_internet))
            false
        }
    }

    override fun openLikeClick(model: PostModel) {
        startActivity(context?.let { LikesActivity.newIntent(it, Gson().toJson(model)) })
    }

    override fun onShareClick(model: PostModel) {

    }

    override fun onOpenProfileClick(postModel: PostModel) {
    }

    override fun onCommentClick(model: PostModel) {
        startActivity(context?.let { CommentsActivity.newIntent(it, Gson().toJson(model)) })
    }

    override fun openFullScreen(postModel: PostModel) {
        startActivity(context?.let {
            FullScreenActivity.newIntent(
                it,
                Gson().toJson(postModel)
            )
        })
    }

    override fun onOptionsMenuClick(
        postModel: PostModel,
        ivOptions: ImageView
    ) {
        if (TextUtils.equals(userModel.id.toString(), postModel.user.id.toString())) {
            showMyPostMenu(postModel, ivOptions)
        } else {
            showOtherPostMenu(postModel, ivOptions)
        }
    }

    private fun showMyPostMenu(postModel: PostModel, ivOptions: ImageView) {
        val wrapper = ContextThemeWrapper(context, R.style.popupMenuStyle)
        val popup = PopupMenu(wrapper, ivOptions)
        //Inflating the Popup using xml file
        popup.menuInflater.inflate(R.menu.my_post_menu, popup.menu)

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_delete_post -> {
                    if (NetworkUtils.isNetworkConnected(context)) {
                        showLoader()
                        viewModel.onDeleteMyPost(userModel.apiToken, postModel.id)
                    } else {
                        showMessage(getString(R.string.no_internet))
                    }
                    true
                }
                else -> true
            }
        }

        popup.show()
    }

    override fun onReportAndDeleteSuccessFully() {
        hideLoader()
        postsAdapter.list.removeAt(adapterPosition)
        postsAdapter.notifyItemRemoved(adapterPosition)
    }

    private fun showOtherPostMenu(postModel: PostModel, ivOptions: ImageView) {
        val wrapper = ContextThemeWrapper(context, R.style.popupMenuStyle)
        val popup = PopupMenu(wrapper, ivOptions)
        //Inflating the Popup using xml file
        popup.menuInflater.inflate(R.menu.other_post_menu, popup.menu)

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_report_post -> {
                    true
                }
                else -> true
            }
        }

        popup.show()
    }


    override fun onQrCodeClick(postModel: PostModel) {
        val postQrCode =
            PostQrDialog.newInstance(Gson().toJson(postModel).toString())
//        profileQrCodeDialog.setListener(this)
        postQrCode.show(childFragmentManager)
    }
}
