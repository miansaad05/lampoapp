package com.lamposcan.ui.main.profile.editprofile

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.navigation.terms.TermsAndConditionsViewModel
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class EditProfileActivityModule : BaseActivityModule<EditProfileViewModel>() {
    @Provides
    fun provideTermsAndConditionsViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): EditProfileViewModel {
        return EditProfileViewModel(dataManager, schedulerProvider)
    }
}
