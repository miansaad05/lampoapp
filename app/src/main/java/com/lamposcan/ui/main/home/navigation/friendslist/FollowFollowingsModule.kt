package com.lamposcan.ui.main.home.navigation.friendslist

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.FollowFollowingPagerAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class FollowFollowingsModule : BaseActivityModule<FollowFollowingsViewModel>() {
    @Provides
    fun provideScanHistoryViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): FollowFollowingsViewModel {
        return FollowFollowingsViewModel(dataManager, schedulerProvider)
    }

    @Provides
    fun provideFollowFollowingPagerAdapter(context: FollowFollowingsActivity): FollowFollowingPagerAdapter {
        return FollowFollowingPagerAdapter(context, context.supportFragmentManager)
    }
}
