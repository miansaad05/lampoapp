package com.lamposcan.ui.main.home.newsfeed.comments

import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.R
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.CommentModel
import com.lamposcan.data.model.api.Data
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.interfaces.ToolbarMenuHelper
import com.lamposcan.utils.rx.SchedulerProvider
import java.util.*

class CommentsViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<CommentsNavigator>(dataManager, schedulerProvider) {
    val userProfileImage = ObservableField<String>("")
    val isRefreshing = ObservableBoolean(false)


    val isImages: ObservableBoolean = ObservableBoolean()
    val isDataLoad: ObservableBoolean = ObservableBoolean()
    val isMultiPost: ObservableBoolean = ObservableBoolean()
    val postImageUrl: ObservableField<String> = ObservableField("")
    val postDescription: ObservableField<String> = ObservableField("")
    val postTitle: ObservableField<String> = ObservableField("")
    val placeHolder: ObservableInt = ObservableInt(R.drawable.ic_error)
    val errorHolder: ObservableInt = ObservableInt(R.drawable.ic_error)
    val postTime: ObservableField<String> = ObservableField("")
    val likedIcon: ObservableInt = ObservableInt()

    val userName: ObservableField<String> = ObservableField("")
    val postUserImage: ObservableField<String> = ObservableField("")
    val postImage: ObservableField<String> = ObservableField("")
    val userLocation: ObservableField<String> = ObservableField("")
    val likesThumb: ObservableField<String> = ObservableField("")
    val likeCounter: ObservableField<String> = ObservableField("")
    val commentsCounter: ObservableField<String> = ObservableField("")
    val sharePostCounter: ObservableField<String> = ObservableField("")


    var mToolbarMenuHelper: ToolbarMenuHelper = object : ToolbarMenuHelper {
        override fun onBackClick() {
            navigator?.onBack()
        }

        override fun onMenuItem() {
        }

    }

    fun fetchPostComments(token: String?, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().getPostComments("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val listType = object : TypeToken<ArrayList<CommentModel>>() {}.type
                                val commentList = Gson().fromJson<List<CommentModel>>(
                                    Gson().toJson(it.data), listType
                                )
                                navigator?.setComments(commentList)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onAddComment() {
        navigator?.getFields()
    }

    fun addComment(token: String?, id: Int, comment: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().addCommentOnPost("Bearer $token", id, comment)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val jsonObject = object : TypeToken<CommentModel>() {}.type
                                val commentModel = Gson().fromJson<CommentModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.addComments(commentModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onRefresh() {
        isRefreshing.set(true)
        navigator?.onRefresh()
    }

    fun onLikeClick(token: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().likeComment("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val jsonObject = object : TypeToken<CommentModel>() {}.type

                                val model = Gson().fromJson<CommentModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.onLikeResponse(model)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onUnLikeClick(token: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().unLikeComment("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {

                                val jsonObject = object : TypeToken<CommentModel>() {}.type

                                val model = Gson().fromJson<CommentModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                navigator?.onLikeResponse(model)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onQrCodeClick() {
        navigator?.onQrCodeClick()
    }

    fun onOptionsMenuClick() {
        navigator?.onOptionsMenuClick()
    }

    fun onDeleteMyPost(token: String, id: Int) {
        compositeDisposable.add(
            dataManager.getApiHelper().deletePost("Bearer $token", id)
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        navigator?.onReportAndDeleteSuccessFully()
                        navigator?.showMessage(it.message)
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun onReport(token: String, id: Int, tagName: String, description: String) {
        compositeDisposable.add(
            dataManager.getApiHelper().reportPost(
                "Bearer $token",
                id.toString(), tagName, description
            )
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        navigator?.onReportAndDeleteSuccessFully()
                        navigator?.showMessage(it.message)
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")

                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }
}
