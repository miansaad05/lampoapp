package com.lamposcan.ui.main.home.conversation

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class ConversationViewModel(dataManager: DataManager, mSchedulerProvider: SchedulerProvider) :
    BaseViewModel<ConversationNavigator>(dataManager, mSchedulerProvider)
