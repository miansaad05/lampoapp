package com.lamposcan.ui.main.home.navigation.friendslist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ActivityFollowFollowingsBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.navigation.friendslist.adapter.FollowFollowingPagerAdapter
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_follow_followings.*
import javax.inject.Inject

class FollowFollowingsActivity :
    BaseActivity<ActivityFollowFollowingsBinding, FollowFollowingsViewModel>(),
    HasSupportFragmentInjector,
    FollowFollowingsNavigator {

    @Inject
    protected lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var binding: ActivityFollowFollowingsBinding
    @Inject
    lateinit var viewModel: FollowFollowingsViewModel
    @Inject
    lateinit var adapter: FollowFollowingPagerAdapter

    companion object {
        fun newIntent(context: Context) = Intent(context, FollowFollowingsActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_follow_followings
    }

    override fun viewModel(): FollowFollowingsViewModel {
        viewModel = ViewModelProviders.of(this, factory).get(FollowFollowingsViewModel::class.java)
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        view_pager.adapter = adapter
        tabs.setupWithViewPager(view_pager)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    fun setRequestsCount(count: Int) {
        viewModel.totalRequestCount.set(count.toString())
    }
}
