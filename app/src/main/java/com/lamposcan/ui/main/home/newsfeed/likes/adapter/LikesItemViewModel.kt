package com.lamposcan.ui.main.home.newsfeed.likes.adapter

import android.text.TextUtils
import androidx.databinding.ObservableField
import com.lamposcan.data.model.api.LikesModel
import com.lamposcan.data.remote.ApiEndPoints

class LikesItemViewModel(userModel: LikesModel) {

    val userName: ObservableField<String> = ObservableField()
    val userProfileImage: ObservableField<String> = ObservableField("")

    init {
        userName.set(userModel.fname + " " + userModel.lname)
        if (TextUtils.isEmpty(userModel.profilePicture)) {
            userProfileImage.set("")
        } else
            userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + userModel.profilePicture)
    }
}

