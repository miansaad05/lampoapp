package com.lamposcan.ui.main.qrcode.profile

import android.graphics.Bitmap
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.data.remote.ApiEndPoints
import com.lamposcan.databinding.ProfileQrCodeBinding
import com.lamposcan.ui.base.BaseDialog
import com.lamposcan.ui.main.qrcode.LampoQrCode.getColorHtmlHex
import com.lamposcan.utils.NetworkUtils
import net.quikkly.android.Quikkly
import net.quikkly.android.render.AndroidSkinBuilder
import java.io.IOException
import java.math.BigInteger
import javax.inject.Inject

class ProfileQrDialog : BaseDialog<ProfileQrCodeBinding, ProfileQrViewModel>(), ProfileQrNavigator {

    private lateinit var binding: ProfileQrCodeBinding
    @Inject
    lateinit var viewModel: ProfileQrViewModel

    private var listener: Callback? = null

    companion object {
        fun newInstance(userModel: String): ProfileQrDialog {
            val fragment = ProfileQrDialog()
            val bundle = Bundle()
            bundle.putString("userModel", userModel)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.profile_qr_code
    }

    override fun viewModel(): ProfileQrViewModel? {
        return viewModel
    }

    private var userModel: UserModel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        dialog?.window?.setBackgroundDrawableResource(R.drawable.ic_lampo_pop_bg)
        baseActivity?.let {
            mContext = it
        }
        TEMPLATES = Quikkly.getInstance().templateIdentifiers
        userModel =
            Gson().fromJson(arguments?.getString("userModel").toString(), UserModel::class.java)

        if (userModel == null) {
            dismiss()
        } else {
            viewModel.username.set(userModel?.fname + " " + userModel?.lname)
        }
        if (NetworkUtils.isNetworkConnected(context)) {
            if (!TextUtils.isEmpty(userModel?.profile?.profileImage)
                && !TextUtils.equals(userModel?.profile?.profileImage, "/storage/")
            ) {
                DownloadImageTask().execute(ApiEndPoints.IMAGE_BASE_URL + userModel?.profile?.profileImage)
            } else {
                generateQrCode()
            }
        } else {
            generateQrCode()
            Toast.makeText(context, getString(R.string.no_internet), Toast.LENGTH_LONG).show()
        }

        return view
    }

    private fun downloadImageFromUrl(): Unit? {
        if (NetworkUtils.isNetworkConnected(mContext)) {
            binding.pbView.visibility = View.VISIBLE

            val imageUrl = ApiEndPoints.IMAGE_BASE_URL + userModel?.profile?.profileImage
            Glide.with(this)
                .asBitmap()
                .load(imageUrl)
                .listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        o: Any,
                        target: Target<Bitmap>,
                        b: Boolean
                    ): Boolean {
                        hideLoading()
                        generateQrCode()
                        /*if (!NetworkUtils.isNetworkConnected(mContext)) {
                        generateQrCode()
                        Toast.makeText(context, getString(R.string.no_internet), Toast.LENGTH_LONG)
                            .show()
                    } else {
                        generateQrCode()
                    }*/
                        return false
                    }

                    override fun onResourceReady(
                        bitmap: Bitmap,
                        o: Any,
                        target: Target<Bitmap>,
                        dataSource: DataSource,
                        b: Boolean
                    ): Boolean {

                        photo = bitmap
                        generateQrCode()
                        /*val extension = ".jpg"

                    val fileName = System.currentTimeMillis().toString()
                    val destination =
                        PathUtil.getInternalUploadFilePath(mContext, fileName, extension)

                    saveBitmap(extension, destination, bitmap)*/

                        return true
                    }
                }
                ).submit()
        } else {
            generateQrCode()
        }

        return null
    }

    private var TEMPLATES = arrayOf("template0001style1")
    internal var photo: Bitmap? = null

    private fun generateQrCode() {
        val template = TEMPLATES[38]

        val dataString = userModel?.qrCode.toString()

        val data = if (TextUtils.isEmpty(dataString)) BigInteger.ZERO else BigInteger(dataString)

        val sb = AndroidSkinBuilder()
            .setBackgroundColor(getColorHtmlHex(-0xf0478c))
            .setBorderColor(getColorHtmlHex(-0x1))
            .setOverlayColor(getColorHtmlHex(-0x1))
            .setMaskColor(getColorHtmlHex(-0xf0478c))
            .setDataColors(arrayOf(getColorHtmlHex(-0x1)))
            .setImageFit(3)
            .setLogoFit(2)
        try {
            if (photo == null) {
                binding.pbView.visibility = View.GONE
                binding.renderQrCode.visibility = View.VISIBLE
                sb.setAssetsImage(context, "ic_default_user.png")
            } else {
                sb.setImage(photo)
            }
        } catch (e: IOException) {
            Log.e(Quikkly.TAG, "Error loading image", e)
        }

        binding.renderQrCode.setAll(template, data, sb.build())
    }

    private inner class DownloadImageTask : AsyncTask<String, Void, Unit>() {
        override fun onPreExecute() {
            binding.pbView.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg p0: String?): Unit? {
            return downloadImageFromUrl()
        }

        override fun onPostExecute(result: Unit?) {
            binding.pbView.visibility = View.GONE
            binding.renderQrCode.visibility = View.VISIBLE
            generateQrCode()
        }
    }

    fun setListener(mListener: Callback) {
        listener = mListener
    }

    fun show(fragmentManager: FragmentManager) {
        super.show(fragmentManager, "TAG")
    }

    interface Callback {
        fun onSubmitClick(tagName: String, description: String, postModel: String)
    }

    override fun onSubmitClick() {
        dialog?.dismiss()
    }

}