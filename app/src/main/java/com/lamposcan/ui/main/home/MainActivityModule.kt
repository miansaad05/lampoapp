package com.lamposcan.ui.main.home

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.adapter.NavigationDrawerAdapter
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class MainActivityModule : BaseActivityModule<MainViewModel>() {
    @Provides
    fun provideMainViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): MainViewModel {
        return MainViewModel(dataManager, schedulerProvider)
    }

    @Provides
    fun provideNavigationAdapter(context: MainActivity): NavigationDrawerAdapter {
        return NavigationDrawerAdapter(context)
    }
}
