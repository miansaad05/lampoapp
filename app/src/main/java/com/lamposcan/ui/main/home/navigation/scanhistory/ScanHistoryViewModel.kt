package com.lamposcan.ui.main.home.navigation.scanhistory
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.interfaces.ToolbarMenuHelper
import com.lamposcan.utils.rx.SchedulerProvider

class ScanHistoryViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<ScanHistoryNavigator>(dataManager, schedulerProvider) {
    var mToolbarMenuHelper: ToolbarMenuHelper = object : ToolbarMenuHelper {
        override fun onBackClick() {
            navigator?.onBack()
        }

        override fun onMenuItem() {
        }

    }
}
