package com.lamposcan.ui.main.home.discover

import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseNavigator

interface DiscoverNavigator : BaseNavigator {

    fun showLoader()

    fun onRefresh()
    fun hideLoader()
    fun setUsersList(usersList: List<UserModel>)
}
