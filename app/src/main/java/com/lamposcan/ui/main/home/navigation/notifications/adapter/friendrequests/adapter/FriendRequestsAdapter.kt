package com.lamposcan.ui.main.home.navigation.notifications.adapter.friendrequests.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.databinding.ItemRequestsBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class FriendRequestsAdapter(val context: Context?) : RecyclerView.Adapter<BaseViewHolder>(),
    AdapterUpdateListener {
    var list: MutableList<UserModel> = ArrayList()
    private var listener: OnClickListener? = null


    fun setListener(listener: OnClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemRequestsBinding.inflate(
            LayoutInflater.from(context), parent, false
        )
        return FriendRequestsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun addItems(obj: Any) {
        list.add(0, obj as UserModel)
        notifyItemInserted(0)
    }

    interface OnClickListener {
        fun onCancel(userModel: UserModel, position: Int)
        fun onConfirm(userModel: UserModel, position: Int)
    }

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {
        list.addAll(items as Collection<UserModel>)
        notifyDataSetChanged()
    }

    open inner class FriendRequestsViewHolder(val binding: ItemRequestsBinding) :
        BaseViewHolder(binding.root), FriendRequestsItemViewModel.OnClickListener {
        private lateinit var itemViewModel: FriendRequestsItemViewModel

        override fun onBind(position: Int) {
            itemViewModel = FriendRequestsItemViewModel(list[position], this)
            binding.viewModel = itemViewModel
            binding.executePendingBindings()
        }

        override fun onCancel() {
            listener?.onCancel(list[adapterPosition], adapterPosition)
        }

        override fun onConfirm() {
            listener?.onConfirm(list[adapterPosition], adapterPosition)
        }
    }
}