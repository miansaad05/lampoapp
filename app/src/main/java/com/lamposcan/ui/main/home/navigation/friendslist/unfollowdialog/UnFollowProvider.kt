package com.lamposcan.ui.main.home.navigation.friendslist.unfollowdialog

import com.lamposcan.ui.main.home.newsfeed.reportpost.ReportPostModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UnFollowProvider {
    @ContributesAndroidInjector(modules = [UnFollowModule::class])
    abstract fun provideUnFollowDialogFactory(): UnFollowDialog
}