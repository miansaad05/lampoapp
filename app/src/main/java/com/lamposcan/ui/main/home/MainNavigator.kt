package com.lamposcan.ui.main.home

import com.lamposcan.ui.base.BaseNavigator

interface MainNavigator : BaseNavigator {

    fun decideNextActivity()

    fun onLogoutClicked()

    fun onEditProfileClicked()
    fun openNotificationsActivity()

    fun openDiscoverActivity()

    fun openDrawerMenu()
}
