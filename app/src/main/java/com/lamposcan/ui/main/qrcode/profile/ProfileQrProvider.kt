package com.lamposcan.ui.main.qrcode.profile

import com.lamposcan.ui.main.home.newsfeed.reportpost.ReportPostModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProfileQrProvider {
    @ContributesAndroidInjector(modules = [ProfileQrModule::class])
    abstract fun provideProfileQrFactory(): ProfileQrDialog
}