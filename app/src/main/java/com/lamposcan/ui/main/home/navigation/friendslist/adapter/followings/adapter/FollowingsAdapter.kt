package com.lamposcan.ui.main.home.navigation.friendslist.adapter.followings.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamposcan.data.model.api.FollowingsModel
import com.lamposcan.databinding.ItemFollowingBinding
import com.lamposcan.ui.base.BaseViewHolder
import com.lamposcan.utils.interfaces.AdapterUpdateListener

class FollowingsAdapter(val context: Context?) : RecyclerView.Adapter<BaseViewHolder>(),
    AdapterUpdateListener {
    var list: MutableList<FollowingsModel> = ArrayList()
    private var listener: OnClickListener? = null


    fun setListener(listener: OnClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemFollowingBinding.inflate(
            LayoutInflater.from(context), parent, false
        )
        return FollowersViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    interface OnClickListener {
        fun onUnFollowClick(
            followingsModel: FollowingsModel,
            adapterPosition: Int
        )
    }

    override fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun addItems(obj: Any) {
        list.add(0, obj as FollowingsModel)
        notifyItemInserted(0)
    }

    override fun addItems(items: Collection<*>, isLoadMore: Boolean) {
        list.addAll(items as Collection<FollowingsModel>)
        notifyDataSetChanged()
    }

    open inner class FollowersViewHolder(val binding: ItemFollowingBinding) :
        BaseViewHolder(binding.root), FollowingsItemViewModel.OnClickListener {
        private lateinit var itemViewModel: FollowingsItemViewModel
        override fun onBind(position: Int) {
            itemViewModel = FollowingsItemViewModel(list[position], this)
            binding.viewModel = itemViewModel
            binding.executePendingBindings()
        }

        override fun onUnFollowClick() {
            listener?.onUnFollowClick(list[adapterPosition], adapterPosition)
        }
    }
}