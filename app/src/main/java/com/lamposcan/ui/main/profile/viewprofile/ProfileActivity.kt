package com.lamposcan.ui.main.profile.viewprofile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.databinding.ActivityProfileBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.profile.viewprofile.profilefragment.ProfileFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class ProfileActivity : BaseActivity<ActivityProfileBinding, ProfileViewModel>(), ProfileNavigator,
    HasSupportFragmentInjector {

    @Inject
    protected lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private lateinit var binding: ActivityProfileBinding
    @Inject
    lateinit var viewModel: ProfileViewModel

    companion object {
        fun newIntent(context: Context, user: String): Intent {
            val intent = Intent(context, ProfileActivity::class.java)
            intent.putExtra("visitedUserModel", user)
            return intent
        }

        fun newIntent(
            context: Context,
            embededCode: String,
            embededString: String
        ): Intent {
            val intent = Intent(context, ProfileActivity::class.java)
            intent.putExtra("embededCode", embededCode)
            intent.putExtra("embededString", embededString)
            return intent
        }

    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_profile
    }

    override fun viewModel(): ProfileViewModel {
        viewModel = ViewModelProviders.of(this, factory).get(ProfileViewModel::class.java)
        return viewModel
    }

    lateinit var visitedUserModel: UserModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }

        viewModel.navigator = this

        userModel = getUserModelFromPref()
        intent?.let {
            if (it.hasExtra("visitedUserModel")) {
                visitedUserModel = Gson().fromJson(
                    intent.getStringExtra("visitedUserModel"),
                    UserModel::class.java
                )
                createFragment()
            } else if (it.hasExtra("embededCode") && it.hasExtra("embededString")) {
                ProfileFragment::class.simpleName?.let {
                    addFragment(
                        ProfileFragment.newInstance(
                            intent.getStringExtra("embededCode"),
                            intent.getStringExtra("embededString")
                        ),
                        it
                    )
                }
            }
        }


    }

    private fun createFragment() {
        ProfileFragment::class.simpleName?.let { it1 ->
            addFragment(
                ProfileFragment.newInstance(visitedUserModel),
                it1
            )
        }
    }

    private fun addFragment(fragment: Fragment, tag: String) {
        val supportFragmentManager = supportFragmentManager
        supportFragmentManager.beginTransaction().add(binding.frameContainer.id, fragment, tag)
            .commit()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    override fun onHandleError(message: String) {

    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onBack() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun showMessage(message: String) {
    }
}
