package com.lamposcan.ui.main.home.newsfeed.likes

import com.lamposcan.data.model.api.LikesModel
import com.lamposcan.ui.base.BaseNavigator

interface LikesNavigator : BaseNavigator {
    fun showLoader()
    fun onRefresh()
    fun hideLoader()
    fun setLikes(likesList: List<LikesModel>)
}
