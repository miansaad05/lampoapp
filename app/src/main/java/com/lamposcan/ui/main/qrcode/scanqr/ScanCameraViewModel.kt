package com.lamposcan.ui.main.qrcode.scanqr

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class ScanCameraViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<ScanCameraNavigator>(dataManager, schedulerProvider)
