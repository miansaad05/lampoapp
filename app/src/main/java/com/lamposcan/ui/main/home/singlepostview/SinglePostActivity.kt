package com.lamposcan.ui.main.home.singlepostview

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.data.remote.ApiEndPoints
import com.lamposcan.databinding.ActivitySinglePostBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.newsfeed.comments.CommentsActivity
import com.lamposcan.ui.main.home.newsfeed.likes.LikesActivity
import com.lamposcan.utils.NetworkUtils
import javax.inject.Inject

class SinglePostActivity : BaseActivity<ActivitySinglePostBinding, SinglePostViewModel>(),
    SinglePostNavigator {

    private lateinit var binding: ActivitySinglePostBinding
    @Inject
    lateinit var viewModel: SinglePostViewModel

    companion object {
        fun newIntent(
            context: Context,
            embededCode: String,
            embededString: String
        ): Intent {
            val intent = Intent(context, SinglePostActivity::class.java)
            intent.putExtra("embededCode", embededCode)
            intent.putExtra("embededString", embededString)

            return intent
        }
    }


    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_single_post
    }

    override fun viewModel(): SinglePostViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userModel = getUserModelFromPref()
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        context = this

        if (intent.hasExtra("embededCode") && intent.hasExtra("embededString")) {
            if (NetworkUtils.isNetworkConnected(context)) {
                val qrCode = intent.getStringExtra("embededString")
                val qrCodeType = intent.getStringExtra("embededCode")
                viewModel.getPostInfo(userModel.apiToken, qrCodeType, qrCode)
                showLoading()
            }
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    lateinit var postModel: PostModel
    override fun setPostInfo(postModel: PostModel) {
        this.postModel = postModel

        hideLoading()
        viewModel.userName.set(postModel.user.fname + " " + postModel.user.lname)
        viewModel.userProfileImage.set(ApiEndPoints.IMAGE_BASE_URL + postModel.user.profile?.profileImage)

        viewModel.postTitle.set(postModel.title)
        viewModel.postDescription.set(postModel.description)
        viewModel.postTime.set(postModel.createdAt)

        when {
            postModel.images != null && postModel.images.size > 1 -> viewModel.isMultiPost.set(true)
            else -> viewModel.isMultiPost.set(false)
        }

        if (postModel.images != null && postModel.images.size > 0) {
            viewModel.postImageUrl.set(ApiEndPoints.IMAGE_BASE_URL + postModel.images.get(0).postImage)
        }

        when {
            postModel.totalLikes == 0 ->
                viewModel.likeCounter.set("0")
            else -> viewModel.likeCounter.set(postModel.totalLikes.toString())
        }
        when {
            postModel.totalShares == 0 ->
                viewModel.sharePostCounter.set("0")
            else -> viewModel.sharePostCounter.set(postModel.totalShares.toString())
        }

        when {
            postModel.totalComments == 0 ->
                viewModel.commentsCounter.set("0")
            else -> viewModel.commentsCounter.set(postModel.totalComments.toString())
        }

        if (!TextUtils.isEmpty(postModel.user.profile?.city) && !TextUtils.isEmpty(postModel.user.profile?.country))
            viewModel.userLocation.set(postModel.user.profile?.city + ", " + postModel.user.profile?.country)
        else if (!TextUtils.isEmpty(postModel.user.profile?.city)) {
            viewModel.userLocation.set(postModel.user.profile?.city)
        } else {
            viewModel.userLocation.set(postModel.user.profile?.country)
        }

        if (postModel.isLikedByAuthUser == 0) {
            viewModel.likedIcon.set(R.drawable.ic_like)
        } else {
            viewModel.likedIcon.set(R.drawable.ic_like_green)
        }

        viewModel.isDataLoad.set(true)
    }

    override fun showLoader() {
        showLoading()
    }

    override fun hideLoader() {
        hideLoading()
    }

    override fun onLikeClick() {
        if (NetworkUtils.isNetworkConnected(context)) {
            showLoader()
            if (postModel.isLikedByAuthUser == 0) {
                viewModel.hitLikeApi(userModel.apiToken, (postModel.id))
            } else {
                viewModel.hitUnLikeApi(userModel.apiToken, (postModel.id))
            }
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun onLikeResponse(postModel: PostModel) {
        hideLoader()
        viewModel.likeCounter.set(postModel.totalLikes.toString())
    }

    override fun openLikeClick() {
        startActivity(context.let { LikesActivity.newIntent(it, Gson().toJson(postModel)) })
    }

    override fun onCommentClick() {
        startActivity(context.let { CommentsActivity.newIntent(it, Gson().toJson(postModel)) })
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }
}
