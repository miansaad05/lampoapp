package com.lamposcan.ui.main.profile.editprofile

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import com.google.gson.Gson
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.Profile
import com.lamposcan.data.model.api.ProfileImageUpdateModel
import com.lamposcan.data.remote.ApiEndPoints
import com.lamposcan.databinding.ActivityEditProfileBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.utils.CommonUtils
import com.lamposcan.utils.MessageAlert
import com.lamposcan.utils.PermissionHandlerHelper
import com.lamposcan.utils.Utility
import com.tramsun.libs.prefcompat.Pref
import kotlinx.android.synthetic.main.input_widget.view.*
import java.io.ByteArrayOutputStream
import javax.inject.Inject

class EditProfileActivity :
    BaseActivity<ActivityEditProfileBinding, EditProfileViewModel>(),
    EditProfileNavigator {

    private lateinit var binding: ActivityEditProfileBinding
    @Inject
    lateinit var viewModel: EditProfileViewModel

    private var fileUri: Uri

    init {
        fileUri = Uri.parse("")
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, EditProfileActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_edit_profile
    }

    override fun viewModel(): EditProfileViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userModel = getUserModelFromPref()
        viewDataBinding?.let { binding = it }
        viewModel.navigator = this

        viewModel.userModel.set(userModel)
        setData()
    }

    private fun setData() {
        viewModel.firstName.set(userModel.fname)
        viewModel.lastName.set(userModel.lname)
        viewModel.fullName.set(viewModel.firstName.get() + " " + viewModel.lastName.get())
        viewModel.email.set(userModel.email)
        viewModel.country.set(userModel.profile?.country)
        viewModel.city.set(userModel.profile?.city)
        viewModel.dob.set(userModel.dob)

        if (TextUtils.isEmpty(userModel.profile?.profileImage))
            viewModel.imageUrl.set("")
        else {
            viewModel.imageUrl.set(ApiEndPoints.IMAGE_BASE_URL + userModel.profile?.profileImage)
        }

        binding.ipLastDob.et_input.setText(viewModel.dob.get().toString())
        when {
            userModel.gender == 1 -> viewModel.gender.set(1)
            else -> viewModel.gender.set(0)
        }
    }

    override fun getFields() {
        viewModel.firstName.set(binding.etFirstName.text.toString().trim())
        viewModel.lastName.set(binding.etLastName.text.toString().trim())
        viewModel.country.set(binding.etCountry.text.toString().trim())
        viewModel.city.set(binding.etCity.text.toString().trim())
        viewModel.dob.set(binding.ipLastDob.et_input.text.toString().trim())

        when {
            binding.rbMale.isChecked -> viewModel.gender.set(1)
            else -> viewModel.gender.set(0)
        }
        viewModel.submitFields()
    }

    override fun showLoader() {
        showLoading()
    }

    override fun showMessage(message: String) {
        MessageAlert.showInformationMessage(
            binding.root,
            message
        )
    }

    override fun onResponse() {
        hideLoading()
    }

    override fun hideLoader() {
        hideLoading()
    }

    override fun onResponse(profileImageUpdateModel: ProfileImageUpdateModel) {

        if (userModel.profile == null) {
            userModel.profile = Profile()
            userModel.profile?.profileImage =
                ApiEndPoints.IMAGE_BASE_URL + profileImageUpdateModel.profileImage
        } else {
            userModel.profile?.profileImage = profileImageUpdateModel.profileImage
        }
        Pref.putString(CommonUtils.USER_MODEL, Gson().toJson(userModel))
        viewModel.userModel.set(userModel)
        hideLoading()
    }

    override fun updateProfilePicClicked() {
        checkPermissions()
    }

    private fun checkPermissions() {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.CAMERA
            ) === PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) === PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) === PackageManager.PERMISSION_GRANTED
        ) {
            open()
        } else {
            askCameraPermission(
                Manifest.permission.CAMERA,
                CommonUtils.CAMERA_PERMISSION_RESULT
            )
        }
    }

    private fun open() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems =
            arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallery()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    private fun choosePhotoFromGallery() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, CommonUtils.GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CommonUtils.CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == CommonUtils.CAMERA) {
                data?.let {
                    val imageBitmap = it.extras?.get("data") as Bitmap?
                    imageBitmap?.let {
                        fileUri = getImageUri(context, imageBitmap)
                        showLoading()
                        uploadPrescriptionToServer()
                    }
                }

            } else if (requestCode == CommonUtils.GALLERY) {
                data?.data?.let {
                    fileUri = it
                    getBitmapFromGallery(data)
                    showLoading()
                    uploadPrescriptionToServer()
                }
            }
        }
    }

    private fun uploadPrescriptionToServer() {
        viewModel.imageUrl.set(fileUri.toString())
        val imagePath = getRealPathFromURI(fileUri, this)

        imagePath?.let {
            viewModel.updateProfilePicture(
                it
            )
        }
    }

    fun getRealPathFromURI(contentURI: Uri, context: Activity): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = context.managedQuery(contentURI, projection, null, null, null) ?: return null
        val column_index = cursor
            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        return if (cursor.moveToFirst()) {
            // cursor.close();
            cursor.getString(column_index)
        } else null
        // cursor.close();
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        if (requestCode == CommonUtils.CAMERA_PERMISSION_RESULT) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                askCameraPermission(permissions[0], CommonUtils.CAMERA_PERMISSION_RESULT)
            } else {
                askStoragePermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    CommonUtils.STORAGE_PERMISSION_RESULT
                )
            }
        } else if (requestCode == CommonUtils.STORAGE_PERMISSION_RESULT) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                askStoragePermission(permissions[0], CommonUtils.STORAGE_PERMISSION_RESULT)
            } else {
                open()
            }
        }
    }

    private fun getBitmapFromGallery(data: Intent) {
        val pickedImage = data.data
        // Let's read picked image path using content resolver
        val filePath = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = pickedImage?.let { contentResolver.query(it, filePath, null, null, null) }
        cursor?.moveToFirst()
        val imagePath = cursor?.getString(cursor.getColumnIndex(filePath[0]))

        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888

        val imageBitmap = BitmapFactory.decodeFile(imagePath, options)

        cursor?.close()

        fileUri = getImageUri(this, imageBitmap)

    }

    private fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val path =
            MediaStore.Images.Media.insertImage(
                inContext.contentResolver,
                inImage,
                "Title",
                null
            )
        return Uri.parse(path)
    }

    private fun askCameraPermission(permission: String, flag: Int) {
        PermissionHandlerHelper.checkPermissionHelper(
            this,
            arrayOf(permission),
            object : PermissionHandlerHelper.CheckPermissionResponse {
                override fun permissionGranted() {
                    askStoragePermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        CommonUtils.STORAGE_PERMISSION_RESULT
                    )
                }

                override fun showNeededPermissionDialog() {
                    AlertDialog.Builder(context)
                        .setMessage("Camera permission require to take photo. Please grant this app storage Permission.")
                        .setPositiveButton(
                            "Settings"
                        ) { dialog, which -> Utility.openPermissionsInSettings(context as Activity) }
                        .create().show()
                }

                @TargetApi(Build.VERSION_CODES.M)
                override fun requestPermission() {
                    requestPermissions(arrayOf(permission), flag)
                }
            })
    }

    private fun askStoragePermission(permission: String, flag: Int) {
        PermissionHandlerHelper.checkPermissionHelper(
            this,
            PermissionHandlerHelper.STORAGE_PERMISSION,
            object : PermissionHandlerHelper.CheckPermissionResponse {
                override fun permissionGranted() {

                }

                override fun showNeededPermissionDialog() {
                    AlertDialog.Builder(context)
                        .setMessage("Permissions require to select Photo. Please grant this app storage Permission.")
                        .setPositiveButton(
                            "Settings"
                        ) { dialog, which -> Utility.openPermissionsInSettings(context as Activity) }
                        .create().show()
                }

                @TargetApi(Build.VERSION_CODES.M)
                override fun requestPermission() {
                    requestPermissions(arrayOf(permission), flag)
                }
            })
    }
}
