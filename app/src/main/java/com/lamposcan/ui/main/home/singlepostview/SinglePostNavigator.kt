package com.lamposcan.ui.main.home.singlepostview

import com.lamposcan.data.model.api.PostModel
import com.lamposcan.ui.base.BaseNavigator

interface SinglePostNavigator : BaseNavigator {
    fun setPostInfo(postModel: PostModel)

    fun showLoader()

    fun hideLoader()
    fun onLikeClick()
    fun openLikeClick()
    fun onCommentClick()

    fun onLikeResponse(postModel: PostModel)
}
