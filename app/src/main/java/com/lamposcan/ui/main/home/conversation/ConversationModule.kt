package com.lamposcan.ui.main.home.conversation

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class ConversationModule : BaseActivityModule<ConversationViewModel>() {
    @Provides
    fun provideNewsFeedViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): ConversationViewModel {
        return ConversationViewModel(dataManager, mSchedulerProvider)
    }
}