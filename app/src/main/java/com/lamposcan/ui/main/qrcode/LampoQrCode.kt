package com.lamposcan.ui.main.qrcode

import androidx.annotation.ColorInt

object LampoQrCode {
    private var TEMPLATES = arrayOf("template0001style1")


    fun getColorHtmlHex(@ColorInt color: Int): String {
        val hex = Integer.toHexString(color and 0x00FFFFFF).toUpperCase()
        return '#'.toString() + "000000".substring(0, 6 - hex.length) + hex
    }
}