package com.lamposcan.ui.main.home.newsfeed

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.ui.main.home.newsfeed.adapter.PostsAdapter
import com.lamposcan.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class NewsFeedModule : BaseActivityModule<NewsFeedViewModel>() {
    @Provides
    fun provideNewsFeedViewModel(
        dataManager: DataManager,
        mSchedulerProvider: SchedulerProvider
    ): NewsFeedViewModel {
        return NewsFeedViewModel(dataManager, mSchedulerProvider)
    }

    @Provides
    fun providePostsAdapter(fragment: NewsFeedFragment): PostsAdapter {
        return PostsAdapter(fragment.context)
    }
}