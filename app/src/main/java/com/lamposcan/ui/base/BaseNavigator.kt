package com.lamposcan.ui.base

interface BaseNavigator {
    fun onHandleError(message: String)

    fun onBack()

    fun showMessage(message: String)
}
