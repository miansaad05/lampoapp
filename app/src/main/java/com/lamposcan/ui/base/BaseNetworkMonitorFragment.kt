package com.lamposcan.ui.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.databinding.ViewDataBinding

import com.lamposcan.ui.base.baseFragment.BaseFragment
import com.lamposcan.utils.NetworkUtils


abstract class BaseNetworkMonitorFragment<T : ViewDataBinding, V : BaseViewModel<*>> :
    BaseFragment<T, V>() {


    private val networkStateReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            if (NetworkUtils.isNetworkConnected(context)) {
                onNetworkConnected()
            }
        }
    }

    abstract fun onNetworkConnected()

    override fun onResume() {
        super.onResume()

        baseActivity!!.registerReceiver(
            networkStateReceiver,
            IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    override fun onPause() {
        baseActivity!!.unregisterReceiver(networkStateReceiver)
        super.onPause()
    }
}
