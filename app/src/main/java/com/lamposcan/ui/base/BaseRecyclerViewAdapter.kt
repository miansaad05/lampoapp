package com.lamposcan.ui.base

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter<T : RecyclerView.ViewHolder> : RecyclerView.Adapter<T>() {
    val VIEW_TYPE_NORMAL = 1

    protected abstract fun createNormalItemViewHolder(parent: ViewGroup): T

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): T {
        when (viewType) {
            VIEW_TYPE_NORMAL -> return createNormalItemViewHolder(parent)
            else -> return createNormalItemViewHolder(parent)
        }
    }
}
