package com.lamposcan.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lamposcan.ViewModelProviderFactory

import dagger.Module
import dagger.Provides

@Module
abstract class BaseActivityModule<T : ViewModel> {

    @Provides
    protected fun viewModelProvider(mViewModel: T): ViewModelProvider.Factory {
        return ViewModelProviderFactory(mViewModel)
    }
}
