package com.lamposcan.ui.base

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.PostModel
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference
import java.util.ArrayList

abstract class BaseViewModel<N>(
    val dataManager: DataManager,
    private val mSchedulerProvider: SchedulerProvider
) : ViewModel() {
    lateinit var userModel: ObservableField<UserModel>
    var isLoading: ObservableBoolean

    var menuIcon: ObservableInt
    var logginUser: ObservableBoolean? = null
    val compositeDisposable: CompositeDisposable
    private var mNavigator: WeakReference<N>? = null

    var navigator: N?
        get() = if (mNavigator == null) null else mNavigator!!.get()
        set(navigator) {
            this.mNavigator = WeakReference<N>(navigator)
        }

    init {
        this.compositeDisposable = CompositeDisposable()
        /*this.logginUser = new ObservableBoolean(dataManager.getPreferencesHelper()
                .getCurrentUserLoggedInMode() == LoggedInMode.LOGGED_IN_MODE_SERVER.getType()
                || dataManager.getPreferencesHelper().getCurrentUserLoggedInMode()
                == LoggedInMode.LOGGED_IN_MODE_FB.getType());*/

        this.isLoading = ObservableBoolean()
        this.menuIcon = ObservableInt(0)
    }

    fun setLogginUser(logginUser: Boolean) {
        this.logginUser!!.set(logginUser)
    }

    fun getLogginUser(): Boolean {
        return logginUser!!.get()
    }

    fun setMenuIcon(menuIcon: Int) {
        this.menuIcon.set(menuIcon)
    }

    fun setIsLoading(isLoading: Boolean) {
        this.isLoading.set(isLoading)
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    fun getmSchedulerProvider(): SchedulerProvider {
        return mSchedulerProvider
    }
}
