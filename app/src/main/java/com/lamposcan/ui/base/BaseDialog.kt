package com.lamposcan.ui.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.RelativeLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import dagger.android.support.AndroidSupportInjection
import java.lang.ref.WeakReference


abstract class BaseDialog<T : ViewDataBinding, V : BaseViewModel<*>> : DialogFragment(),
    BaseNavigator {

    /**
     * This function will return instance of activity to which the fragment dialog belongs
     *
     * @return instance of BaseActivity
     */

    var baseActivity: BaseActivity<*, *>? = null

    protected var weakBaseActivity: WeakReference<BaseActivity<*, *>>? = null
    protected var mRootView: View? = null
    protected var viewDataBinding: T? = null
    protected var mViewModel: V? = null

    lateinit var mContext: Context

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun bindingVariable(): Int

    /**
     * @return layout resource id
     */

    abstract fun layoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun viewModel(): V?

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // the content
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        // creating the fullscreen dialog
        //final Dialog dialog = new Dialog(getContext());
        baseActivity = activity as BaseActivity<*, *>?
        val dialog = Dialog(this.baseActivity!!)


        baseActivity?.let {
            mContext = it
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        if (dialog.window != null) {
//            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            dialog.window?.setGravity(Gravity.CENTER)
        }
        dialog.setCanceledOnTouchOutside(false)



        return dialog
    }

    private fun performDataBinding(inflater: LayoutInflater, container: ViewGroup?): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, layoutId(), container, false)
        this.mViewModel = if (mViewModel == null) viewModel() else mViewModel
        viewDataBinding?.setVariable(bindingVariable(), mViewModel)
        viewDataBinding?.executePendingBindings()
        return viewDataBinding?.root

    }

    fun performDependencyInjection() {
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        performDependencyInjection()

        return performDataBinding(inflater, container)
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        //super.onSaveInstanceState(outState);
    }

    override fun show(fragmentManager: FragmentManager, tag: String?) {
        /*FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment prevFragment = fragmentManager.findFragmentByTag(tag);
        if (prevFragment != null) {
            transaction.remove(prevFragment);
        }
        transaction.addToBackStack(null);
        show(transaction, tag);*/

        fragmentManager.beginTransaction().add(this, tag).commitAllowingStateLoss()
    }

    /**
     * This function will dismiss the FragmentDialog
     *
     * @param tag the Tag of the fragment that we want to dismiss
     */
    fun dismissDialog(tag: String) {
        try {
            if (fragmentManager != null) {
                // dismiss();
                dismissAllowingStateLoss()
            }
            /*if (getBaseActivity() != null)
                getBaseActivity().onFragmentDetached(tag);*/
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * This function will hide loading indicator
     */
    fun hideLoading() {
        if (baseActivity != null) {
            baseActivity?.hideLoading()
        }
    }

    /**
     * This function is to show loading indicator when doing background tasks
     */
    fun showLoading() {
        if (baseActivity != null) {
            baseActivity?.showLoading()
        }
    }

    override fun onHandleError(message: String) {
        baseActivity?.onHandleError(message)
    }

    override fun onBack() {
        dialog?.dismiss()
    }

    override fun showMessage(message: String) {

    }
}
