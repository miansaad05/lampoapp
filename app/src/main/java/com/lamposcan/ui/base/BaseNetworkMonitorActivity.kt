package com.lamposcan.ui.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.databinding.ViewDataBinding
import com.lamposcan.utils.NetworkUtils


abstract class BaseNetworkMonitorActivity<T : ViewDataBinding, V : BaseViewModel<*>> :
    BaseActivity<T, V>() {

    private val networkStateReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            if (NetworkUtils.isNetworkConnected(context)) {
                onNetworkConnected()
            }
        }
    }

    abstract fun onNetworkConnected()

    override fun onResume() {
        super.onResume()
        registerReceiver(
            networkStateReceiver,
            IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    override fun onPause() {
        unregisterReceiver(networkStateReceiver)
        super.onPause()
    }
}

