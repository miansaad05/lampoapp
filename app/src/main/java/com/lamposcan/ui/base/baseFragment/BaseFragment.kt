package com.lamposcan.ui.base.baseFragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.base.BaseNavigator
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.ui.registeration.login.LoginActivity
import com.lamposcan.utils.AppLogger
import com.lamposcan.utils.CommonUtils
import com.lamposcan.utils.MessageAlert
import com.tramsun.libs.prefcompat.Pref
import dagger.android.support.AndroidSupportInjection
import java.lang.ref.WeakReference

abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel<*>> : Fragment(), BaseNavigator {

    var baseActivity: BaseActivity<*, *>? = null

    protected var weakBaseActivity: WeakReference<BaseActivity<*, *>>? = null
    protected var mRootView: View? = null
    protected var viewDataBinding: T? = null
    protected var mViewModel: V? = null

    lateinit var mContext: Context
    lateinit var userModel: UserModel
    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun bindingVariable(): Int

    /**
     * @return layout resource id
     */

    abstract fun layoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun viewModel(): V?


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            val activity = context as BaseActivity<*, *>?
            this.baseActivity = activity
            weakBaseActivity = WeakReference<BaseActivity<*, *>>(activity)

        }
    }


    fun getWeakBaseActivity(): BaseActivity<*, *>? {
        return weakBaseActivity?.get()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)
        baseActivity = activity as BaseActivity<*, *>?

        baseActivity?.let {
            mContext = it
        }

        mViewModel = viewModel()
        setHasOptionsMenu(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, layoutId(), container, false)
        mRootView = viewDataBinding?.root
        return mRootView
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.setVariable(bindingVariable(), mViewModel)
        viewDataBinding?.executePendingBindings()
    }

    /**
     * This function will hide loading indicator
     */
    /*fun hideKeyboard() {
        if (baseActivity != null) {
            baseActivity.hideKeyboard()
        }
    }*/

    fun performDependencyInjection() {
        AndroidSupportInjection.inject(this)
    }

    override fun onBack() {
        baseActivity?.finish()
    }



    override fun onHandleError(message: String) {
        if (message.contains("Unauthenticated") || message.contains("Invalid")) {
            onSessionExpire()
        } else {
            viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
        }
    }

    fun getUserModelFromPref(): UserModel {
        val gsonModel = Pref.getString(CommonUtils.USER_MODEL)
        AppLogger.d("getUserModelFromPref : ", Pref.getString(CommonUtils.USER_MODEL))

        if (TextUtils.isEmpty(gsonModel)) onSessionExpire()
        userModel = Gson().fromJson(gsonModel, UserModel::class.java)
        if (TextUtils.isEmpty(userModel.apiToken)) onSessionExpire()
        return userModel
    }

    fun onSessionExpire() {
        baseActivity?.let {
            Toast.makeText(baseActivity, "Your Sessions has been Expired", Toast.LENGTH_LONG)
                .show()
                Pref.putString(CommonUtils.USER_MODEL, "")
                val intent = LoginActivity.newIntent(it)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                it.finishAffinity()
        }
    }

    interface Callback {

        fun onFragmentAttached()

        fun onFragmentDetached(tag: String)
    }
}
