@file:Suppress("DEPRECATION")

package com.lamposcan.ui.base


import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.MotionEvent
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.registeration.login.LoginActivity
import com.lamposcan.utils.AppLogger
import com.lamposcan.utils.CommonUtils
import com.lamposcan.utils.MessageAlert
import com.tramsun.libs.prefcompat.Pref
import dagger.android.AndroidInjection

abstract class BaseActivity<T : ViewDataBinding, V : ViewModel> : AppCompatActivity(),
    BaseNavigator {

    lateinit var context: Context
    protected var postponeDataBinding: Boolean = false
    var viewDataBinding: T? = null
    lateinit var userModel: UserModel
    private var mViewModel: V? = null
    private var lockTouch: Boolean = false

    private var mProgressDialog: ProgressDialog? = null

    abstract fun bindingVariable(): Int

    @LayoutRes
    abstract fun layoutId(): Int

    abstract fun viewModel(): V

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)

        context = this

        if (!postponeDataBinding)
            performDataBinding()
    }

    fun performDependencyInjection() {
        AndroidInjection.inject(this)
    }

    protected fun performDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutId())

        this.mViewModel = if (mViewModel == null) viewModel() else mViewModel

        viewDataBinding?.setVariable(bindingVariable(), mViewModel)

        viewDataBinding?.executePendingBindings()
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        try {
            return lockTouch || super.dispatchTouchEvent(ev)
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }

    }

    fun lockTouch(lockTouch: Boolean) {
        this.lockTouch = lockTouch
    }

    override fun onHandleError(message: String) {
        hideLoading()
        viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
    }

    override fun onBack() {
        super.onBackPressed()
    }

    override fun showMessage(message: String) {
        viewDataBinding?.root?.let { MessageAlert.showErrorMessage(it, message) }
    }

    fun showLoading() {
        hideLoading()
        mProgressDialog = CommonUtils.showLoadingDialog(context, false)
        // setProgressDialogCancelable(true);
    }

    fun hideLoading() {
        mProgressDialog?.cancel()
    }

    fun getUserModelFromPref(): UserModel {
        val gsonModel = Pref.getString(CommonUtils.USER_MODEL)
        AppLogger.d("getUserModelFromPref : ", Pref.getString(CommonUtils.USER_MODEL))

        if (TextUtils.isEmpty(gsonModel)) onSessionExpire()
        userModel = Gson().fromJson(gsonModel, UserModel::class.java)
        if (TextUtils.isEmpty(userModel.apiToken)) onSessionExpire()
        return userModel
    }

    fun onSessionExpire() {
        Toast.makeText(applicationContext, "Your Sessions has been Expired", Toast.LENGTH_LONG)
            .show()
        Handler().postDelayed({
            Pref.putString(CommonUtils.USER_MODEL, "")
            val intent = LoginActivity.newIntent(context)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finishAffinity()
        }, 100)

    }
}
