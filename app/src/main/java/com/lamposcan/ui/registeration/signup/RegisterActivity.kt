package com.lamposcan.ui.registeration.signup

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.DatePicker
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.databinding.ActivityRegisterBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.MainActivity
import com.lamposcan.utils.MessageAlert
import com.lamposcan.utils.NetworkUtils
import kotlinx.android.synthetic.main.input_widget.view.*
import java.util.*
import javax.inject.Inject

class RegisterActivity : BaseActivity<ActivityRegisterBinding, RegisterViewModel>(),
    RegisterNavigator {

    private lateinit var binding: ActivityRegisterBinding
    @Inject
    lateinit var viewModel: RegisterViewModel

    companion object {
        fun newIntent(context: Context) = Intent(context, RegisterActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_register
    }

    override fun viewModel(): RegisterViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this
    }

    override fun showMessage(message: String) {
        MessageAlert.showErrorMessage(binding.root, message)
    }

    override fun hideLoader() {
        hideLoading()
    }

    override fun validateInputFields() {
        viewModel.firstName.set(binding.ipFirstName.et_input.text.toString().trim())
        viewModel.lastName.set(binding.ipLastName.et_input.text.toString().trim())
        viewModel.email.set(binding.ipEmail.et_input.text.toString().trim())
        viewModel.password.set(binding.ipPassword.et_input.text.toString().trim())
        viewModel.confirmPassword.set(binding.ipConfirmPassword.et_input.text.toString().trim())
        viewModel.dob.set(binding.ipLastDob.et_input.text.toString().trim())

        when {
            binding.rbMale.isChecked -> viewModel.gender.set(1)
            else -> viewModel.gender.set(0)
        }
        if (NetworkUtils.isNetworkConnected(context)) {
            showLoading()
            viewModel.submitFields()
        } else {
            showMessage(getString(R.string.no_internet))
        }
    }

    override fun selectDob() {
        val calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)

        val picker = DatePickerDialog(
            context,
            object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(
                    p0: DatePicker?,
                    year: Int,
                    monthOfYear: Int,
                    dayOfMonth: Int
                ) {
                    binding.ipLastDob.et_input.setText("$dayOfMonth/" + (monthOfYear + 1) + "/ $year")
                    viewModel.dob.set(binding.ipLastDob.et_input.text.toString().trim())
                }

            }, year, month, day
        )
        picker.show()
    }

    override fun showLoader() {
        showLoading()
    }

    override fun onResponse(userModel: UserModel) {
        hideLoading()
        startActivity(MainActivity.newIntent(context))
        finishAffinity()
    }
}
