package com.lamposcan.ui.registeration.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.databinding.ActivityLoginBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.main.home.MainActivity
import com.lamposcan.ui.registeration.forgotpassword.ForgotPasswordActivity
import com.lamposcan.ui.registeration.signup.RegisterActivity
import com.lamposcan.utils.NetworkUtils
import kotlinx.android.synthetic.main.input_widget.view.*
import javax.inject.Inject

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>(), LoginNavigator {

    private lateinit var binding: ActivityLoginBinding
    @Inject
    lateinit var viewModel: LoginViewModel

    companion object {
        fun newIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_login
    }

    override fun viewModel(): LoginViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this
    }

    override fun validateFields() {
        viewModel.email.set(binding.ipEmail.et_input.text.toString().trim())
        viewModel.password.set(binding.ipPassword.et_input.text.toString().trim())


        if (NetworkUtils.isNetworkConnected(context)) {
            showLoading()
            viewModel.submitFields()
        } else {
            showMessage(getString(R.string.no_internet))
        }

    }

    override fun onForgotPasswordClicked() {
        startActivity(ForgotPasswordActivity.newIntent(context))
    }

    override fun onCreateAccountClicked() {
        startActivity(RegisterActivity.newIntent(context))
    }

    override fun hideLoader() {
        hideLoading()
    }

    override fun showLoader() {
        showLoading()
    }

    override fun onResponse(userModel: UserModel) {
        hideLoading()
        startActivity(MainActivity.newIntent(context))
        finishAffinity()
    }
}
