package com.lamposcan.ui.registeration.success

import android.os.Handler
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class SuccessViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<SuccessNavigator>(dataManager, schedulerProvider) {

    fun onLoginClick(){
        navigator?.onLoginClicked()
    }
}
