package com.lamposcan.ui.registeration.forgotpassword

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.rx.SchedulerProvider

class ForgotPasswordViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<ForgotPasswordNavigator>(dataManager, schedulerProvider) {

    fun goToNextActivity() {

    }

    fun onBack() {
        navigator?.goBack()
    }
}
