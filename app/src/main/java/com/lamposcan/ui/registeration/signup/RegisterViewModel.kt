package com.lamposcan.ui.registeration.signup

import android.text.TextUtils
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.CommonUtils
import com.lamposcan.utils.rx.SchedulerProvider
import com.lamposcan.utils.validation.Validator
import com.tramsun.libs.prefcompat.Pref

class RegisterViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<RegisterNavigator>(dataManager, schedulerProvider) {

    val firstName: ObservableField<String>
    val lastName: ObservableField<String>
    val email: ObservableField<String>
    val password: ObservableField<String>
    val confirmPassword: ObservableField<String>
    val dob: ObservableField<String>
    val gender: ObservableInt

    init {
        firstName = ObservableField()
        lastName = ObservableField()
        email = ObservableField()
        password = ObservableField()
        confirmPassword = ObservableField()
        dob = ObservableField()
        gender = ObservableInt()
    }

    fun onSubmit() {
        navigator?.validateInputFields()
    }

    private fun hitRegisterApi() {
        compositeDisposable.add(
            dataManager.getApiHelper().registerUser(
                firstName.get().toString().trim(),
                lastName.get().toString().trim(),
                email.get().toString().trim(),
                password.get().toString().trim(),
                confirmPassword.get().toString().trim(),
                dob.get().toString().trim(),
                gender.get()
            )
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {
                                val jsonObject = object : TypeToken<UserModel>() {}.type
                                val userModel = Gson().fromJson<UserModel>(
                                    Gson().toJson(it.data), jsonObject
                                )

                                Pref.putString(CommonUtils.USER_MODEL, Gson().toJson(userModel))
                                navigator?.onResponse(userModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }

    fun submitFields() {
        when {
            !Validator.validateName(firstName.get().toString()) -> {
                navigator?.hideLoader()
                navigator?.showMessage("Please enter first name")
                return
            }
            !Validator.validateName(lastName.get().toString()) -> {
                navigator?.showMessage("Please enter last name")
                return
            }
            !Validator.validateEmail(email.get().toString()) -> {
                navigator?.hideLoader()
                navigator?.showMessage("Email not valid")
                return
            }
            !Validator.validatePassword(password.get().toString()) -> {
                navigator?.hideLoader()
                navigator?.showMessage("Password must be greater then 6.")
                return
            }
            !Validator.validateConfirmPassword(
                password.get().toString(),
                confirmPassword.get().toString()
            ) -> {
                navigator?.hideLoader()
                navigator?.showMessage("Password not matched!")
                return
            }
            !Validator.validateDob(dob.get().toString()) -> {
                navigator?.hideLoader()
                navigator?.showMessage("Please select Date of Birth")
                return
            }
            else -> {
                navigator?.showLoader()
                hitRegisterApi()
            }
        }
    }

    fun onSelectDob() {
        navigator?.selectDob()
    }

    fun onAlreadyAccount() {
        navigator?.onBack()
    }
}
