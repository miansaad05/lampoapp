package com.lamposcan.ui.registeration.signup

import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseNavigator

interface RegisterNavigator : BaseNavigator {
    fun onResponse(userModel: UserModel)

    fun validateInputFields()

    fun selectDob()
    fun hideLoader()

    fun showLoader()

}
