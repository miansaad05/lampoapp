package com.lamposcan.ui.registeration.login

import android.text.TextUtils
import androidx.databinding.ObservableField
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lamposcan.data.DataManager
import com.lamposcan.data.model.api.Data
import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseViewModel
import com.lamposcan.utils.CommonUtils
import com.lamposcan.utils.rx.SchedulerProvider
import com.lamposcan.utils.validation.Validator
import com.tramsun.libs.prefcompat.Pref

class LoginViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider) :
    BaseViewModel<LoginNavigator>(dataManager, schedulerProvider) {


    val email: ObservableField<String>
    val password: ObservableField<String>

    init {
        email = ObservableField()
        password = ObservableField()
    }

    fun onLoginClick() {
        navigator?.validateFields()
    }

    fun onForgotPasswordClick() {
        navigator?.onForgotPasswordClicked()
    }

    fun onCreateAccountClick() {
        navigator?.onCreateAccountClicked()
    }

    fun submitFields() {
        when {
            !Validator.validateEmail(email.get().toString()) -> {
                navigator?.hideLoader()
                navigator?.showMessage("Email not valid")
                return
            }
            !Validator.validatePassword(password.get().toString()) -> {
                navigator?.hideLoader()
                navigator?.showMessage("Password must be greater then 6.")
                return
            }
            else -> {
                hitLoginApi()
            }
        }
    }

    private fun hitLoginApi() {
        compositeDisposable.add(
            dataManager.getApiHelper().loginUser(
                email.get().toString().trim(),
                password.get().toString().trim()
            )
                .subscribeOn(getmSchedulerProvider().io()).observeOn(getmSchedulerProvider().ui())
                .subscribe({
                    if (it.code == 200) {
                        it?.let(fun(it: Data): Unit? {
                            return it.data?.let(fun(it1: Any) {
                                val jsonObject = object : TypeToken<UserModel>() {}.type
                                val userModel = Gson().fromJson<UserModel>(
                                    Gson().toJson(it.data), jsonObject
                                )
                                Pref.putString(CommonUtils.USER_MODEL, Gson().toJson(userModel))
                                navigator?.onResponse(userModel)
                            })
                        })
                    } else {
                        navigator?.hideLoader()
                        navigator?.onHandleError(it.message)
                    }
                }, {
                    navigator?.hideLoader()
                    if (it != null) {
                        val error = it as ANError

                        if (!TextUtils.isEmpty(error.errorBody))
                            navigator?.onHandleError(error.errorBody)
                        else
                            navigator?.onHandleError("No Internet Connection Available")


                    } else {
                        navigator?.onHandleError("No Internet Connection Available")
                    }
                })
        )
    }
}
