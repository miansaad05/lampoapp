package com.lamposcan.ui.registeration.login
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class LoginActivityModule : BaseActivityModule<LoginViewModel>() {
    @Provides
    fun provideLoginViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): LoginViewModel {
        return LoginViewModel(dataManager, schedulerProvider)
    }
}
