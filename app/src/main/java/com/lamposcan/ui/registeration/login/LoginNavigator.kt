package com.lamposcan.ui.registeration.login

import com.lamposcan.data.model.api.UserModel
import com.lamposcan.ui.base.BaseNavigator

interface LoginNavigator : BaseNavigator {

    fun onForgotPasswordClicked()

    fun onCreateAccountClicked()

    fun validateFields()

    fun showLoader()

    fun hideLoader()

    fun onResponse(userModel: UserModel)
}
