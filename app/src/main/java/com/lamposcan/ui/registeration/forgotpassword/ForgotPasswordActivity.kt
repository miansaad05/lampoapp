package com.lamposcan.ui.registeration.forgotpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ActivityForgotPasswordBinding
import com.lamposcan.ui.base.BaseActivity
import javax.inject.Inject

class ForgotPasswordActivity :
    BaseActivity<ActivityForgotPasswordBinding, ForgotPasswordViewModel>(),
    ForgotPasswordNavigator {

    private lateinit var binding: ActivityForgotPasswordBinding
    @Inject
    lateinit var viewModel: ForgotPasswordViewModel

    companion object {
        fun newIntent(context: Context) = Intent(context, ForgotPasswordActivity::class.java)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_forgot_password
    }

    override fun viewModel(): ForgotPasswordViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this
    }

    override fun goBack() {
        super.onBack()
    }
}
