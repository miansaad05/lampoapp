package com.lamposcan.ui.registeration.success
import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class SuccessActivityModule : BaseActivityModule<SuccessViewModel>() {
    @Provides
    fun provideLoginViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): SuccessViewModel {
        return SuccessViewModel(dataManager, schedulerProvider)
    }
}
