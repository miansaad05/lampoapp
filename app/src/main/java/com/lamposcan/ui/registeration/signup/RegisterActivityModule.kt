package com.lamposcan.ui.registeration.signup

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class RegisterActivityModule : BaseActivityModule<RegisterViewModel>() {
    @Provides
    fun provideRegisterViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): RegisterViewModel {
        return RegisterViewModel(dataManager, schedulerProvider)
    }
}
