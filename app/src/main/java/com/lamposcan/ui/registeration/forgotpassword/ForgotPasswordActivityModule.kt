package com.lamposcan.ui.registeration.forgotpassword

import com.lamposcan.data.DataManager
import com.lamposcan.ui.base.BaseActivityModule
import com.lamposcan.utils.rx.SchedulerProvider

import dagger.Module
import dagger.Provides

@Module
class ForgotPasswordActivityModule : BaseActivityModule<ForgotPasswordViewModel>() {
    @Provides
    fun provideLoginViewModel(
        dataManager: DataManager,
        schedulerProvider: SchedulerProvider
    ): ForgotPasswordViewModel {
        return ForgotPasswordViewModel(dataManager, schedulerProvider)
    }
}
