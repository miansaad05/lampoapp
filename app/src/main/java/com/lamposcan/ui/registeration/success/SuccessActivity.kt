package com.lamposcan.ui.registeration.success

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.lamposcan.BR
import com.lamposcan.R
import com.lamposcan.databinding.ActivitySuccessBinding
import com.lamposcan.ui.base.BaseActivity
import com.lamposcan.ui.registeration.login.LoginActivity
import javax.inject.Inject

class SuccessActivity : BaseActivity<ActivitySuccessBinding, SuccessViewModel>(), SuccessNavigator {

    private lateinit var binding: ActivitySuccessBinding
    @Inject
    lateinit var viewModel: SuccessViewModel

    companion object {
        fun newIntent(context: Context) = Intent(context, SuccessActivity::class.java)
    }


    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_success
    }

    override fun viewModel(): SuccessViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding?.let { binding = it }
        viewModel.navigator = this
    }

    override fun onLoginClicked() {
        startActivity(LoginActivity.newIntent(context))
        finishAffinity()
    }
}
