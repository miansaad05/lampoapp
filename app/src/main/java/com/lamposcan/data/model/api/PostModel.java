package com.lamposcan.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostModel {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("is_public")
    @Expose
    private int isPublic;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("has_images")
    @Expose
    private int hasImages;
    @SerializedName("is_liked_by_auth_user")
    @Expose
    private int isLikedByAuthUser;
    @SerializedName("user")
    @Expose
    private UserModel user;
    @SerializedName("total_likes")
    @Expose
    private int totalLikes;
    @SerializedName("total_comments")
    @Expose
    private int totalComments;
    @SerializedName("total_shares")
    @Expose
    private int totalShares;
    @SerializedName("qr_code")
    @Expose
    private int qrCode;
    @SerializedName("is_reported_by_auth_user")
    @Expose
    private int isReportedByAuthUser;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getHasImages() {
        return hasImages;
    }

    public void setHasImages(int hasImages) {
        this.hasImages = hasImages;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public int getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(int totalLikes) {
        this.totalLikes = totalLikes;
    }

    public int getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(int totalComments) {
        this.totalComments = totalComments;
    }

    public int getTotalShares() {
        return totalShares;
    }

    public void setTotalShares(int totalShares) {
        this.totalShares = totalShares;
    }

    public int getIsLikedByAuthUser() {
        return isLikedByAuthUser;
    }

    public void setIsLikedByAuthUser(int isLikedByAuthUser) {
        this.isLikedByAuthUser = isLikedByAuthUser;
    }

    public int getQrCode() {
        return qrCode;
    }

    public void setQrCode(int qrCode) {
        this.qrCode = qrCode;
    }

    public int getIsReportedByAuthUser() {
        return isReportedByAuthUser;
    }

    public void setIsReportedByAuthUser(int isReportedByAuthUser) {
        this.isReportedByAuthUser = isReportedByAuthUser;
    }
}