package com.lamposcan.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FollowersDataModel {

    @SerializedName("followers")
    @Expose
    private List<FollowersModel> followers = null;
    @SerializedName("total_no_of_requests")
    @Expose
    private int totalNoOfRequests;

    public List<FollowersModel> getFollowers() {
        return followers;
    }

    public void setFollowers(List<FollowersModel> followers) {
        this.followers = followers;
    }

    public int getTotalNoOfRequests() {
        return totalNoOfRequests;
    }

    public void setTotalNoOfRequests(int totalNoOfRequests) {
        this.totalNoOfRequests = totalNoOfRequests;
    }

}