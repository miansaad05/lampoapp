package com.lamposcan.data.model.others;

public class PostImagesModel {

    private String name;
    private boolean isUrlEmpty = true;

    public PostImagesModel(String name) {
        this.name = name;
    }

    public PostImagesModel(String name, boolean isUrlEmpty) {
        this.name = name;
        this.isUrlEmpty = isUrlEmpty;
    }

    public boolean isUrlEmpty() {
        return isUrlEmpty;
    }

    public void setUrlEmpty(boolean urlEmpty) {
        isUrlEmpty = urlEmpty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}