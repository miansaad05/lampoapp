package com.lamposcan.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("lname")
    @Expose
    private String lname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("email_verified_at")
    @Expose
    private String emailVerifiedAt;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("gender")
    @Expose
    private int gender;
    @SerializedName("is_follower")
    @Expose
    private int isFollower;
    @SerializedName("is_following")
    @Expose
    private int isFollowing;
    @SerializedName("is_pending_request")
    @Expose
    private int isPendingRequest;
    @SerializedName("is_follow_request_sent")
    @Expose
    private int isFollowRequestSent;
    @SerializedName("token")
    @Expose
    private String apiToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("profile")
    @Expose
    private Profile profile;

    @SerializedName("qr_code")
    @Expose
    private int qrCode;

    @SerializedName("is_public")
    @Expose
    private int profilePrivacy;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(String emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public int getProfilePrivacy() {
        return profilePrivacy;
    }

    public void setProfilePrivacy(int profilePrivacy) {
        this.profilePrivacy = profilePrivacy;
    }

    public int getIsFollower() {
        return isFollower;
    }

    public void setIsFollower(int isFollower) {
        this.isFollower = isFollower;
    }

    public int getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(int isFollowing) {
        this.isFollowing = isFollowing;
    }

    public int getIsPendingRequest() {
        return isPendingRequest;
    }

    public void setIsPendingRequest(int isPendingRequest) {
        this.isPendingRequest = isPendingRequest;
    }

    public int getIsFollowRequestSent() {
        return isFollowRequestSent;
    }

    public void setIsFollowRequestSent(int isFollowRequestSent) {
        this.isFollowRequestSent = isFollowRequestSent;
    }

    public int getQrCode() {
        return qrCode;
    }

    public void setQrCode(int qrCode) {
        this.qrCode = qrCode;
    }
}
