package com.lamposcan.data.model.others;

public class TagModel {

    private String tagName;
    private boolean isSelected;

    public TagModel(String tagName, boolean isSelected) {
        this.tagName = tagName;
        this.isSelected = isSelected;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}