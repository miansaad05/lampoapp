package com.lamposcan.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FollowingsDataModel {

    @SerializedName("followings")
    @Expose
    private List<FollowingsModel> followers = null;
    @SerializedName("total_no_of_requests")
    @Expose
    private int totalNoOfRequests;

    public List<FollowingsModel> getFollowers() {
        return followers;
    }

    public void setFollowers(List<FollowingsModel> followers) {
        this.followers = followers;
    }

    public int getTotalNoOfRequests() {
        return totalNoOfRequests;
    }

    public void setTotalNoOfRequests(int totalNoOfRequests) {
        this.totalNoOfRequests = totalNoOfRequests;
    }

}