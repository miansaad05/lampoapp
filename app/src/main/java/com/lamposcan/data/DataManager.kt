package com.lamposcan.data

import com.lamposcan.data.prefs.PreferenceHelper
import com.lamposcan.data.remote.ApiHelper
import com.lamposcan.utils.ResourceProvider

interface DataManager : PreferenceHelper, ApiHelper {

    fun getPreferencesHelper(): PreferenceHelper

    fun getResourceProvider() : ResourceProvider
    fun getApiHelper() : ApiHelper

    fun setUserAsLoggedOut()
}
