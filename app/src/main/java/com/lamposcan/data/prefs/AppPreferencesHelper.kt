package com.lamposcan.data.prefs

import android.content.Context
import android.content.SharedPreferences
import com.lamposcan.di.PreferenceInfo

import javax.inject.Inject

class AppPreferencesHelper
@Inject
constructor(context: Context, @PreferenceInfo fileName: String) : PreferenceHelper {

    val mPrefs = context.getSharedPreferences(fileName, Context.MODE_PRIVATE)

    override fun clearPreferences() {
        mPrefs.edit().clear().apply()
    }
}
