package com.lamposcan.data.remote

import com.lamposcan.data.model.api.Data
import io.reactivex.Single
import java.io.File

interface ApiHelper {
    fun registerUser(
        fname: String,
        lname: String,
        email: String,
        password: String,
        confirm_password: String,
        dob: String,
        gender: Int
    ): Single<Data>


    fun loginUser(
        email: String,
        password: String
    ): Single<Data>

    fun updateUserProfile(
        fname: String,
        lname: String,
        email: String,
        country: String,
        city: String,
        dob: String,
        gender: Int,
        apiToken: String
    ): Single<Data>

    fun updateUserProfilePicture(
        image: File,
        token: String
    ): Single<Data>

    fun createPost(
        title: String,
        description: String,
        token: String,
        isPublic: Int,
        files: MutableList<File>
    ): Single<Data>


    fun fetchPosts(
        token: String
    ): Single<Data>

    fun likePosts(
        token: String,
        id: Int
    ): Single<Data>

    fun likeComment(
        token: String,
        id: Int
    ): Single<Data>

    fun unLikePost(
        token: String,
        id: Int
    ): Single<Data>

    fun unLikeComment(
        token: String,
        id: Int
    ): Single<Data>

    fun getPostLikes(
        token: String,
        id: Int
    ): Single<Data>

    fun getPostComments(
        token: String,
        id: Int
    ): Single<Data>

    fun addCommentOnPost(
        token: String,
        id: Int,
        comment: String
    ): Single<Data>

    fun getUserProfile(
        token: String,
        id: Int
    ): Single<Data>

    fun getUserPost(
        token: String,
        id: Int
    ): Single<Data>

    fun getUserFollowers(
        token: String
    ): Single<Data>

    fun getUserFollowings(
        token: String
    ): Single<Data>

    fun deletePost(
        token: String,
        id: Int
    ): Single<Data>


    fun unFollowUser(
        token: String,
        id: Int
    ): Single<Data>

    fun addFriend(
        token: String,
        id: Int
    ): Single<Data>

    fun acceptRequest(
        token: String,
        id: Int
    ): Single<Data>

    fun rejectRequest(
        token: String,
        id: Int
    ): Single<Data>

    fun getUserRequests(
        token: String
    ): Single<Data>

    fun getNotifications(
        token: String
    ): Single<Data>

    fun getUsersList(
        token: String
    ): Single<Data>

    fun reportPost(
        token: String,
        postId: String,
        tagName: String,
        description: String
    ): Single<Data>

    fun getScanResult(
        token: String,
        qrCodeType: String,
        qrCode: String
    ): Single<Data>
}
