package com.lamposcan.data.remote

import com.lamposcan.data.model.api.Data
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Single
import java.io.File
import javax.inject.Inject

class AppApiHelper
@Inject
constructor() : ApiHelper {
    override fun registerUser(
        fname: String,
        lname: String,
        email: String,
        password: String,
        confirm_password: String,
        dob: String,
        gender: Int
    ): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.METHOD_REGISTER)
            .addBodyParameter("fname", fname)
            .addBodyParameter("lname", lname)
            .addBodyParameter("email", email)
            .addBodyParameter("password", password)
            .addBodyParameter("password_confirmation", confirm_password)
            .addBodyParameter("dob", dob)
            .addBodyParameter("gender", gender.toString())
            .build().getObjectSingle(Data::class.java)
    }

    override fun loginUser(email: String, password: String): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.METHOD_LOGIN)
            .addBodyParameter("email", email)
            .addBodyParameter("password", password)
            .build().getObjectSingle(Data::class.java)
    }

    override fun updateUserProfile(
        fname: String,
        lname: String,
        email: String,
        country: String,
        city: String,
        dob: String,
        gender: Int,
        apiToken: String
    ): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.USER_UPDATE)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", apiToken)
            .addBodyParameter("fname", fname)
            .addBodyParameter("lname", lname)
            .addBodyParameter("email", email)
            .addBodyParameter("country", country)
            .addBodyParameter("city", city)
            .addBodyParameter("dob", dob)
            .addBodyParameter("gender", gender.toString())
            .build().getObjectSingle(Data::class.java)
    }

    override fun updateUserProfilePicture(image: File, token: String): Single<Data> {
        return Rx2AndroidNetworking.upload(ApiEndPoints.API_BASE_URL + ApiEndPoints.USER_UPDATE_PROFILE_PIC)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .addMultipartFile("image", image)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun createPost(
        title: String,
        description: String,
        token: String,
        isPublic: Int,
        files: MutableList<File>
    ): Single<Data> {
        return Rx2AndroidNetworking.upload(ApiEndPoints.API_BASE_URL + ApiEndPoints.CREATE_POST)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .addMultipartParameter("title", title)
            .addMultipartParameter("is_public", isPublic.toString())
            .addMultipartParameter("description", description)
            .addMultipartFileList("images[]", files)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun fetchPosts(
        token: String
    ): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.GET_POST)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun likePosts(
        token: String,
        id: Int
    ): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.LIKE_POST + "/$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun unLikePost(
        token: String,
        id: Int
    ): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.UN_LIKE_POST + "/$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }


    override fun likeComment(
        token: String,
        id: Int
    ): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.LIKE_COMMENT + "/$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun unLikeComment(
        token: String,
        id: Int
    ): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.UN_LIKE_COMMENT + "/$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun getPostLikes(
        token: String,
        id: Int
    ): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.GET_POST_LIKES + "/$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun getPostComments(
        token: String,
        id: Int
    ): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.GET_POST_COMMENTS + "/$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun addCommentOnPost(
        token: String,
        id: Int,
        comment: String
    ): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.ADD_COMMENTS + "/$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .addBodyParameter("comment", comment)
            .build()
            .getObjectSingle(Data::class.java)
    }


    override fun getUserPost(token: String, id: Int): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.GET_USER_POSTS + "/$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun getUserProfile(token: String, id: Int): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.GET_USER_PROFILE + "/$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun getUserFollowers(token: String): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.FOLLOWERS)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun getUserFollowings(token: String): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.FOLLOWINGS)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }


    override fun deletePost(token: String, id: Int): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.DELETE_POST + "$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun unFollowUser(token: String, id: Int): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.UNFOLLOW_USER + "$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun getUserRequests(token: String): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.PENDING_REQUESTS)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun addFriend(token: String, id: Int): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.ADD_FRIEND + "$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun getNotifications(token: String): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.GET_NOTIFICATIONS)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun getUsersList(token: String): Single<Data> {
        return Rx2AndroidNetworking.get(ApiEndPoints.API_BASE_URL + ApiEndPoints.GET_USERS)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun acceptRequest(token: String, id: Int): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.ACCEPT_REQUEST + "$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun getScanResult(token: String, qrCodeType: String, qrCode: String): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.SCAN_POST)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .addBodyParameter("qr_code_type", qrCodeType)
            .addBodyParameter("qr_code", qrCode)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun reportPost(
        token: String,
        postId: String,
        tagName: String,
        description: String
    ): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.REPORT_POST)
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .addBodyParameter("post_id", postId)
            .addBodyParameter("tag_name", tagName)
            .addBodyParameter("description", description)
            .build()
            .getObjectSingle(Data::class.java)
    }

    override fun rejectRequest(token: String, id: Int): Single<Data> {
        return Rx2AndroidNetworking.post(ApiEndPoints.API_BASE_URL + ApiEndPoints.CANCEL_REQUEST + "$id")
            .addHeaders("Accept", "application/json")
            .addHeaders("Authorization", token)
            .build()
            .getObjectSingle(Data::class.java)
    }
}
