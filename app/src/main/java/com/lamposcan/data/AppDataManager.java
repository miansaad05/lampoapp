package com.lamposcan.data;

import android.content.Context;

import com.google.gson.Gson;
import com.lamposcan.data.model.api.Data;
import com.lamposcan.data.prefs.PreferenceHelper;
import com.lamposcan.data.remote.ApiHelper;
import com.lamposcan.utils.ResourceProvider;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppDataManager implements DataManager {

    final PreferenceHelper mPreferencesHelper;
    final ResourceProvider mResourceProvider;
    final ApiHelper apiHelper;
    private final Context mContext;
    private final Gson mGson;

    @Inject
    public AppDataManager(Context context, PreferenceHelper preferencesHelper, Gson gson, ResourceProvider resourceProvider,
                          ApiHelper apiHelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mGson = gson;
        this.mResourceProvider = resourceProvider;
        this.apiHelper = apiHelper;
    }

    @Override
    public void setUserAsLoggedOut() {

    }

    @Override
    public ResourceProvider getResourceProvider() {
        return mResourceProvider;
    }

    @Override
    public void clearPreferences() {
        mPreferencesHelper.clearPreferences();
    }

    @Override
    public PreferenceHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    @NotNull
    @Override
    public Single<Data> registerUser(String fname, String lname,
                                     String email, String password,
                                     String confirm_password, String dob, int gender) {
        return apiHelper.registerUser(fname, lname, email, password, confirm_password, dob, gender);
    }

    @NotNull
    @Override
    public Single<Data> loginUser(@NotNull String email, @NotNull String password) {
        return apiHelper.loginUser(email, password);
    }

    @NotNull
    @Override
    public Single<Data> updateUserProfile(@NotNull String fname, @NotNull String lname, @NotNull String email, @NotNull String country, @NotNull String city, @NotNull String dob, int gender, @NotNull String apiToken) {
        return apiHelper.updateUserProfile(fname, lname, email, country, city, dob, gender, "Bearer ${userModel.get()?.apiToken}");
    }

    @NotNull
    @Override
    public Single<Data> updateUserProfilePicture(@NotNull File image, @NotNull String token) {
        return apiHelper.updateUserProfilePicture(image, token);
    }


    @NotNull
    @Override
    public Single<Data> createPost(@NotNull String title, @NotNull String description, @NotNull String token, int isPublic, List<File> files) {
        return apiHelper.createPost(title, description, token, isPublic, files);
    }

    @NotNull
    @Override
    public Single<Data> getPostLikes(@NotNull String token, int id) {
        return apiHelper.getPostLikes(token, id);
    }

    @NotNull
    @Override
    public Single<Data> getPostComments(@NotNull String token, int id) {
        return apiHelper.getPostComments(token, id);
    }

    @NotNull
    @Override
    public Single<Data> addCommentOnPost(@NotNull String token, int id, @NotNull String comment) {
        return apiHelper.addCommentOnPost(token, id, comment);
    }

    @NotNull
    @Override
    public Single<Data> fetchPosts(@NotNull String token) {
        return apiHelper.fetchPosts(token);
    }

    @NotNull
    @Override
    public Single<Data> likePosts(@NotNull String token, @NotNull int id) {
        return apiHelper.likePosts(token, id);
    }

    @NotNull
    @Override
    public Single<Data> getNotifications(@NotNull String token) {
        return apiHelper.getNotifications(token);
    }

    @NotNull
    @Override
    public Single<Data> getUsersList(@NotNull String token) {
        return apiHelper.getUsersList(token);
    }

    @NotNull
    @Override
    public Single<Data> reportPost(@NotNull String token, @NotNull String postId, @NotNull String tagName, @NotNull String description) {
        return apiHelper.reportPost(token, postId, tagName, description);
    }

    @NotNull
    @Override
    public Single<Data> getScanResult(@NotNull String token, @NotNull String qrCodeType, @NotNull String qrCode) {
        return apiHelper.getScanResult(token, qrCodeType, qrCode);
    }

    @NotNull
    @Override
    public Single<Data> unLikePost(@NotNull String token, int id) {
        return apiHelper.unLikePost(token, id);
    }

    @NotNull
    @Override
    public Single<Data> likeComment(@NotNull String token, int id) {
        return apiHelper.likeComment(token, id);
    }

    @NotNull
    @Override
    public Single<Data> unLikeComment(@NotNull String token, int id) {
        return apiHelper.unLikePost(token, id);
    }

    @NotNull
    @Override
    public Single<Data> getUserProfile(@NotNull String token, int id) {
        return apiHelper.getUserProfile(token, id);
    }

    @NotNull
    @Override
    public Single<Data> getUserPost(@NotNull String token, int id) {
        return apiHelper.getUserPost(token, id);
    }

    @NotNull
    @Override
    public Single<Data> getUserFollowers(@NotNull String token) {
        return apiHelper.getUserFollowers(token);
    }

    @NotNull
    @Override
    public Single<Data> getUserFollowings(@NotNull String token) {
        return apiHelper.getUserFollowings(token);
    }

    @NotNull
    @Override
    public Single<Data> deletePost(@NotNull String token, int id) {
        return apiHelper.deletePost(token, id);
    }

    @NotNull
    @Override
    public Single<Data> unFollowUser(@NotNull String token, int id) {
        return apiHelper.unFollowUser(token, id);
    }

    @NotNull
    @Override
    public Single<Data> getUserRequests(@NotNull String token) {
        return apiHelper.getUserRequests(token);
    }

    @NotNull
    @Override
    public Single<Data> rejectRequest(@NotNull String token, int id) {
        return apiHelper.rejectRequest(token, id);
    }

    @NotNull
    @Override
    public Single<Data> acceptRequest(@NotNull String token, int id) {
        return apiHelper.acceptRequest(token, id);
    }

    @NotNull
    @Override
    public Single<Data> addFriend(@NotNull String token, int id) {
        return apiHelper.addFriend(token, id);
    }

    @NotNull
    @Override
    public ApiHelper getApiHelper() {
        return apiHelper;
    }
}
