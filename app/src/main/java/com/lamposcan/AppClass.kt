package com.lamposcan

import android.app.Activity
import android.app.Application
import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import androidx.annotation.RequiresApi
import com.lamposcan.di.component.DaggerAppComponent
import com.lamposcan.ui.shake.ServiceShake
import com.lamposcan.ui.shake.ShakeReceiver
import com.tramsun.libs.prefcompat.Pref
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import javax.inject.Inject

class AppClass : Application(), HasActivityInjector, HasServiceInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var dispatchingServiceInjector: DispatchingAndroidInjector<Service>

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        val appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
        appComponent.inject(this)

        Pref.init(this)

       /* val intent = Intent(applicationContext, ServiceShake::class.java)
        startForegroundService(intent)

        val shakeReceiver = ShakeReceiver()
        registerReceiver(shakeReceiver, IntentFilter("shake.detector"))
        registerReceiver(shakeReceiver, IntentFilter("serviceRestarted"))*/
    }


    override fun activityInjector(): DispatchingAndroidInjector<Activity>? {
        return activityDispatchingAndroidInjector
    }

    override fun serviceInjector(): AndroidInjector<Service>? {
        return dispatchingServiceInjector
    }
}