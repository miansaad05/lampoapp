package com.lamposcan.utils

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.annotations.NonNull

class ItemOffsetDecoration(
    private val left: Int, private val top: Int,
    private val right: Int, private val bottom: Int
) :
    RecyclerView.ItemDecoration() {

    constructor(
        @NonNull context: Context, @DimenRes left: Int,
        @DimenRes top: Int, @DimenRes right: Int, @DimenRes bottom: Int
    ) : this(
        context.resources.getDimensionPixelSize(left),
        context.resources.getDimensionPixelSize(top),
        context.resources.getDimensionPixelSize(right),
        context.resources.getDimensionPixelSize(bottom)
    )

    override fun getItemOffsets(
        outRect: Rect, view: View, parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.set(left, top, right, bottom)
    }

}