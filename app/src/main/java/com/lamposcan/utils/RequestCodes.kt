package com.lamposcan.utils

object RequestCodes{
    val ADD_POST = 1000
    val SCAN_CAMERA = 1001
    val PHOTO_REQUEST = 1005
    val DELETE_POST_FROM_COMMENT = 1006
}