package com.lamposcan.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    private static final int REQUEST_APP_SETTINGS = 70;
    private static String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void finishWithResult(Activity activity, Bundle bundle, int result) {
        Intent i = new Intent();
        if (bundle != null) {
            i.putExtras(bundle);
        }
        activity.setResult(result, i);
        activity.finish();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the cu rrently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        view = null;
    }

    public static String getDeviceToken(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean emailValidate(String email) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
//    public static void showOKDialog(Context mContext, String title, String message, final DialogInterface.OnClickListener okListener) {
//        final Dialog d = new Dialog(mContext);
//        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        d.setCancelable(true);
//        d.setContentView(R.layout.dialog_buy_error);
//        if (okListener == null) {
//            ((LinearLayout) d.findViewById(R.id.ok_parent)).setVisibility(View.GONE);
//        }
//        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
//        TextView t1 = (TextView) d.findViewById(R.id.buying_validation_title);
//        if (title != null) {
//            t1.setText(title);
//        } else {
//            t1.setVisibility(View.GONE);
//        }
//
//        TextView t2 = (TextView) d.findViewById(R.id.buying_validation_msg);
//        t2.setText(message);
//
//        ImageView x = (ImageView) d.findViewById(R.id.cancel_buy_error);
//        x.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                d.cancel();
//                d.dismiss();
//            }
//        });
//
//        Button ok = (Button) d.findViewById(R.id.error_ok);
//        ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (okListener != null) {
//                    okListener.onClick(new DialogInterface() {
//                        @Override
//                        public void cancel() {
//                            d.cancel();
//                        }
//
//                        @Override
//                        public void dismiss() {
//                            d.dismiss();
//                        }
//                    }, 0);
//                } else {
//                    d.cancel();
//                    d.dismiss();
//                }
//            }
//        });
//        d.show();
//        /*new AlertDialog.Builder(mContext)
//                .setTitle(title)
//                .setMessage(message)
//                .setPositiveButton(mContext.getString(R.string.dialog_ok), okListener).setCancelable(false).create().show();*/
//
//    }


//    Cloudinary

    public static String GetUTCdatetimeAsString(Date date) {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(date);

        return utcTime;
    }

    public static Date StringDateToDate(String StrDate) {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        try {
            dateToReturn = dateFormat.parse(StrDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateToReturn;
    }

    public static void openPermissionsInSettings(Activity activity) {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + activity.getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        activity.startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);
    }

    public static String getDate(long time) {
        long exactTime = time * 1000;
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(exactTime);
        String date = DateFormat.format("dd MM, yyyy", cal).toString();

        StringTokenizer tokenizer = new StringTokenizer(date, " ");
        String day = tokenizer.nextToken();
        String monthWithComma = tokenizer.nextToken();
        String month = monthWithComma.substring(0, monthWithComma.length() - 1);

        String year = "";
        for (int count = date.length() - 1; count > date.length() - 5; count--) {
            year += date.charAt(count);
        }
        StringBuffer buffer = new StringBuffer(year);
        year = String.valueOf(buffer.reverse());

        if (month.equals("1")) {
            month = "Jan";
        } else if (month.equals("2")) {
            month = "Feb";
        } else if (month.equals("3")) {
            month = "March";
        } else if (month.equals("4")) {
            month = "April";
        } else if (month.equals("5")) {
            month = "May";
        } else if (month.equals("6")) {
            month = "June";
        } else if (month.equals("7")) {
            month = "July";
        } else if (month.equals("8")) {
            month = "August";
        } else if (month.equals("9")) {
            month = "Sep";
        } else if (month.equals("10")) {
            month = "Oct";
        } else if (month.equals("11")) {
            month = "Nov";
        } else if (month.equals("12")) {
            month = "Dec";
        }

        String finalDate = day + " " + month + ", " + year;
//        StringTokenizer tokenAfterMonth = tokenizer.nextToken().toString();
        return finalDate;
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public String getUtcDate(String dateStr) {
        //note: doesn't check for null

        return GetUTCdatetimeAsString(StringDateToDate(dateStr));

    }
}