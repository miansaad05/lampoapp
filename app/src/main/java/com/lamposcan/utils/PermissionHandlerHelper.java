package com.lamposcan.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionHandlerHelper
{
    public static final String[] STORAGE_PERMISSION = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static final String[] CAMERA_PERMISSION = new String[]{Manifest.permission.CAMERA};

    public static void checkPermissionHelper(@NonNull Context context, @NonNull String[] permissions, @NonNull CheckPermissionResponse response)
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
        {
            response.permissionGranted();
            return;
        }
        if (checkIsPermissionsGranted((Activity) context, permissions, 0))
        {
            response.permissionGranted();
        }
        else
        {
            for (String permission : permissions)
            {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission))
                {
                    response.showNeededPermissionDialog();
                    return;
                }
            }
            response.requestPermission();
        }
    }

    private static boolean checkIsPermissionsGranted(Activity activity, String[] permissions, int pos)
    {
        int permission = ContextCompat.checkSelfPermission(activity,
                permissions[pos]);
        return permission == PackageManager.PERMISSION_GRANTED && (permissions.length <= (pos + 1) || (checkIsPermissionsGranted(activity, permissions, pos + 1)));
    }

    public static void onRequestPermissionsResultHelper(@NonNull Context context, @NonNull String[] permissions, @NonNull PermissionResultResponse response)
    {
        if (checkIsPermissionsGranted(((Activity) context), permissions, 0))
        {
            response.permissionGranted();
        }
        else
        {
            response.permissionDenied();
        }
    }

    public interface CheckPermissionResponse
    {
        public void permissionGranted();

        public void showNeededPermissionDialog();

        public void requestPermission();
    }

    public interface PermissionResultResponse
    {
        public void permissionDenied();

        public void permissionGranted();
    }
}
