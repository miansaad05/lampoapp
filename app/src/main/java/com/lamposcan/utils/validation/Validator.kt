package com.lamposcan.utils.validation

import android.text.TextUtils
import android.util.Patterns

object Validator {
    fun validateName(name: String): Boolean {
        if (!TextUtils.isEmpty(name)) {
            return true
        }
        return false
    }

    fun validateEmail(email: String): Boolean {
        if (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true
        }
        return false
    }

    fun validatePassword(password: String): Boolean {
        if (!TextUtils.isEmpty(password) && password.length >= 6) {
            return true
        }
        return false
    }

    fun validateConfirmPassword(password: String, confirmPassword: String): Boolean {
        if (password.equals(confirmPassword)) {
            return true
        }
        return false
    }

    fun validateDob(dob: String): Boolean {
        if (!TextUtils.isEmpty(dob) && !dob.equals("Date of Birth", ignoreCase = true)) {
            return true
        }
        return false
    }

}
