package com.lamposcan.utils

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.lamposcan.R

object MessageAlert {
    fun showErrorMessage(view: View, message: String) {
        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT)

        val snackbarLayout = snackbar.view as (Snackbar.SnackbarLayout)
        val textView =
            snackbarLayout.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.visibility = View.INVISIBLE

        val params = snackbarLayout.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.BOTTOM

        val layoutInflater = LayoutInflater.from(view.context)
        val snackView = layoutInflater.inflate(R.layout.top_snack_bar, null)

        val titleView = snackView.findViewById<TextView>(R.id.tv_title)
        titleView.text = message

        snackbarLayout.setPadding(0, 0, 0, 0)
        snackbarLayout.addView(snackView, 0)

        snackbar.show()
    }


    fun showInformationMessage(view: View, message: String) {
        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT)

        val snackbarLayout = snackbar.view as (Snackbar.SnackbarLayout)
        val textView =
            snackbarLayout.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.visibility = View.INVISIBLE

        val params = snackbarLayout.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.BOTTOM

        val layoutInflater = LayoutInflater.from(view.context)
        val snackView = layoutInflater.inflate(R.layout.top_snack_bar_info, null)

        val titleView = snackView.findViewById<TextView>(R.id.tv_title)
        titleView.text = message

        snackbarLayout.setPadding(0, 0, 0, 0)
        snackbarLayout.addView(snackView, 0)

        snackbar.show()
    }
}