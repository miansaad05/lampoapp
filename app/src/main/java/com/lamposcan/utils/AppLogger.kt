package com.lamposcan.utils


import android.util.Log

object AppLogger {

    fun d(tag: String, message: String) {
        Log.d(tag, message)
    }
}
