package com.lamposcan.utils

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.WindowManager

import com.lamposcan.R

object CommonUtils {

    var USER_MODEL = "UserModel"
    fun showLoadingDialog(context: Context?, cancelable: Boolean): ProgressDialog {
        val progressDialog = ProgressDialog(context)
        if (context != null)
            progressDialog.show()
        if (progressDialog.window != null) {
            progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        progressDialog.setContentView(R.layout.progress_dialog)
        progressDialog.isIndeterminate = true
        progressDialog.setCancelable(cancelable)
        progressDialog.setCanceledOnTouchOutside(cancelable)
        progressDialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)//dialog background will not be dim (transparent)

        return progressDialog
    }


    const val CAMERA = 0
    const val GALLERY = 1
    const val STORAGE_PERMISSION_RESULT = 8001
    const val CAMERA_PERMISSION_RESULT = 8002
}

