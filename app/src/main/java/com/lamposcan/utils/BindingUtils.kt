package com.lamposcan.utils

import android.text.TextUtils
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.lamposcan.R

class BindingUtils {
    companion object {
        @JvmStatic
        @BindingAdapter("setSrc")
        fun setSrc(imageView: ImageView, src: Int) {
            if (src != 0) {
                imageView.setImageResource(src)
            }
        }

        @JvmStatic
        @BindingAdapter(
            value = ["imageUrlPlaceHolder", "placeHolder", "errorHolder"],
            requireAll = false
        )
        fun setImageWithPlaceHolder(
            imageView: ImageView, url: String, place: Int,
            error: Int
        ) {
            val placeHolder = if (place == 0) R.drawable.ic_default_user else place
            val errorHolder = if (error == 0) R.drawable.ic_default_user else error

            val context = imageView.context
            val requestOptions = RequestOptions()
                .placeholder(placeHolder)
                .error(errorHolder)

            Glide.with(context).load(url).apply(requestOptions)
                .into(imageView)
        }


        /* For local Use like getting images from gallery/camera */
        @JvmStatic
        @BindingAdapter("imageUrlFromUri")
        fun setImageUrlFromUri(imageView: ImageView, imageUrl: String) {
            if (!TextUtils.isEmpty(imageUrl)) {
                Glide.with(imageView.context)
                    .load(imageUrl)
                    .into(imageView)
            }
        }

        @JvmStatic
        @BindingAdapter("refreshing")
        fun setRefreshing(swipeRefreshLayout: SwipeRefreshLayout, isRefreshing: Boolean) {
            swipeRefreshLayout.isRefreshing = isRefreshing
        }
    }
}
