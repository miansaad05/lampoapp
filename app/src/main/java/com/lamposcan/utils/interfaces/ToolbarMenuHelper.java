package com.lamposcan.utils.interfaces;

/**
 * This interface is for the general toolbar.
 */
public interface ToolbarMenuHelper {

    void onBackClick();

    void onMenuItem();
}