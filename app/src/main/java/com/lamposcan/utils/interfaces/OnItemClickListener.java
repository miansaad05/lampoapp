package com.lamposcan.utils.interfaces;

public interface OnItemClickListener {
    void onItemClick(Object object);
}
