package com.lamposcan.utils.interfaces;

/**
 * This interface is for the general toolbar.
 */
public interface MoreOptionsToolbarMenuHelper {

    void onBackClick();

    void onOpenDrawer();

    void onMenuItem();

    void onMenuItem1();

    void onMenuItem2();
}