package com.lamposcan.utils.interfaces

interface AdapterUpdateListener {
    fun clearItems()

    fun removeItem(position: Int)

    fun addItems(obj: Any)

    fun addItems(items: Collection<*>, isLoadMore: Boolean)
}
